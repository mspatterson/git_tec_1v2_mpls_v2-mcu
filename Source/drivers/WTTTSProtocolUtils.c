//******************************************************************************
//
//  WTTTSProtocolUtils.c: Communication protocol routines
//
//  Copyright (c) 2013, Microlynx Systems Ltd.
//  ALL RIGHTS RESERVED
//
//  This module provides functions to read and write to the information memory
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2013-March-26     DD        Initial Version
//	2013-Nov-04		DD 			Update RFC packet structure - RFC now reports voltage and mA/h used.
//  2018-Nov-29     DD          Update for MPLS firmware.
//******************************************************************************

#include "WTTTSProtocolUtils.h"

//#pragma package(smart_init)


//
// Private Declarations
//

// The WTTTS packet parser is table driven. To add a new command
// or response, do the following:
//
//   1. In the .h file, add a new WTTTS_CMD_ or WTTTS_RESP_ define
//      (if required)
//
//   2. Change the NBR_CMD_TYPES or NBR_RESP_TYPES define (if
//      a new cmd or response was defined).
//
//   3. If a new command or response has been defined, and that
//      item has a data payload, define a structure for the
//      payload in the .h file, and add a member of that structure
//      type to the WTTTS_DATA_UNION union definition.
//
//   4. Finally, add a row to (for new cmds or responses) or
//      modify an existing entry in the m_pktInfo[] table.

#define NBR_CMD_TYPES  			10      // Total number of WTTTS_CMD_... defines
#define NBR_RESP_TYPES 			4       // Total number of WTTTS_RESP_... defines
#define RFC_RATE_DEFAULT		1000	//ms
#define RFC_TIMEOUT_DEFAULT		100		//ms
#define STREAM_RATE_DEFAULT		10		//ms
#define STREAM_TIMEOUT_DEFAULT	300		//seconds
#define PAIR_TIMEOUT_DEFAULT	900		//seconds
#define SOL_TIMEOUT_DEFAULT		10		// seconds
#define RF_PACKET_NUMBER		BUF_LENGTH

#define SER_NUMBER_SIZE	8

typedef struct {
    BYTE pktHdr;        // Expected header byte
    BYTE cmdRespByte;   // One of the WTTTS_CMD_... or WTTTS_RESP_ defines
    BYTE expDataLen;    // Required value for header pktLen param
} WTTTS_PKT_INFO;

static const WTTTS_PKT_INFO m_pktInfo[ NBR_CMD_TYPES ] = {
	{ MPL_HDR_FROM_HOST, WTTTS_CMD_NO_CMD, 			0 },
	{ MPL_HDR_FROM_HOST, MPL_CMD_CHANNEL_IN_USE, 	0 },
	{ MPL_HDR_FROM_HOST, MPL_CMD_SET_PIB_CONTACTS, 	sizeof(MPL_SET_PIB_CONTACTS_PKT)},
	{ MPL_HDR_FROM_HOST, MPL_CMD_SET_TEC_PARAM,   	sizeof(MPL_SET_TEC_PARAM_PKT)	},
	{ MPL_HDR_FROM_HOST, MPL_CMD_SET_PLUG_DET,  	sizeof(MPL_SET_PLUG_DET_PKT)	},
    { MPL_HDR_FROM_HOST, WTTTS_CMD_SET_RATE,        sizeof(WTTTS_RATE_PKT)          },
	{ MPL_HDR_FROM_HOST, WTTTS_CMD_QUERY_VER,       sizeof(WTTTS_GET_VER_PKT)       },
    { MPL_HDR_FROM_HOST, WTTTS_CMD_SET_RF_CHANNEL,  sizeof( WTTTS_SET_CHAN_PKT )    },
    { MPL_HDR_FROM_HOST, WTTTS_CMD_GET_CFG_DATA,    sizeof( WTTTS_CFG_ITEM )        },
    { MPL_HDR_FROM_HOST, WTTTS_CMD_SET_CFG_DATA,    sizeof( WTTTS_CFG_DATA )        },
};

static WORD wtttsTimeParams[NBR_SET_RATE_TYPES];

const WORD wtttsTimeParamsDefault[NBR_SET_RATE_TYPES]= {
		RFC_RATE_DEFAULT,
		RFC_TIMEOUT_DEFAULT,
		STREAM_RATE_DEFAULT,
		STREAM_TIMEOUT_DEFAULT,
		PAIR_TIMEOUT_DEFAULT,
		SOL_TIMEOUT_DEFAULT,
};

//static WTTTS_RFC_PKT	wtttsRfcPacket;

static BYTE radioPacket[RF_PACKET_NUMBER];
static BYTE *wtttsPacket;
static BYTE sequenceNumber;
static BOOL commNoCommand;

static BOOL commPowerDown;
static BYTE pageNumber;
static BYTE flashWriteResult;
static BOOL havePkt = FALSE;
static BOOL radioBusy = FALSE;
WTTTS_PKT_HDR* pktHdr;
BYTE* pktData;
WTTTS_CFG_DATA* cfgData;
WTTTS_CFG_ITEM* cfgItem;
static BYTE DeviceSerialNum[8];

static MPL_RF_RESP_ST mplRfRespSt = RF_RESP_IDLE;;
static MPL_SET_PIB_CONTACTS_PKT *pSetPibPkt;
static MPL_SET_TEC_PARAM_PKT    *pSetTecPkt;
static MPL_SET_PLUG_DET_PKT     *pSetPlugPkt;

BYTE adcAveraging[MASTER_ADC_CHNLS] = {ADC_AVRG_DEFAULT,ADC_AVRG_DEFAULT,ADC_AVRG_DEFAULT,ADC_AVRG_DEFAULT,ADC_AVRG_DEFAULT,ADC_AVRG_DEFAULT,};    // it's also used from slave TEC in PIBProtocolUtils.c


/*********************************************************
 * External Variables
 *********************************************************/
extern UVOLTS uvResultAverage[ADS1220_TOTAL];
extern signed long gyroSumAverage;

extern WORD wGyroReadCounter;
extern WORD digGyroSampleCounter;

extern WORD digGyroMoveCounter;
extern WORD digGyroZeroCounter;
extern WORD digGyroLastMovePoint;
extern WORD SamplesCounter[ADC_USED_LAST+2];

void word2ByteArray(void *pVoid, WORD wValue);



// Do not store paramaters in Flash. Always start with default values.
void UpdateTimeParamsFromFlash(void)
{
    WORD i;

    for(i=0;i<NBR_SET_RATE_TYPES;i++)
    {
        wtttsTimeParams[i] = wtttsTimeParamsDefault[i];

    }

}

//******************************************************************************
//
//  Function: SetTimeParameter
//
//  Arguments: BYTE *paramValues - pointer to array with three WORD data.
//
//  Returns:
//
//  Description: set Time parameters.
//
//******************************************************************************
void SetTimeParameter(BYTE *paramValues)
{
    WORD wTemp;

    wTemp = paramValues[1];
    wTemp  <<= 8;
    wTemp  |= paramValues[0];

    if(wTemp >= RFC_RATE_DEFAULT )
    {
        wtttsTimeParams[SRT_RFC_RATE] = wTemp;
    }

//	wtttsTimeParams[SRT_RFC_RATE] = paramValues[1];
//	wtttsTimeParams[SRT_RFC_RATE] <<= 8;
//	wtttsTimeParams[SRT_RFC_RATE] |= paramValues[0];

	wtttsTimeParams[SRT_RFC_TIMEOUT] = paramValues[3];
	wtttsTimeParams[SRT_RFC_TIMEOUT] <<= 8;
	wtttsTimeParams[SRT_RFC_TIMEOUT] |= paramValues[2];

	wtttsTimeParams[SRT_PAIR_TIMEOUT] = paramValues[5];
	wtttsTimeParams[SRT_PAIR_TIMEOUT] <<= 8;
	wtttsTimeParams[SRT_PAIR_TIMEOUT] |= paramValues[4];

}

//******************************************************************************
//
//  Function: GetTimeParameter
//
//  Arguments: WTTTS_RATE_TYPE paramNum - selects the parameter
//
//  Returns:    WORD  - time parameter value
//
//  Description: set Time parameters.
//
//******************************************************************************
WORD GetTimeParameter(const WTTTS_RATE_TYPE paramNum)
{
	return wtttsTimeParams[paramNum];

}

//******************************************************************************
//
//  Function: HaveWtttsPacket
//
//  Arguments: BYTE *pBuff - pointer to data buffer
//             WORD buffLen - size of the data buffer
//
//  Returns:    TRUE if valid packet is found, FALSE otherwise
//
//  Description: Search the data buffer for a valid packet.
//
//******************************************************************************
BOOL HaveWtttsPacket(BYTE *pBuff, WORD buffLen)
{
	   // Scans the passed buffer for a WTTTS command or response. Returns true if
	    // a packet is found, in which case pktHdr and pktData pointers are asigned.
	    // Returns false if no packet is found.
    WORD bytesSkipped = 0;

    BOOL foundInfoItem = FALSE;
    int  dataLen       = 0;
    int  expPktLen     = 0;
    unsigned int iInfoItem = 0;
    BYTE expCRC;

    havePkt = FALSE;

    if( buffLen < (WORD)MIN_WTTTS_PKT_LEN )
        return FALSE;

    while( bytesSkipped + (WORD)MIN_WTTTS_PKT_LEN <= buffLen )
       {
    		ResetWatchdog();
           // Have enough bytes for a packet. Header byte must be the first
           // byte we see
           if( pBuff[bytesSkipped] != MPL_HDR_FROM_HOST )
           {
               bytesSkipped++;
               continue;
           }

           // Looks like the start of a header. See if the rest of the packet
           // makes sense.
           pktHdr = (WTTTS_PKT_HDR*)&( pBuff[bytesSkipped] );

           for(iInfoItem = 0; iInfoItem < NBR_CMD_TYPES ; iInfoItem++ )
           {

               if( m_pktInfo[iInfoItem].cmdRespByte != pktHdr->pktType )
                   continue;

               // Found a hdr / type pair match. Calc the number of bytes required
               // for this packet. That would be a header, the data portion, and
               // a checksum byte.
               dataLen   = m_pktInfo[iInfoItem].expDataLen;
               expPktLen = MIN_WTTTS_PKT_LEN + dataLen;

               foundInfoItem = TRUE;
               break;
           }

           if( !foundInfoItem )
           {
               bytesSkipped++;
               continue;
           }

           // If we have enough bytes, check the CRC. Otherwise, we have to
           // wait for more bytes to come in
           if( bytesSkipped + expPktLen > buffLen )
               break;

           // Have enough bytes - validate the CRC
           expCRC = CalculateCRC( &( pBuff[bytesSkipped] ), expPktLen - 1 );

           if( pBuff[ bytesSkipped + expPktLen - 1 ] == expCRC )
           {
               // Success!
        	   pktHdr = (WTTTS_PKT_HDR*)&pBuff[bytesSkipped];

               if( dataLen > 0 )
               {
            	   pktData = &pBuff[bytesSkipped + sizeof( WTTTS_PKT_HDR )] ;
               }
               // Remove this packet from the buffer
               bytesSkipped += expPktLen;

               havePkt = TRUE;
               break;
           }

           // CRC error - continue scanning
           bytesSkipped++;
       }

    return havePkt;
}

//******************************************************************************
//
//  Function: ExecuteCommand
//
//  Arguments: none
//
//  Returns:   none
//
//  Description: executes the command in the RF packet
//
//******************************************************************************
void ExecuteCommand(void)
{

	radioBusy = TRUE;
	ResetWatchdog();
	switch(pktHdr->pktType)
	{
		case WTTTS_CMD_NO_CMD:
			SetNoCommand(TRUE);
			radioBusy = FALSE;
			break;
		case WTTTS_CMD_SET_RATE:
			SetTimeParameter(pktData);
			SendAckResp(WTTTS_CMD_SET_RATE, SET_CMD_ACK);
			break;
		case WTTTS_CMD_QUERY_VER:
			SendVersionInfo(pktData);
			break;
		case WTTTS_CMD_SET_RF_CHANNEL:
			SetChangeChannelPending(pktData[0]);
			break;
		case WTTTS_CMD_SET_CFG_DATA:
			cfgData = (WTTTS_CFG_DATA*)pktData;
			pageNumber = cfgData->pageNbr;
			flashWriteResult = WritePageInFlash(pageNumber, cfgData->pageData, (WORD)NBR_BYTES_PER_WTTTS_PAGE);
			SendCfgEvent();
			break;
		case WTTTS_CMD_GET_CFG_DATA:
			cfgItem = (WTTTS_CFG_ITEM*)pktData;
			pageNumber = cfgItem->pageNbr;
			SendCfgEvent();
			break;
		case MPL_CMD_CHANNEL_IN_USE:
			mplSetDoNotUseChannel();
			break;
		case MPL_CMD_SET_PIB_CONTACTS:
			pSetPibPkt = (MPL_SET_PIB_CONTACTS_PKT*)pktData;

			switch(pSetPibPkt->targetPIB)
			{
			case SET_MASTER_PIB:
	            PibSetParamFromRf(pSetPibPkt->targetPIB, pSetPibPkt->solenoidState, pSetPibPkt->contactState, pSetPibPkt->solenoidTimeout );
			    PostRs485Command(POST_MPIB_SET_PRMS);
			    mplRfRespSt = RF_RESP_SET_PIB;
			    break;
			case SET_SLAVE_PIB:
	            PibSetParamFromRf(pSetPibPkt->targetPIB, pSetPibPkt->solenoidState, pSetPibPkt->contactState, pSetPibPkt->solenoidTimeout );
                PostRs485Command(POST_SPIB_SET_PRMS);
                mplRfRespSt = RF_RESP_SET_PIB;
			    break;
			default:
			    SendAckResp(MPL_CMD_SET_PIB_CONTACTS, SET_CMD_NACK_PARAM_ERR);
			    break;
			}

			break;
		case MPL_CMD_SET_TEC_PARAM:
		    pSetTecPkt = (MPL_SET_TEC_PARAM_PKT*)pktData;

		    if(((pSetTecPkt->numberADC & SET_TEC_MASTER)==0)&&((pSetTecPkt->numberADC & SET_TEC_SLAVE)==0))
		    {
		        SendAckResp(MPL_CMD_SET_TEC_PARAM, SET_CMD_NACK_PARAM_ERR);
		        break;
		    }

		    if(pSetTecPkt->numberADC & SET_TEC_MASTER)
		    {
	            if((pSetTecPkt->numberADC & 0x3F) == 0)
	            {
	                // set all adc - master and slave
	                BYTE adcNumber;
	                for(adcNumber = ADC_USED_FIRST; adcNumber<MASTER_ADC_CHNLS; adcNumber++)
	                {
	                    SetAdcCurrent(adcNumber,  pSetTecPkt->currentOutput);
	                    ADS1220Reset(adcNumber);
	                    ADS1220Configure(ADS1220_IN_AIN1_AIN2,adcNumber, (ADS1220_IDAC_CURRENT) pSetTecPkt->currentOutput);
	                    adcAveraging[adcNumber] = pSetTecPkt->avgFactor;
	                }
	                if((pSetTecPkt->numberADC & SET_TEC_SLAVE)==0) // if the command only sets the Master send the ACK now
	                {
	                    SendAckResp(MPL_CMD_SET_TEC_PARAM, SET_CMD_ACK);
	                }
	            }
	            else
	            {
	                if(pSetTecPkt->numberADC <= MASTER_ADC_CHNLS)
	                {
	                    // set master adc
	                    BYTE adcNumber = pSetTecPkt->numberADC -1;
	                    SetAdcCurrent(adcNumber, pSetTecPkt->currentOutput);
	                    ADS1220Reset(adcNumber);
	                    ADS1220Configure(ADS1220_IN_AIN1_AIN2,adcNumber, (ADS1220_IDAC_CURRENT) pSetTecPkt->currentOutput);
	                    adcAveraging[adcNumber] = pSetTecPkt->avgFactor;
	                    if((pSetTecPkt->numberADC & SET_TEC_SLAVE)==0) // if the command only sets the Master send the ACK now
	                    {
	                        SendAckResp(MPL_CMD_SET_TEC_PARAM, SET_CMD_ACK);
	                    }

	                }
	                else
	                {
	                    if((pSetTecPkt->numberADC & SET_TEC_SLAVE)==0) // if the command only sets the Master send the ACK now
	                    {
	                        SendAckResp(MPL_CMD_SET_TEC_PARAM, SET_CMD_NACK_PARAM_ERR);
	                    }
	                }

	            }

		    }

		    if(pSetTecPkt->numberADC & SET_TEC_SLAVE)
		    {
                TecSlaveSetParamRf( pSetTecPkt->numberADC,pSetTecPkt->currentOutput, pSetTecPkt->avgFactor);
                PostRs485Command(POST_STEC_SET_PRMS);
                mplRfRespSt = RF_RESP_SET_TEC;
		    }

			break;
		case MPL_CMD_SET_PLUG_DET:
		    pSetPlugPkt = (MPL_SET_PLUG_DET_PKT*)pktData;
		    PtsSetParamRf( pSetPlugPkt->magnSampleRate ,pSetPlugPkt->magnAvg);
		    PostRs485Command(POST_PLUG_SET_PRMS);
            mplRfRespSt = RF_RESP_SET_PLUG;
			break;
		default:
			break;
	}
}

//******************************************************************************
//
//  Function: SendVersionInfo
//
//  Arguments:  BYTE setSel - - 1 = get master devices IDs, -   2 = get slave device IDs
//
//  Returns:    none
//
//  Description: Send boards versions event.
//
//******************************************************************************
void SendVersionInfo(BYTE* devId)
{
	WTTTS_PKT_HDR*	pWtttsHeader;
	MPL_VERSION_PKT*	pWtttsVerPacket;
	BYTE*	pWtttsCRC;
	BYTE bAddDelay;
	BYTE setSel;
	bAddDelay=0;

	setSel = *devId;
	// Parameter check
	if((setSel != VER_SEL_MASTER)&&(setSel != VER_SEL_SLAVE))
	{
	    SendAckResp(WTTTS_CMD_QUERY_VER, SET_CMD_NACK_PARAM_ERR);
	    return;
	}

	// Initialize the pointers
	radioPacket[0]  = TX_DATA;
	wtttsPacket     = &radioPacket[1];
	pWtttsHeader    = (WTTTS_PKT_HDR*)wtttsPacket;
	pWtttsVerPacket = (MPL_VERSION_PKT*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsCRC	    = (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(MPL_VERSION_PKT)];

	pWtttsHeader->pktHdr    = MPL_HDR_FROM_DEVICE;
	pWtttsHeader->pktType   = WTTTS_RESP_VER_PKT;
	pWtttsHeader->seqNbr    = sequenceNumber++;
	pWtttsHeader->dataLen   = sizeof( MPL_VERSION_PKT );

	word2ByteArray(&pWtttsHeader->timeStamp,GetTenMsCount());

	pWtttsVerPacket->devId = setSel;

	switch(setSel)
	{
	case VER_SEL_MASTER:
	    ReadPageFromFlash(VERS_PAGE_LOW, &pWtttsVerPacket->tecSerNum[0], (WORD)NBR_BYTES_PER_WTTTS_PAGE);       // TODO - check the page number with Ken
	    ReadPageFromFlash(VERS_PAGE_HIGH, &pWtttsVerPacket->tecSerNum[16], (WORD)NBR_BYTES_PER_WTTTS_PAGE);
	    pWtttsVerPacket->tecFwVer[0] = MAJOR_VERSION;
	    pWtttsVerPacket->tecFwVer[1] = MINOR_VERSION;
	    pWtttsVerPacket->tecFwVer[2] = BUILD_NUMBER;
	    pWtttsVerPacket->tecFwVer[3] = REL_DBG;
	    pWtttsVerPacket->tecHwVer[0] = GetHardwareRev();
	    pWtttsVerPacket->tecHwVer[1] = 0;
	    pWtttsVerPacket->tecHwVer[2] = 0;
	    pWtttsVerPacket->tecHwVer[3] = 0;

	    GetPibHwVer(PIB_M_BOARD,&pWtttsVerPacket->pibHwVer[0]);
	    GetPibFwVer(PIB_M_BOARD,&pWtttsVerPacket->pibFwVer[0]);

	    break;
	case VER_SEL_SLAVE:
	    GetSlaveTecHwVer(&pWtttsVerPacket->tecHwVer[0]);
	    GetSlaveTecFwVer(&pWtttsVerPacket->tecFwVer[0]);
	    GetSlaveTecSerNum(&pWtttsVerPacket->tecSerNum[0]);
        GetPibHwVer(PIB_S_BOARD,&pWtttsVerPacket->pibHwVer[0]);
        GetPibFwVer(PIB_S_BOARD,&pWtttsVerPacket->pibFwVer[0]);

	    break;
	default:
	    break;
	}

	GetPlugHwVer(&pWtttsVerPacket->plugDetHwVer[0]);
	GetPlugFwVer(&pWtttsVerPacket->plugDetFwVer[0]);
	GetPlugSerNum(&pWtttsVerPacket->plugDetSerNum[0]);

	*pWtttsCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(MPL_VERSION_PKT));

    while(IsZbTiSending())
    {
        ResetWatchdog();
        bAddDelay = 1;
    }
    if(bAddDelay)
    {
    __delay_cycles(55000);  // wait 5 ms
    }
	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(MPL_VERSION_PKT)+2);

}

//******************************************************************************
//
//  Function: SendCfgEvent
//
//  Arguments:  none
//
//  Returns:    none
//
//  Description: Send configuration event to the radio.
//
//******************************************************************************
void SendCfgEvent(void)
{
	WTTTS_PKT_HDR*	pWtttsHeader;
	WTTTS_CFG_DATA*	pWtttsCfgPacket;
	BYTE*	pWtttsCRC;
	BYTE bAddDelay;
	bAddDelay = 0;

	// Initialise the pointers
	radioPacket[0]  = TX_DATA;
	wtttsPacket     = &radioPacket[1];
	pWtttsHeader    = (WTTTS_PKT_HDR*)wtttsPacket;
	pWtttsCfgPacket = (WTTTS_CFG_DATA*) &wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsCRC	    = (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_CFG_DATA)];

	pWtttsHeader->pktHdr    = MPL_HDR_FROM_DEVICE;
	pWtttsHeader->pktType   = WTTTS_RESP_CFG_DATA;
	pWtttsHeader->seqNbr    = sequenceNumber++;
	pWtttsHeader->dataLen   = sizeof( WTTTS_CFG_DATA );
	word2ByteArray(&pWtttsHeader->timeStamp,GetTenMsCount());

	pWtttsCfgPacket->pageNbr = pageNumber;
	pWtttsCfgPacket->result  = ReadPageFromFlash(pageNumber, pWtttsCfgPacket->pageData, (WORD)NBR_BYTES_PER_WTTTS_PAGE);

	*pWtttsCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_CFG_DATA));


    while(IsZbTiSending())
    {
        ResetWatchdog();
        bAddDelay = 1;
    }
    if(bAddDelay)
    {
    __delay_cycles(55000);  // wait 5 ms
    }

	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_CFG_DATA)+2);

}


void SetNoCommand(BOOL bVal)
{
	commNoCommand = bVal;
}

BOOL GetCommandStatus(void)
{
	return commNoCommand;
}

void SetPowerDown(BOOL bVal)
{
	commPowerDown = bVal;
}

BOOL GetPowerDownStatus(void)
{
	return commPowerDown;
}

void ConvertLongTo3Bytes(signed long slValue, BYTE *pData)
{
	if(slValue>INT24_MAX_POSITIVE)
		slValue = INT24_MAX_POSITIVE;

	if(slValue < INT24_MAX_NEGATIVE)
			slValue = INT24_MAX_NEGATIVE;

	pData[0] = 	(BYTE) slValue;
	pData[1] = 	(BYTE) (slValue>>8);
	pData[2] = 	(BYTE) (slValue>>16);

}


void ConvertLongTo4Bytes(signed long slValue, BYTE *pData)
{

	pData[0] = 	(BYTE) slValue;
	pData[1] = 	(BYTE) (slValue>>8);
	pData[2] = 	(BYTE) (slValue>>16);
	pData[3] = 	(BYTE) (slValue>>24);

}

void word2ByteArray(void *pVoid, WORD wValue)
{
	BYTE *pByte;
	WORD wTemp;
	wTemp = wValue;	// function to do
	pByte = (BYTE*)	pVoid;
	*pByte++ = (BYTE)wTemp;
	*pByte = (BYTE)(wTemp>>8);
}



BOOL GetRadioPacketStatus(void)
{
	return havePkt;
}


void ClearRadioPacketStatus(void)
{
	havePkt = FALSE;
}

BOOL IsRadioBusy(void)
{
	return radioBusy;
}

void SetRadioBusy(BOOL bValue)
{
	radioBusy = bValue;
}

void SaveSerNumber(BYTE *pData)
{
	WORD i;
	for(i=0;i<SER_NUMBER_SIZE;i++)
	{
		DeviceSerialNum[i] = *pData++;
	}
}

void GetSerNumber(BYTE *pDest, BYTE nbrBytes)
{
    WORD i;
    for(i=0;i<nbrBytes;i++)
    {
        pDest[i] = DeviceSerialNum[i] ;
    }
}


//******************************************************************************
//
//  Function: mplSendRfcPIB
//
//  Arguments:  none
//
//  Returns:    none
//
//  Description: assemble and sends PIB RFC packet
//
//******************************************************************************
void mplSendRfcPIB(void)
{
    BYTE bAddDelay;
    BYTE bStatusTemp;
	WTTTS_PKT_HDR*	pWtttsRfcHeader;
	BYTE*	pWtttsRfcCRC;
	//WORD i;
	MPL_RFC_PKT* pMplRfcPacket;

	bAddDelay = 0;

	radioPacket[0] = TX_DATA;
	// Initialise the pointers
	wtttsPacket     = &radioPacket[1];
	pWtttsRfcHeader = (WTTTS_PKT_HDR*)wtttsPacket;
	pMplRfcPacket   = (MPL_RFC_PKT*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
	pWtttsRfcCRC	= (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(MPL_RFC_PKT)];

	pWtttsRfcHeader->pktHdr  = MPL_HDR_FROM_DEVICE;
	pWtttsRfcHeader->pktType = WTTTS_RESP_REQ_FOR_CMD;
	pWtttsRfcHeader->seqNbr  = sequenceNumber++;
	pWtttsRfcHeader->dataLen = sizeof( MPL_RFC_PKT );

	word2ByteArray(&pWtttsRfcHeader->timeStamp,GetTenMsCount());

	GetPibSlaveVoltage(&pMplRfcPacket->slaveBattVoltage);       // Voltage in mV (eg 3.742V would be 3742)
	GetSlaveTankPressure(&pMplRfcPacket->slaveTankPressure[0]);    //24 bit raw A/D value for pressure.
	GetSlaveRegPressure(&pMplRfcPacket->slaveRegPressure[0]);      //24 bit raw A/D value for pressure.
	GetPibMasterVoltage(&pMplRfcPacket->masterBattVoltage);     //Voltage in mV (eg 3.742V would be 3742)

	ConvertLongTo3Bytes(uvResultAverage[ADC_TANK_PRESSURE], pMplRfcPacket->masterTankPressure);
	ConvertLongTo3Bytes(uvResultAverage[ADC_REG_PRESSURE], pMplRfcPacket->masterRegPressure);

	pMplRfcPacket->rpm = GetRPM();
	pMplRfcPacket->temperature= GetTemperature();           // signed value, 0.5C per count with, -64C to +63.5C
	GetPtsData(&pMplRfcPacket->plugData[0]);                   //



	bStatusTemp  = GetLastReset() & 0x0003;
	pMplRfcPacket->systemStatusBits[LOW_BYTE]  = bStatusTemp;
	bStatusTemp = (GetBatType() & 0x0003) << 2;
    pMplRfcPacket->systemStatusBits[LOW_BYTE]  |= bStatusTemp;
    bStatusTemp = (GetSlaveBatteryType() & 0x0003) << 4;
    pMplRfcPacket->systemStatusBits[LOW_BYTE]  |= bStatusTemp;

    //bStatusTemp = (BYTE)CommBoardNotPresent(TEC_S_BOARD) << 2;
    bStatusTemp = CommBoardNotPresent(TEC_S_BOARD)? 0x04 : 0x00;
    pMplRfcPacket->systemStatusBits[HIGH_BYTE]  = bStatusTemp;
    //bStatusTemp = (BYTE)CommBoardNotPresent(PIB_S_BOARD) << 3;
    bStatusTemp = CommBoardNotPresent(PIB_S_BOARD)? 0x08 : 0x00;
    pMplRfcPacket->systemStatusBits[HIGH_BYTE]  |= bStatusTemp;
    //bStatusTemp = (BYTE)CommBoardNotPresent(PIB_M_BOARD) << 4;
    bStatusTemp = CommBoardNotPresent(PIB_M_BOARD)? 0x10 : 0x00;
    pMplRfcPacket->systemStatusBits[HIGH_BYTE]  |= bStatusTemp;
    //bStatusTemp = (BYTE)CommBoardNotPresent(PTS_BOARD) << 5;
    bStatusTemp = CommBoardNotPresent(PTS_BOARD)?  0x20 : 0x00;
    pMplRfcPacket->systemStatusBits[HIGH_BYTE]  |= bStatusTemp;
    bStatusTemp = (BYTE)GetPtsFlagBit() << 6;
    pMplRfcPacket->systemStatusBits[HIGH_BYTE]  |= bStatusTemp;

//    wStatusTemp  = GetLastReset() & 0x0003;
//    pMplRfcPacket->systemStatusBits  = wStatusTemp;
//
//	pMplRfcPacket->systemStatusBits  = GetLastReset() & 0x0003;
//	pMplRfcPacket->systemStatusBits |= (GetBatType() & 0x0003)          << 2;
//	pMplRfcPacket->systemStatusBits |= (GetSlaveBatteryType() & 0x0003) << 4;
////	pMplRfcPacket->systemStatusBits |= PowerUpBoardPresent(TEC_S_BOARD) << 6;
////	pMplRfcPacket->systemStatusBits |= PowerUpBoardPresent(PIB_S_BOARD) << 7;
////	pMplRfcPacket->systemStatusBits |= PowerUpBoardPresent(PIB_M_BOARD) << 8;
////	pMplRfcPacket->systemStatusBits |= PowerUpBoardPresent(PTS_BOARD)   << 9;
//    pMplRfcPacket->systemStatusBits |= CommBoardPresent(TEC_S_BOARD)    << 10;
//    pMplRfcPacket->systemStatusBits |= CommBoardPresent(PIB_S_BOARD)    << 11;
//    pMplRfcPacket->systemStatusBits |= CommBoardPresent(PIB_M_BOARD)    << 12;
//    pMplRfcPacket->systemStatusBits |= CommBoardPresent(PTS_BOARD)      << 13;
// system status bits:
//	D1 � D0: power up reason
//	    0x00 = RFU
//	    0x01 = Normal power up
//	    0x02 = Watch dog timer reset
//	    0x03 = Brown-out reset
//	D3 � D2: Master MPLS battery type
//	    0xnn = bit values to be determined
//	D5 � D4: Slave MPLS battery type
//	    0xnn = bit values to be determined
//	D6: RFU zero
//	D7: RFU zero
//	D8: RFU zero
//	D9: RFU zero
//	D10: Slave MPLS comms error (set to 1 if a Slave MPLS was detected on power up, but afterwards failed to respond to a serial command).
//	D11: Slave PIB comms error
//	D12: Master PIB comms error
//	D13: Plug Detector comms error
//	D14: Flag Status Bit (0 = Set, 1 = Tripped)
//	D15: RFU, always set to zero

	pMplRfcPacket->solenoidState    = GetPibSolenoidStatus();       // Lower nibble for the Master PIB. Upper nibble for Slave PIB solenoid used. Only one Solenoid can be active.
	pMplRfcPacket->contactState     = GetPibContactStatus();        // Lower nibble for Master outputs. Upper nibble for Slave outputs in a bit format where 1=ON.
	pMplRfcPacket->slaveSolenoidCurrent  = GetPibSlaveCurrent();    // integer value in mA.
	pMplRfcPacket->masterSolenoidCurrent = GetPibMasterCurrent();     // integer value in mA.
	pMplRfcPacket->rfuArray[0] = 0;
	pMplRfcPacket->rfuArray[1] = 0;
	pMplRfcPacket->rfuArray[2] = 0;
	pMplRfcPacket->rfuArray[3] = 0;

	*pWtttsRfcCRC = CalculateCRC((BYTE*)wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(MPL_RFC_PKT));

    while(IsZbTiSending())
    {
        ResetWatchdog();
        bAddDelay = 1;
    }
    if(bAddDelay)
    {
    __delay_cycles(55000);  // wait 5 ms
    }

	ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(MPL_RFC_PKT)+2);

	__delay_cycles(1000);
	// wait until is sending over the uart. There was intermittent delay during transmission (for unknown reasons) and the radio was truncating the packet.
    while(IsZbTiSending())
    {
        ResetWatchdog();
    }
}


//******************************************************************************
//
//  Function: UpdateRfRespState
//
//  Arguments: none
//
//  Returns: none
//
//  Description: This function updates the wireless response state machine.
//              It is used to wait for response from the slave boards and send the response over the radio.
//              It stays in idle mode unless set by ExecuteCommand() function to one of the wait for slave board response states.
//              When the correct response is received or timeout it sends the response over the radio and the state moves to IDLE.
//******************************************************************************
void UpdateRfRespState(void)
{
    switch(mplRfRespSt)
    {
    case RF_RESP_IDLE:
        // stays in idle unless set by ExecuteCommand()
        break;
    case RF_RESP_SET_PIB:
        switch(IsRs485RspAvail())
        {
        case PIB_SET_ANY_RSP:
            SendAckResp(MPL_CMD_SET_PIB_CONTACTS, SET_CMD_ACK);
            mplRfRespSt = RF_RESP_IDLE;
            break;
        case SET_RSP_TIMEOUT:           // how to handle both PIBs
            SendAckResp(MPL_CMD_SET_PIB_CONTACTS,SET_CMD_NACK);
            mplRfRespSt = RF_RESP_IDLE;
            break;
        default:
            break;
        }

        break;
    case RF_RESP_SET_TEC:
        switch(IsRs485RspAvail())
        {
        case TEC_SET_S_RSP:
            SendAckResp(MPL_CMD_SET_TEC_PARAM, SET_CMD_ACK);
            mplRfRespSt = RF_RESP_IDLE;
            break;
        case SET_RSP_TIMEOUT:
            SendAckResp(MPL_CMD_SET_TEC_PARAM,SET_CMD_NACK);
            mplRfRespSt = RF_RESP_IDLE;
            break;
        default:
            break;
        }
        break;

    case RF_RESP_SET_PLUG:
        switch(IsRs485RspAvail())
        {
        case PTS_SET_RSP:
            SendAckResp(MPL_CMD_SET_PLUG_DET, SET_CMD_ACK);
            mplRfRespSt = RF_RESP_IDLE;
            break;
        case SET_RSP_TIMEOUT:
            SendAckResp(MPL_CMD_SET_PLUG_DET, SET_CMD_NACK);
            mplRfRespSt = RF_RESP_IDLE;
            break;
        default:
            break;
        }
        break;

    default:
        mplRfRespSt = RF_RESP_IDLE;
        break;
    }
}



BOOL IsCmdWaitRespIdle(void)
{
    if (mplRfRespSt == RF_RESP_IDLE)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}


//******************************************************************************
//
//  Function: SendAckResp
//
//  Arguments: BYTE ackResp - success or fail. Values TBD.
//
//  Returns:
//
//  Description: send response to SET command over the radio.
//
//******************************************************************************
void SendAckResp(BYTE cmdType, BYTE ackResp)
{
    BYTE bAddDelay;
    WTTTS_PKT_HDR*  pWtttsHeader;
    WTTTS_ACK_RESP_PKT*  pWtttsAckResp;
    BYTE*   pWtttsCRC;

    bAddDelay = 0;
    // Initialise the pointers
    radioPacket[0]  = TX_DATA;
    wtttsPacket     = &radioPacket[1];
    pWtttsHeader    = (WTTTS_PKT_HDR*)wtttsPacket;
    pWtttsAckResp = (WTTTS_ACK_RESP_PKT*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)];
    pWtttsCRC       = (BYTE*)&wtttsPacket[sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_ACK_RESP_PKT)];

    pWtttsHeader->pktHdr    = MPL_HDR_FROM_DEVICE;
    pWtttsHeader->pktType   = WTTTS_RESP_SET_CMD;
    pWtttsHeader->seqNbr    = sequenceNumber++;
    pWtttsHeader->dataLen   = sizeof( WTTTS_ACK_RESP_PKT );
    word2ByteArray(&pWtttsHeader->timeStamp,GetTenMsCount());

    pWtttsAckResp->cmdType = cmdType;
    pWtttsAckResp->ackResp = ackResp;

    *pWtttsCRC = CalculateCRC(wtttsPacket, sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_ACK_RESP_PKT));

    while(IsZbTiSending())
    {
        ResetWatchdog();
        bAddDelay = 1;
    }
    if(bAddDelay)
    {
    __delay_cycles(55000);  // wait 5 ms
    }

    ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(WTTTS_ACK_RESP_PKT)+2);

}



//******************************************************************************
//
//  Function: getAdcAverage
//
//  Arguments: BYTE adcNbr
//
//  Returns: BYTE - average factor for adcNbr
//
//  Description:
//
//******************************************************************************
BYTE getAdcAverage(BYTE adcNbr)
{
    if(adcNbr<MASTER_ADC_CHNLS)
    {
        return adcAveraging[adcNbr];
    }
    else
    {
        return 0;
    }

}

//******************************************************************************
//
//  Function: SetAdcAverage
//
//  Arguments: BYTE adcNbr
//             BYTE value
//
//  Returns: none
//
//  Description: Set ADC averaging coefficient.
//
//******************************************************************************
void SetAdcAverage(BYTE adcNbr, BYTE value)
{
    if(adcNbr<MASTER_ADC_CHNLS)
    {
        adcAveraging[adcNbr] = value;
    }

}

//******************************************************************************
//
//  Function: CalculateCRC
//
//  Arguments: BYTE *bPointer
//             WORD wLentgth
//
//  Returns: BYTE - calculated CRC
//
//  Description: calculate CRC
//
//******************************************************************************
BYTE CalculateCRC(BYTE *bPointer, WORD wLength)
{
	const WORD c1 = 52845;
    const WORD c2 = 22719;
    WORD r;
    WORD i;
    DWORD sum;
	BYTE cipher;

	r = 55665;
	sum = 0;

	for( i=0; i<wLength;i++)
	{
		cipher = ( *bPointer ^ ( r >> 8 ) );
		r = ( cipher + r ) * c1 + c2;
		sum += cipher;
		bPointer++;
	}

	return (BYTE)sum;
}


//******************************************************************************
//
//  Function: mplResendRFCpib
//
//  Arguments: none
//
//
//  Returns: none
//
//  Description: resend the same RFC packet. There shouldn't be any other radio commands between mplSendRfcPIB and mplResendRFCpib
//              otherwise the radioPacket buffer will change and we need to populate it again.
//              Here we presume that the radioPacket buffer still contains the RFC packet.
//
//******************************************************************************
void mplResendRFCpib(void)
{
    BYTE  bAddDelay = 0;

    while(IsZbTiSending())
    {
        ResetWatchdog();
        bAddDelay = 1;
    }
    if(bAddDelay)
    {
    __delay_cycles(55000);  // wait 5 ms
    }

    ZbTiSendBinaryData(radioPacket,  sizeof(WTTTS_PKT_HDR)+sizeof(MPL_RFC_PKT)+2);

}
