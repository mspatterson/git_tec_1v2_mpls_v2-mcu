//******************************************************************************
//
//  TIMER.C: Module which supports timer services.
//
//  Copyright (c) 2011, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//  Modified on:    By:         For:
//-------------------------------------------------------------
//  2011-04-21      JD          Mlynx
//
//******************************************************************************

//------------------------------------------------------------------------------
//  INCLUDES
//------------------------------------------------------------------------------

#include "timer.h"


// TIMER_TICKS_PER_SECOND indicates the timer update frequency
// This will normally be 1000 for 1 millisecond resolution
#define TIMER_TICKS_PER_SECOND    1000


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

typedef struct {
        BOOL bIsAllocated;
        BOOL bIsRunning;
        WORD wMillisecsLeft;
    } TIMERINFO;

typedef struct {
        BOOL bIsAllocated;
        BOOL bIsRunning;
        WORD wSecsLeft;
    } TIMERINFO_SEC;

// low power mode structure
typedef struct {
    	BOOL bLPMode;			// the system is in low power mode with timer running
    	 WORD wSecsLeft;	// when tiLPMTimer expire the MCU will wake up
    } LPM_INFO;

//------------------------------------------------------------------------------
//  GLOBAL DECLARATIONS
//------------------------------------------------------------------------------

volatile TIMERINFO tiTimers[MAX_TIMERS];
volatile TIMERINFO_SEC tiSecTimers[MAX_TIMERS_SEC];
static volatile  BOOL  IsTimerInitialized = FALSE;
//static volatile DWORD TickCount  = 1;
static volatile DWORD UpTime     = 0;
static volatile DWORD UTCTime    = 0;
static volatile WORD  MsecsCount = 0;
static volatile WORD  MsecsCount1 = 0;
static volatile WORD  WaitCount  = 0;
static volatile WORD  TenMsecCount =0;
static LPM_INFO lpVar;

//------------------------------------------------------------------------------
//  PRIVATE FUNCTION PROTOTYPES
//------------------------------------------------------------------------------

static void SysTick_Handler(void);

//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS
//------------------------------------------------------------------------------

void	SysTimerConfig(void)
{
	TA1CCTL0 = CCIE;                          // CCR0 interrupt enabled
	TA1CCR0 = 1000;
  	TA1CTL = TASSEL_2 +ID_3+ MC_1 + TACLR;         // SMCLK /8 , upmode, clear TAR
  	__bis_SR_register(GIE);   
}

void SysTimerDisable(void)
{
	__bic_SR_register(GIE);
	TA1CCTL0 = 0;                          // CCR0 interrupt enabled
	TA1CCR0 = 0;
  	TA1CTL = 0;         // SMCLK /8 , upmode, clear TAR
  	__bis_SR_register(GIE);
}


// Timer A0 interrupt service routine
#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void)
{
	SysTick_Handler();                            // 
	if(lpVar.bLPMode == TRUE)
	{
		if(lpVar.wSecsLeft)
		{
			lpVar.wSecsLeft--;
		}
		else
		{
			LPM0_EXIT;
		}
	}
}



//******************************************************************************
//
//  Function: InitTimers
//
//  Arguments: function pointer for System Tick callback
//
//  Returns: nothing
//
//  Description: Initialises timers, must be called before any other function in
//              this module. This function assumes that interrupts can be hooked.
//              The SysTick interrupt is configured and used for timer updates.
//
//******************************************************************************
void InitTimers( void )
{
//    if( IsTimerInitialized == FALSE )
//    {
	unsigned int iIndex;

//        TickCount  = 1;
        UpTime     = 0;
        UTCTime    = 0;
        MsecsCount = 0;
        WaitCount  = 0;
        TenMsecCount = 0;
        MsecsCount1 = 0;

        // Clear the timer info array.
        for( iIndex = 0; iIndex < MAX_TIMERS; iIndex ++ )
        {
            tiTimers[iIndex].bIsAllocated   = FALSE;
            tiTimers[iIndex].bIsRunning     = FALSE;
            tiTimers[iIndex].wMillisecsLeft = 0;
        }

	for (iIndex = 0; iIndex < MAX_TIMERS_SEC; iIndex++) {
		tiSecTimers[iIndex].bIsAllocated = FALSE;
		tiSecTimers[iIndex].bIsRunning = FALSE;
		tiSecTimers[iIndex].wSecsLeft = 0;
	}
        // Setup the system tick timer
        // priority is set to lowest (31)
        SysTimerConfig();
        
        IsTimerInitialized = TRUE;
    //}
}


//******************************************************************************
//
//  Function: GetTickCount
//
//  Arguments:
//
//  Returns: number of msec that have passed since the module was initialized
//
//  Description: counts the number of msec since the module was initialized
//              resets to zero after 2^32 msec
//
//******************************************************************************
//DWORD GetTickCount( void )
//{
//    return TickCount;
//}

//******************************************************************************
//
//  Function: GetTenMsCount
//
//  Arguments:
//
//  Returns: number of 10msec that have passed since the module was initialized
//
//  Description: counts the number of 10msec since the module was initialized
//              resets to zero after 655350 msec
//
//******************************************************************************
WORD GetTenMsCount (void)
{
	return TenMsecCount;
//	return MsecsCount1;
}


DWORD GetSystemUpTime( void )
{
	// Returns the number of seconds since the system booted up
	return UpTime;
}


DWORD GetSystemUTCTime( void )
{
	// This timer shadows the UTC time received from the GPS
	return UTCTime;
}


void SetSystemUTCTime( DWORD newTime )
{
	// Sets the internal value of the UTC time shadow. No need
	// to disable interrupts here as the processor is a 32-bit
	// wide device and so the transfer is atomic.
	UTCTime = newTime;
}


//******************************************************************************
//
//  Function: WaitMsecs
//
//  Arguments:
//            Word msecCount - Number of milliseconds to wait
//
//  Returns: nothing
//
//  Description: waits for a given number of msec to pass before continuing on.
//               This function is ment for short waits. Otherwise the system can
//               stall here for up to 65 seconds
//
//******************************************************************************
void WaitMsecs( WORD msecCount )
{
    WaitCount = 0;
    do{
    	ResetWatchdog();
    }while( WaitCount < msecCount );
}


//******************************************************************************
//
//  Function: AllocateTimer
//
//  Arguments:  none
//
//  Returns: Timer handle
//
//  Description: allocates a timer to a specific process. Returns NULL if there
//               are no timers available
//
//******************************************************************************
TIMERHANDLE AllocateTimer( void )
{
	unsigned int iIndex;
	
	for( iIndex = 0; iIndex < MAX_TIMERS; iIndex++ )
    {
        if( tiTimers[iIndex].bIsAllocated == FALSE )
        {
        	break;
        }
        
    }

    if( iIndex == MAX_TIMERS )
    {
        return NULL;
    }

    tiTimers[iIndex].bIsRunning = FALSE;
    tiTimers[iIndex].bIsAllocated = TRUE;

    return iIndex + 1;
}



TIMERHANDLE AllocateSecTimer( void )
{
	unsigned int iIndex;

	for( iIndex = 0; iIndex < MAX_TIMERS_SEC; iIndex++ )
    {
        if( tiSecTimers[iIndex].bIsAllocated == FALSE )
        {
        	break;
        }

    }

    if( iIndex == MAX_TIMERS_SEC )
    {
        return NULL;
    }

    tiSecTimers[iIndex].bIsRunning = FALSE;
    tiSecTimers[iIndex].bIsAllocated = TRUE;

    return iIndex + 1;
}
//******************************************************************************
//
//  Function: RegisterTimer
//
//  Arguments:  none
//
//  Returns: Timer handle
//
//  Description: this is an alias for the 'allocateTimer' function which is
//               compatible with legacy code
//
//******************************************************************************
TIMERHANDLE RegisterTimer( void )
{
	TIMERHANDLE theHandle;

	theHandle = AllocateTimer();

    return theHandle;
}



TIMERHANDLE RegisterSecTimer( void )
{
	TIMERHANDLE theHandle;

	theHandle = AllocateSecTimer();

    return theHandle;
}
//******************************************************************************
//
//  Function: UnregisterTimer
//
//  Arguments:  Timer Handle
//
//  Returns: BOOL
//
//  Description: unregister timer
//               
//
//******************************************************************************
BOOL UnregisterTimer( TIMERHANDLE theHandle)
{
	unsigned int iIndex;
	iIndex = (int)theHandle;
	if(IsTimerHandleValid(theHandle))
	{
		tiTimers[iIndex-1].bIsRunning = FALSE;
    	tiTimers[iIndex-1].bIsAllocated = FALSE;
	}
	else
	{
		return NULL;
	}

    return TRUE;
}


BOOL UnregisterSecTimer( TIMERHANDLE theHandle)
{
	unsigned int iIndex;
	iIndex = (int)theHandle;
	if(IsSecTimerHandleValid(theHandle))
	{
		tiSecTimers[iIndex-1].bIsRunning = FALSE;
    	tiSecTimers[iIndex-1].bIsAllocated = FALSE;
	}
	else
	{
		return NULL;
	}

    return TRUE;
}

//******************************************************************************
//
//  Function: ResetTimer
//
//  Arguments:
//             TIMERHANDLE thTimer - handle fot timer to be reset
//             WORD wMillisecs - length of time to reset to timer to
//
//  Returns: nothing
//
//  Description: Initializes the passed timer to the value passed in milliseconds.
//             The timer state is not changed (eg if it was running, it is left running).
//
//******************************************************************************
void ResetTimer( TIMERHANDLE thTimer, WORD wMillisecs )
{
    if( !IsTimerHandleValid( thTimer ) )
        return;

    tiTimers[thTimer-1].wMillisecsLeft = wMillisecs;
}



void ResetSecTimer( TIMERHANDLE thTimer, WORD wSecs )
{
    if( !IsSecTimerHandleValid( thTimer ) )
        return;

    tiSecTimers[thTimer-1].wSecsLeft = wSecs;
}

//******************************************************************************
//
//  Function: StartTimer
//
//  Arguments:
//            TIMERHANDLE  thTimer - handle for the timer to be started
//
//  Returns: nothing
//
//  Description:  Starts the passed timer. Timer counts down from the
//                current/last count of the timer.
//
//******************************************************************************
void StartTimer( TIMERHANDLE thTimer )
{
    if( !IsTimerHandleValid( thTimer ) )
        return;

    tiTimers[thTimer-1].bIsRunning = TRUE;
}

void StartSecTimer( TIMERHANDLE thTimer )
{
    if( !IsSecTimerHandleValid( thTimer ) )
        return;

    tiSecTimers[thTimer-1].bIsRunning = TRUE;
}

//******************************************************************************
//
//  Function: StopTimer
//
//  Arguments:
//            TIMERHANDLE  thTimer - handle for the timer to be stopped
//
//  Returns: nothing
//
//  Description: Stops the passed timer. Does not clear or reset the timer value.
//
//******************************************************************************
void StopTimer ( TIMERHANDLE thTimer )
{
    if( !IsTimerHandleValid( thTimer ) )
        return;

    tiTimers[thTimer-1].bIsRunning = FALSE;
}


//******************************************************************************
//
//  Function: TimerRunning
//
//  Arguments:
//            TIMERHANDLE  thTimer - handle for the timer to be checked
//
//  Returns: true/false Is timer running?
//
//  Description: finds the current running state of the specified timer
//
//******************************************************************************
BOOL TimerRunning( TIMERHANDLE thTimer )
{
    if( !IsTimerHandleValid( thTimer ) )
        return 0;

    return tiTimers[thTimer-1].bIsRunning;
}


//******************************************************************************
//
//  Function: GetTimeLeft
//
//  Arguments:
//            TIMERHANDLE  thTimer - handle for the timer to be checked
//
//  Returns: the anount of time left on the timer
//
//  Description: calculates how much time is left on a given timer
//               It does not reset, start, or stop the timer though.
//
//******************************************************************************
WORD GetTimeLeft( TIMERHANDLE thTimer )
{
	WORD TimeLeft;
	
    if( !IsTimerHandleValid( thTimer ) )
        return 0;

	TimeLeft = tiTimers[thTimer-1].wMillisecsLeft;
   return TimeLeft;
}


//******************************************************************************
//
//  Function: TimerExpired
//
//  Arguments:
//            TIMERHANDLE  thTimer - handle for the timer to be checked
//
//  Returns: true/false Is the timer expired?
//
//  Description: calculates if a given timer has reached 0
//
//******************************************************************************
BOOL TimerExpired( TIMERHANDLE thTimer )
{
    // Returns TRUE if the specified timer has expired. Does
    // not reset or restart the timer though.
    if( !IsTimerHandleValid( thTimer ) )
        return FALSE;

    // Timer has expired if it is running and there are no
    // milliseconds left to count down.
    if( tiTimers[thTimer-1].bIsRunning && ( tiTimers[thTimer-1].wMillisecsLeft == 0 ) )
        return TRUE;

    // Fall through means the timer has not expired.
    return( FALSE);
}



BOOL SecTimerExpired( TIMERHANDLE thTimer )
{
    // Returns TRUE if the specified timer has expired. Does
    // not reset or restart the timer though.
    if( !IsSecTimerHandleValid( thTimer ) )
        return FALSE;

    // Timer has expired if it is running and there are no
    // milliseconds left to count down.
    if( tiSecTimers[thTimer-1].bIsRunning && ( tiSecTimers[thTimer-1].wSecsLeft == 0 ) )
        return TRUE;

    // Fall through means the timer has not expired.
    return( FALSE);
}
//------------------------------------------------------------------------------
//  PRIVATE FUNCTIONS
//------------------------------------------------------------------------------

//******************************************************************************
//
//  Function: IsTimerHandleValid
//
//  Arguments:
//            TIMERHANDLE  thTimer - handle for the timer to be checked
//
//  Returns: true/false Is handle valid?
//
//  Description:  Returns TRUE if the passed handle is valid, FALSE otherwise.
// Handle is invalid if it falls outside the timer info array range.
//
//******************************************************************************
BOOL IsTimerHandleValid( TIMERHANDLE thAHandle )
{
    if( thAHandle == 0  || thAHandle > MAX_TIMERS )
    {
        return FALSE;
    }

    // Handle is invalid if timer not allocated for that handle.

    if( tiTimers[thAHandle-1].bIsAllocated == FALSE )
    {
        return FALSE;
    }

    // Fall through means timer handle is valid.

    return TRUE;
}



BOOL IsSecTimerHandleValid( TIMERHANDLE thAHandle )
{
    if( thAHandle == 0  || thAHandle > MAX_TIMERS_SEC )
    {
        return FALSE;
    }

    // Handle is invalid if timer not allocated for that handle.

    if( tiSecTimers[thAHandle-1].bIsAllocated == FALSE )
    {
        return FALSE;
    }

    // Fall through means timer handle is valid.

    return TRUE;
}
//******************************************************************************
//
//  Function: TimerISR
//
//  Arguments: none
//
//  Returns: nothing
//
//  Description:  Decrements the milliseconds left on any timer that has been
//                allocated and is running. This function gets called once
//                every millisecond from the 1 ms tick interrupt
//
//******************************************************************************
void SysTick_Handler(void)
{
    BYTE iIndex;
//	static BOOL bTmp = FALSE;
//    TickCount++;   //increment the counter for GetTickCount()
    WaitCount++;   //increment the counter for WaitMsecs()
    MsecsCount++;  //increment msecs counter for seconds counter
    MsecsCount1++;
    UTCTime++;
    if( MsecsCount >= 1000 )
    {
    	UpTime++;
//    	UTCTime++;
    	MsecsCount = 0;
        for( iIndex = 0; iIndex < MAX_TIMERS_SEC; iIndex ++ )
        {
            if( tiSecTimers[iIndex].bIsRunning  == TRUE )
            {
               if( tiSecTimers[iIndex].wSecsLeft > 0 )
               {
                    tiSecTimers[iIndex].wSecsLeft--;
               }
            }
        }
    }

    if( MsecsCount1 >= 10 )
    {
    	TenMsecCount++;
       	MsecsCount1 = 0;
    }

    //parse our timer array
    for( iIndex = 0; iIndex < MAX_TIMERS; iIndex ++ )
    {
        if( tiTimers[iIndex].bIsRunning  == TRUE )
        {
           if( tiTimers[iIndex].wMillisecsLeft > 0 )
           {
                tiTimers[iIndex].wMillisecsLeft--;
           }
        }
    }

}





void SetupLPMode( WORD mSec)
{
	lpVar.bLPMode = TRUE;
	lpVar.wSecsLeft = mSec;
}


void DisableLPMode(void)
{
	lpVar.bLPMode = FALSE;
}

//	the system timer is running 50 times slower in low power mode
void SysTimerLPMConfig(void)
{
	TA1CCTL0 = CCIE;                          // CCR0 interrupt enabled
	TA1CCR0 = 50000;
  	TA1CTL = TASSEL_2 +ID_3+ MC_1 + TACLR;         // SMCLK /8 , upmode, clear TAR
  	__bis_SR_register(GIE);
}


