#ifndef UART_H_
#define UART_H_

#include "..\drivers.h"

//******************************************************************************
// TYPEDEFS
//******************************************************************************

// The UART_MGR_OBJECT is used by this module to maintain the context of an
// open serial port. This struct is required for the duration that a port is
// in use, therefore it should be allocated as a static variable.
typedef struct
{
   	const BYTE* ptrTxQ;                           // The UART manager controls these variables. Users of
    WORD  wTxQIndex;                              // this module must not directly modify these variables.
    WORD  wTxQSize;
    BOOL  bTxQSending;

} UART_OBJECT;

BOOL UARTA1Init();
void UARTA1RxEnable(void);
void UARTA1TxEnable(void);
void UARTA1RxDisable(void);
void UARTA1TxDisable(void);
BOOL UARTA1SendBuffer(UART_OBJECT* ptrUartObj, const BYTE* pBuffer, WORD wLength );
void FlushUartSerialTxQueue( UART_OBJECT* ptrUartObj );

BOOL UARTA2Init(void);
void UARTA2RxEnable(void);
void UARTA2SendBuffer(const BYTE* pBuffer, WORD wLength );

BOOL UARTA3Init(WORD baudRate);
void UARTA3RxEnable(void);
void UARTA3TxEnable(void);
void UARTA3RxDisable(void);
void UARTA3TxDisable(void);
BOOL UARTA3SendBuffer(UART_OBJECT* ptrUartObj, const BYTE* pBuffer, WORD wLength );
void A3RxISR( void );
void A3TxISR( void );
WORD ReadUartA3Data(BYTE *pData);


#endif /*UART_H_*/
