#ifndef FLASH_H_
#define FLASH_H_

#include "..\drivers.h"


#define ADDRESS_INFO_A	0x1980
#define ADDRESS_INFO_B	0x1900
#define ADDRESS_INFO_C	0x1880
#define ADDRESS_INFO_D	0x1800


void ReadFlash(WORD wSegmentAddress, BYTE *pData, WORD wLength);
void WriteFlash(WORD wSegmentAddress, BYTE *pData, WORD wLength);



#endif /*FLASH_H_*/
