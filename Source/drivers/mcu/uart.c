/*
 * uart.c
 *
 *  Created on: 2012-05-28
 *      Author: Owner
 */

#include "uart.h"

#define MAX_BUFFER	128

// UART divider define based on 8 MHZ clock.
#define UART_BR0_4800	130
#define UART_BR1_4800	6

#define UART_BR0_19200	160
#define UART_BR1_19200	1



BYTE RxBufferA3[MAX_BUFFER];
BYTE TxBufferA3[MAX_BUFFER];
BYTE RxIndexA3;
UART_OBJECT	A3UARTObj;




BOOL UARTA1Init(void)
{
	P5SEL |= 0xC0;                             // P5.6,7 = USCI_A1 TXD/RXD
	UCA1CTL1 |= UCSWRST;                      // **Put state machine in reset**
  	UCA1CTL1 |= UCSSEL_2;                     // SMCLK
//  	UCA1BR0 = 69;                              //69 -> 115942bps, 8MHz  / 38400 = 208	-	The 16-bit value of (UCAxBR0 + UCAxBR1 � 256) forms the prescaler value UCBRx.
  	UCA1BR0 = 64;                              //64->125000bps//32->250000bps //69 -> 115942bps, 8MHz  / 38400 = 208	-	The 16-bit value of (UCAxBR0 + UCAxBR1 � 256) forms the prescaler value UCBRx.
  	UCA1BR1 = 0;                              // 8MHz 38400	
  	UCA1MCTL |= UCBRS_3 + UCBRF_0;            // Modulation UCBRSx=3, UCBRFx=0
  	UCA1CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
  	//UCA1IE |= UCRXIE;                         // Enable USCI_A0 RX interrupt
  	return TRUE;
}


void UARTA1RxEnable(void)
{
	UCA1IE |= UCRXIE;
}


void UARTA1TxEnable(void)
{
	UCA1IE |= UCTXIE;
}

void UARTA1RxDisable(void)
{
	UCA1IE &= ~UCRXIE;
}


void UARTA1TxDisable(void)
{
	UCA1IE &= ~UCTXIE;
}


BOOL UARTA1SendBuffer(UART_OBJECT* ptrUartObj, const BYTE* pBuffer, WORD wLength )
{
	// ensure that a tx queue isn't already in progress
    if( ptrUartObj->bTxQSending == TRUE )
        return FALSE;
	
	// disable interrupts 
	// set the queue tracking vars to track this new Tx Queue being posted here
    ptrUartObj->ptrTxQ = pBuffer;
    ptrUartObj->wTxQIndex = 0;
    ptrUartObj->wTxQSize = wLength;
    ptrUartObj->bTxQSending = TRUE;
	
	//UARTA1TxEnable();
	//call the TX ISR explicitly 
	atZbTxISR();

	UARTA1TxEnable();
	// enable interrupts
	
	return TRUE;
}

//******************************************************************************
//
//  Function: FlushUartSerialTxQueue
//
//  Arguments: void.
//
//  Returns: void.
//
//  Description: Call this function to reset the transmit queue sending process;
//               it can be called to terminate a queue send that is in progress,
//               or to reset a queue process that has terminated
//
//******************************************************************************
void FlushUartSerialTxQueue( UART_OBJECT* ptrUartObj )
{
    // Disable interrupts while accessing the queue
	//    DisableInts();

    // reset the Tx queue tracking vars
    ptrUartObj->ptrTxQ = NULL;
    ptrUartObj->wTxQIndex = 0;
    ptrUartObj->wTxQSize = 0;
    ptrUartObj->bTxQSending = FALSE;

    // Enable them again.
    //EnableInts();
}

#pragma vector=USCI_A1_VECTOR
__interrupt void USCI_A1_ISR(void)
{
	
	switch(__even_in_range(UCA1IV,4))
  	{
  		case 0:
  			break;                             		// Vector 0 - no interrupt	
  		case 2:           
  			//atZbRxISR();                       		// Vector 2 - RXIFG
  			#ifdef ZB_PROGRAMMING
  			UCA2TXBUF =UCA1RXBUF;
  			#else
  			atZbRxISR();                       		// Vector 2 - RXIFG
  			#endif
    		break;
  		case 4:
  			atZbTxISR();
  			break;                             		// Vector 4 - TXIFG
  		
  		default: 
  			break;
  }
}



BOOL UARTA2Init(void)
{
	P9SEL |= (1<<4)|(1<<5);                             // P9.4,5 = USCI_A2 TXD/RXD
	UCA2CTL1 |= UCSWRST;                      // **Put state machine in reset**
  	UCA2CTL1 |= UCSSEL_2;                     // SMCLK
  	UCA2BR0 = 208;                              // 8MHz 38400	-	The 16-bit value of (UCAxBR0 + UCAxBR1 � 256) forms the prescaler value UCBRx.
  	UCA2BR1 = 0;                              // 8MHz 38400	
  	UCA2MCTL |= UCBRS_3 + UCBRF_0;            // Modulation UCBRSx=3, UCBRFx=0
  	UCA2CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
  	//UCA1IE |= UCRXIE;                         // Enable USCI_A0 RX interrupt
  	return TRUE;
}


void UARTA2RxEnable(void)
{
	UCA2IE |= UCRXIE;
}

#pragma vector=USCI_A2_VECTOR
__interrupt void USCI_A2_ISR(void)
{
	
	switch(__even_in_range(UCA2IV,4))
  	{
  		case 0:
  			break;                             		// Vector 0 - no interrupt	
  		case 2:           
  			UCA1TXBUF = UCA2RXBUF;                       		// Vector 2 - RXIFG
    		break;
  		case 4:
  			break;                             		// Vector 4 - TXIFG
  		
  		default: 
  			break;
  }
}



//////////////////////////////////////////////////////////////////////////////////////////////////
//!
//!	\fn			UARTA2SendBuffer(const BYTE* pBuffer, WORD wLength )
//!
//!	\brief		writes the pBuffer to the UARTA2
//!
//!	\note 		
//!
//!	\param[in]	pBuffer			pointer to Data to write 
//!
//!	\param[in]	wLength			Number of bytes to write
//!
//!	\param[out]	void
//!
//////////////////////////////////////////////////////////////////////////////////////////////////
void UARTA2SendBuffer(const BYTE* pBuffer, WORD wLength )
{
	unsigned int i;
	
	for(i=0;i<wLength;i++)
	{
		 while (!(UCA2IFG&UCTXIFG));             // USCI_A2 TX buffer ready?
		 UCA2TXBUF = pBuffer[i];                 // TX -> RXed character
	}

	
}



/*******************************
 * UART A3	Functions
 *
 ********************************/


BOOL UARTA3Init(WORD baudRate)
{
	SetPinAsPeripheral(UART_TX);
	SetPinAsPeripheral(UART_RX);
//	UCA3CTL1 |= UCSWRST;                      // **Put state machine in reset**
//  	UCA3CTL1 |= UCSSEL_2;                     // SMCLK
//  	UCA3BR0 = 4;                              //8MHz / 16 *125000bps, oversampling enabled
//  	UCA3BR1 = 0;                              //
//  	UCA3MCTL |= UCBRS_3 +UCBRF_0 +UCOS16;            // Modulation UCBRSx=3, UCBRFx=0
//  	UCA3CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
  	//UCA3IE |= UCRXIE;                         // Enable USCI_A3 RX interrupt
	UCA3CTL1 |= UCSWRST;                      // **Put state machine in reset**
  	UCA3CTL1 |= UCSSEL_2;                     // SMCLK

  	switch(baudRate)
  	{
  	case 4800:
  		UCA3BR0 = UART_BR0_4800;
  		UCA3BR1 = UART_BR1_4800;
  		break;
  	case 19200:
  		UCA3BR0 = UART_BR0_19200;
  		UCA3BR1 = UART_BR1_19200;
  		break;
  	default:
  		return FALSE;
  	}


  	UCA3MCTL |= UCBRS_3 + UCBRF_0;            // Modulation UCBRSx=3, UCBRFx=0
  	UCA3CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
  	return TRUE;
}


void UARTA3RxEnable(void)
{
	UCA3IE |= UCRXIE;
}


void UARTA3TxEnable(void)
{
	UCA3IE |= UCTXIE;
}

void UARTA3RxDisable(void)
{
	UCA3IE &= ~UCRXIE;
}


void UARTA3TxDisable(void)
{
	UCA3IE &= ~UCTXIE;
}


BOOL UARTA3SendBuffer(UART_OBJECT* ptrUartObj, const BYTE* pBuffer, WORD wLength )
{
	// ensure that a tx queue isn't already in progress
    if( ptrUartObj->bTxQSending == TRUE )
        return FALSE;

	// disable interrupts
	// set the queue tracking vars to track this new Tx Queue being posted here
    ptrUartObj->ptrTxQ = pBuffer;
    ptrUartObj->wTxQIndex = 0;
    ptrUartObj->wTxQSize = wLength;
    ptrUartObj->bTxQSending = TRUE;

	//UARTA3TxEnable();
	//call the TX ISR explicitly
	atRs485TxISR();

	UARTA3TxEnable();
	// enable interrupts

	return TRUE;
}

#pragma vector=USCI_A3_VECTOR
__interrupt void USCI_A3_ISR(void)
{

	switch(__even_in_range(UCA3IV,4))
  	{
  		case 0:
  			break;                             		// Vector 0 - no interrupt
  		case 2:
  			atRs485RxISR();                       		// Vector 2 - RXIFG
    		break;
  		case 4:
  			atRs485TxISR();
  			break;                             		// Vector 4 - TXIFG
  		default:
  			break;
  }
}

void A3RxISR( void )
{

	if( RxIndexA3 < MAX_BUFFER )
    {
    	RxBufferA3[RxIndexA3] = UCA3RXBUF;
        ++RxIndexA3;

    }

}

void A3TxISR( void )
{
    // This function is called in the context of an ISR
    if( A3UARTObj.wTxQIndex < A3UARTObj.wTxQSize )
    {
        UCA3TXBUF = A3UARTObj.ptrTxQ[A3UARTObj.wTxQIndex];
        A3UARTObj.wTxQIndex++;

    }
    else
    {
        // Disable TX interrupt
        UARTA3TxDisable();
        UARTA3RxEnable();
        A3UARTObj.bTxQSending = FALSE;

    }
}

WORD ReadUartA3Data(BYTE *pData)
{
	WORD wBytes;
	WORD wCnt;

	wCnt = 0;
	UARTA3RxDisable();		// disable RX interrupt
	wBytes = RxIndexA3;
	while(RxIndexA3)
	{
		*pData = RxBufferA3[wCnt];
		pData++;
		wCnt++;
		RxIndexA3--;

	}
	UARTA3RxEnable();		// enable RX interrupt
	return wBytes;
}



