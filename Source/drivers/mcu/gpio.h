#ifndef GPIO_H_
#define GPIO_H_



#include "..\drivers.h"

// Date Oct 2012 - update for PCB-00583-10-1v0
// Date Feb 2013 - update for PCB-00583-10-1v1
// Date Jan 2014 - update for PCB-00691-02-1v0
//******************************************************************************
// Port 1
//******************************************************************************
// P1.0 - ADC3_DRDYn	[Input]
// P1.1 - P0_5_RX		[Input] - BSL transmit output, becomes output in BSL mode
// P1.2 - P0_4_TX		[Input] - BSL receive input, becomes input in BSL mode
// P1.3 - FXO_INT2		[Input]
// P1.4 - ADC2_DRDYn   	[Input]
// P1.5 - ADC2_CSn	 	[Output]
// P1.6 - L3G_INT1_1 	[Input]
// P1.7 - L3G_DRDY		[Input]
#define ADC3_DRDY		0x00
#define BSL_TX			0x01
#define BSL_RX			0x02
#define FXO_INT2		0x03
#define ADC2_DRDY		0x04
#define ADC2_CSn		0x05
#define L3G_1_INT		0x06
#define L3G_1_DRDY		0x07

#define PORT_1_DIR_INIT     0x00|(1<<5)
#define PORT_1_VALUE_INT    0x00|(1<<5)
#define PORT_1_PERIPH_SEL   0x00
// TO DO: revisit power down mode for CCDS
//  in POWER DOWN MODE set all pins as outputs low
#define PORT_1_DIR_POWER_DOWN		0xFF
//#define PORT_1_DIR_POWER_DOWN		0x00	// temporary
#define PORT_1_VALUE_POWER_DOWN    	0x00


//******************************************************************************
// Port 2
//******************************************************************************
// P2.0 - ST_CAL_A0  	[Input]
// P2.1 - ST_CAL_A1		[Input]
// P2.2 - ST_CAL_A2		[Input]
// P2.3 - FXO_INT1		[Input]
// P2.4 - RTC_INT		[Input]
// P2.5 - REED_SWITCH	[Input]	// the REED_SWITCH has been changed with PUSH BUTTON On-Off
// P2.6 - ADC0_DRDYn	[Input]
// P2.7 - ADC0_CSn		[Output]
#define ST_CAL_A0		0x10
#define ST_CAL_A1		0x11
#define ST_CAL_A2		0x12
#define FXO_INT1		0x13
#define RTC_INT			0x14
#define REED_SWITCH		0x15
#define ADC0_DRDY		0x16
#define ADC0_CSn		0x17


#define PORT_2_DIR_INIT     0x00|(1<<7)
#define PORT_2_VALUE_INT    0x00|(1<<7)
#define PORT_2_PERIPH_SEL  	0x00
// in POWER DOWN MODE set all except P2.4 and P2.5 as outputs low
// TO DO: revisit power down mode for CCDS
#define PORT_2_DIR_POWER_DOWN     (1<<7)|(1<<6)|(1<<3)|(1<<2)|(1<<1)|(1<<0)
//#define PORT_2_DIR_POWER_DOWN     0x00		// temporary
#define PORT_2_VALUE_POWER_DOWN    0x00

  
//******************************************************************************
// Port 3
//******************************************************************************
// P3.0 - ADC_SCLK     	[Output]	clock out in SPI mode
// P3.1 - L3G_SDI		[Output] 	master out in SPI mode
// P3.2 - L3G_SDO		[Input]   	master in in SPI mode
// P3.3 - L3G_SCLK		[Output]	clock out in SPI mode
// P3.4 - ADC_DIN		[Output]	master out in SPI mode
// P3.5 - ADC_DOUT 		[Input] 	master in in SPI mode
// P3.6 - ZB_SPI_CLK	[Output]	not used 
// P3.7 - I2C_SDA		[I/O]		I2C data
#define ADC_SCLK		0x20
#define L3G_SDI			0x21
#define L3G_SDO			0x22
#define L3G_SCLK		0x23
#define ADC_DIN			0x24
#define ADC_DOUT		0x25
#define ZB_SPI_CLK		0x26
#define I2C_SDA			0x27

#define PORT_3_DIR_INIT     (1<<0)|(1<<4)
#define PORT_3_VALUE_INT    0x00
#define PORT_3_PERIPH_SEL   0x00
// in POWER DOWN MODE set all pins as outputs low
// TO DO: revisit power down mode for 1v1 - done
// I2C_SDA - input in power down
#define PORT_3_DIR_POWER_DOWN     	0xFF
//#define PORT_3_DIR_POWER_DOWN	(1<<0)|(1<<1)|(1<<3)|(1<<4)|(1<<6)
#define PORT_3_VALUE_POWER_DOWN    	0x00

   
//******************************************************************************
// Port 4
//******************************************************************************
// P4.0 - ADC1_CSn 	[Output]	ADC1 Chip Select
// P4.1 - S7_EN_n 	[Output]
// P4.2 - S6_EN_n 	[Output]
// P4.3 - S5_EN_n 	[Output]
// P4.4 - S4_EN_n	[Output]
// P4.5 - S3_EN_n	[Output]
// P4.6 - S2_EN_n	[Output]
// P4.7 - S1_EN_n	[Output]
#define ADC1_CSn		0x30
#define S7_EN_n			0x31
#define S6_EN_n			0x32
#define S5_EN_n			0x33
#define S4_EN_n			0x34
#define S3_EN_n			0x35
#define S2_EN_n			0x36
#define S1_EN_n			0x37


#define PORT_4_DIR_INIT     		0xFF
#define PORT_4_VALUE_INT    		0XFF	//0xFF
#define PORT_4_PERIPH_SEL			0x00
// TO DO: revisit power down mode for CCDS
#define PORT_4_DIR_POWER_DOWN		0xFF
//#define PORT_4_VALUE_POWER_DOWN		0XFE
#define PORT_4_VALUE_POWER_DOWN		0X00

//******************************************************************************
// Port 5
//******************************************************************************
// P5.0 - ADC4_DRDYn	[Input]
// P5.1 - ADC4_CSn		[Output]
// P5.2 - XT2IN 		[Input]		Input terminal for crystal oscillator XT2
// P5.3 - XT2OUT 		[Output]	Output terminal of crystal oscillator XT2
// P5.4 - I2C_SCL		[Output]	I2C Clock
// P5.5 - ADC3_CSn		[Output]
// P5.6 - ZB_UART_TX	[Output]
// P5.7 - ZB_UART_RX	[Input]
#define ADC4_DRDY		0x40
#define ADC4_CSn		0x41
#define XT2INPUT		0x42
#define XT2OUTPUT		0x43
#define I2C_SCL			0x44
#define ADC3_CSn		0x45
#define ZB_UART_TX		0x46
#define ZB_UART_RX		0x47


#define PORT_5_DIR_INIT     (1<<1)|(1<<4)|(1<<5)|(1<<6)
#define PORT_5_VALUE_INT    (1<<1)|(1<<4)|(1<<5)|(1<<6)
#define PORT_5_PERIPH_SEL	0x00
// in POWER DOWN MODE set P5.2 as input, all others output low
// TO DO: revisit power down mode for CCDS
//#define PORT_5_DIR_POWER_DOWN     (1<<0)|(1<<1)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<<7)
//#define PORT_5_DIR_POWER_DOWN     (1<<0)|(1<<1)|(1<<3)|(1<<4)|(1<<5)|(1<<6)		// temporary for PCB1v1
//#define PORT_5_DIR_POWER_DOWN     (1<<0)|(1<<1)|(1<<3)|(1<<5)|(1<<6)		// temporary for PCB1v1 - I2C Clock INPUT
//#define PORT_5_VALUE_POWER_DOWN   0x03

#define PORT_5_DIR_POWER_DOWN     0xFF
#define PORT_5_VALUE_POWER_DOWN   0x00
//******************************************************************************
// Port 6
//******************************************************************************
// P6.0 - BAT_V_1		[Input]		ADC Ch0
// P6.1 - BAT_Data		[Input]		ADC Ch1 and data I/O
// P6.2 - BAT_Data1		[Input]		ADC Ch2 and data I/O
// P6.3 - BAT_SENSE_EN	[Output]	ENABLE THE BATTERY VOLTAGE DIVIDERS
// P6.4 - HWD_REV_IN1	[Input]		Hardware Revision Input 1
// P6.5 - HWD_REV_IN2	[Input]		Hardware Revision Input 2
// P6.6 - ADC7_DRDYn	[Input]
// P6.7 - ADC7_CSn		[Output]
#define MCU_ADC_CH0		0x50
#define MCU_ADC_CH1		0x51
#define MCU_ADC_CH2		0x52
#define BAT_SENSE_EN	0x53
#define HWD_REV_IN1		0x54
#define HWD_REV_IN2		0x55
#define ADC7_DRDY		0x56
#define ADC7_CSn		0x57

//set P6.0 to P6.1 output high for now so that the load switch are fully On
#define PORT_6_DIR_INIT     (1<<3)|(1<<7)
#define PORT_6_VALUE_INT    (1<<7)
#define PORT_6_PERIPH_SEL	0x00
// in POWER DOWN MODE set P6.0 to P6.2 as inputs, all others as output low
// TO DO: revisit power down mode for 1v1 - done
//#define PORT_6_DIR_POWER_DOWN     (1<<3)|(1<<4)|(1<<5)|(1<<7)
#define PORT_6_DIR_POWER_DOWN   (1<<2)|(1<<3)|(1<<6)|(1<<7)
#define PORT_6_VALUE_POWER_DOWN   0x00

//******************************************************************************
// Port 7
//******************************************************************************
// P7.0 - XIN 			[Input]		Input terminal for crystal oscillator XT1
// P7.1 - XOUT 			[Output]	Output terminal of crystal oscillator XT1
// P7.2 - EN_V_EXT		[Output]	enable external power supply
// P7.3 - ST_CAL_B2	 	[Input]		external input
// P7.4 - ADC6_DRDYn	[Input]
// P7.5 - ADC6_CSn		[Output]
// P7.6 - ADC5_DRDYn	[Input]
// P7.7 - ADC5_CSn		[Output]
#define XINPUT			0x60
#define XOUTPUT			0x61
#define EN_V_EXT		0x62
#define ST_CAL_B2		0x63
#define ADC6_DRDY		0x64
#define ADC6_CSn		0x65
#define ADC5_DRDY		0x66
#define ADC5_CSn		0x67


#define PORT_7_DIR_INIT     (1<<1)|(1<<2)|(1<<5)|(1<<7)
#define PORT_7_VALUE_INT    (1<<5)|(1<<7)
#define PORT_7_PERIPH_SEL	0x00

//#define PORT_7_DIR_POWER_DOWN     (1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<<7)
#define PORT_7_DIR_POWER_DOWN     0xFF
#define PORT_7_VALUE_POWER_DOWN   0x00


//******************************************************************************
// Port 8
//******************************************************************************
// P8.0 - HWD_REV_IN0	[Input]
// P8.1 - EN_AG	 		[Output]
// P8.2 - L3G_CS_1 		[Output]
// P8.3 - FXO_RST-0		[Output]
// P8.4 - EN_VR	 		[Output]
// P8.5 - ADC1_DRDYn	[Input]
// P8.6 - ST_CAL_B0		[Input]
// P8.7 - ST_CAL_B1		[Input]

#define HWD_REV_IN0		0x70
#define EN_AG			0x71
#define L3G_CS_1		0x72
#define FXO_RST			0x73
#define EN_VR			0x74
#define ADC1_DRDY		0x75
#define ST_CAL_B0		0x76
#define ST_CAL_B1		0x77


#define PORT_8_DIR_INIT     (1<<1)|(1<<2)|(1<<3)|(1<<4)
#define PORT_8_VALUE_INT    (1<<4)
#define PORT_8_PERIPH_SEL	0x00
// in POWER DOWN NODE set all as outputs low

#define PORT_8_DIR_POWER_DOWN     	(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<<7)
#define PORT_8_VALUE_POWER_DOWN    	0x00

//******************************************************************************
// Port 9
//******************************************************************************
// P9.0 - ZB_UART_CTS		[Input]
// P9.1 - ZB_UART_RTS 		[Output]
// P9.2 - ZB_RSTn 			[Output]
// P9.3 - TP39		 		[Output]
// P9.4 - ST_CAL_ON_B		[Output]
// P9.5 - ST_CAL_ON_A		[Output]
// P9.6 - SW_LED			[Output]
// P9.7 - RS485_PWR_EN		[Output]	enable RS485 EXT POWER

#define ZB_UART_CTS		0x80
#define ZB_UART_RTS		0x81
#define ZB_RSTn			0x82
#define P9_TP39			0x83
#define ST_CAL_ON_B		0x84
#define ST_CAL_ON_A		0x85
#define SW_LED			0x86
#define RS485_PWR_EN	0x87

#define PORT_9_DIR_INIT     (1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<<7)
#define PORT_9_VALUE_INT    (1<<7)|(1<<2)
#define PORT_9_PERIPH_SEL	0x00

// in POWER DOWN NODE set all as outputs low
//#define PORT_9_DIR_POWER_DOWN     (1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6)|(1<<7)
//#define PORT_9_VALUE_POWER_DOWN   0x00
//#define PORT_9_VALUE_POWER_DOWN   0x04	// temporary for 1v1
#define PORT_9_DIR_POWER_DOWN     0xFF
#define PORT_9_VALUE_POWER_DOWN   0x00

//******************************************************************************
// Port 10
//******************************************************************************
// P10.0 - EXT_DEV_ADD		[Output]
// P10.1 - MS_SETUP_IN 		[Input]
// P10.2 - RS485_TX_EN 		[Output]
// P10.3 - RS485_RX_EN	 	[Output]
// P10.4 - UART_TX			[Output]
// P10.5 - UART_RX			[Input]
// P10.6 - EN_VD			[Output]
// P10.7 - EN_VA			[Output]

#define EXT_DEV_ADD		0x90
//#define EXT_DEV_EN		0x91
#define MS_SETUP_IN     0x91
#define RS485_TX_EN		0x92
#define RS485_RX_EN		0x93
#define UART_TX			0x94
#define UART_RX			0x95
#define EN_VD			0x96
#define EN_VA			0x97


#define PORT_10_DIR_INIT    (1<<0)|(1<<2)|(1<<3)|(1<<4)|(1<<6)|(1<<7)
#define PORT_10_VALUE_INT   0x00
#define PORT_10_PERIPH_SEL	0x00
// TO DO: revisit power down mode for CCDS
#define PORT_10_DIR_POWER_DOWN     	0xFF
#define PORT_10_VALUE_POWER_DOWN  	0x00

//******************************************************************************
// Port 11	- updated to 1V0
//******************************************************************************
// P11.0 - CLK1OUT			[Output]
// P11.1 - CLK2OUT	 		[Output]
// P11.2 - CLK3OUT	 		[Output]
// P11.3 - na
// P11.4 - na
// P11.5 - na
// P11.6 - na
// P11.7 - na

#define CLK1OUT			0xA0
#define CLK2OUT			0xA1
#define CLK3OUT			0xA2

#define PORT_11_DIR_INIT     0x07
#define PORT_11_VALUE_INT    0xff
#define PORT_11_PERIPH_SEL	 0x00

#define PORT_11_DIR_POWER_DOWN     0x07
#define PORT_11_VALUE_POWER_DOWN    0x00


#define TOTAL_PORT_NUMBERS	11


// get the port number for the pin name
#define GPIO_GET_PORT( a )	( (a) >> 4 )

// get the pin number for the pin name4
#define GPIO_GET_PIN( a )	( (a) & 0x0f )


void SetPinAsPeripheral(BYTE pinName);
void SetPinAsOutput(BYTE pinName);
void SetPinAsInput(BYTE pinName);
void SetOutputPin(BYTE pinName, BOOL bState);
BOOL ReadInputPin(BYTE pinName);
void EnableInputPullUp(BYTE pinName);

void initMPS430Pins( void );
void SetMPS430PinsInPowerDown( void );


#endif /*GPIO_H_*/
