/*
 * system.c
 *
 *  Created on: 2012-05-17
 *      Author: Owner
 */

#include "system.h"

static BYTE lastReset =1;
//******************************************************************************
// Function name:    Set_System_Clock
//******************************************************************************
// Description:      initialize the MCU clock
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void Set_System_Clock (void)
{
	P11DIR = BIT0;                       // P11.1-2 to output direction
	P11SEL |= BIT0;                      // P11.1-2 to output SMCLK,MCLK
	P5SEL |= 0x0C;                            // Port select XT2

	UCSCTL6 &= ~XT2OFF;                       // Enable XT2
	//	  UCSCTL3 |= SELREF_2;                      // FLLref = REFO
	// Since LFXT1 is not used,
	// sourcing FLL with LFXT1 can cause
	// XT1OFFG flag to set
	UCSCTL3 = SELREF__XT2CLK;                      // FLLref = XT2CLK when available, otherwise REFOCLK.
	UCSCTL4 = SELA_2|SELM__XT2CLK|SELS__XT2CLK;                        // ACLK=REFO,SMCLK=XT2CLK,MCLK=XT2CLK

	// Loop until XT1,XT2 & DCO stabilizes
	do
	{
		UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
		// Clear XT2,XT1,DCO fault flags
		SFRIFG1 &= ~OFIFG;                      // Clear fault flags
	}while (SFRIFG1&OFIFG);                   // Test oscillator fault flag

	//	  UCSCTL6 &= ~XT2DRIVE0;                    // Decrease XT2 Drive according to
	// expected frequency
	UCSCTL6 &= ~XT2DRIVE1;                    // Decrease XT2 Drive according to
//	UCSCTL4 |= SELA__XT2CLK + SELS_5 + SELM_5;               // ACLK=SMCLK=MCLK=XT2
	UCSCTL4 = SELA__XT2CLK + SELS_5 + SELM_5;               // ACLK=SMCLK=MCLK=XT2
//	UCSCTL4 = SELA_2 + SELS_5 + SELM_5;               // ACLK=REFO, SMCLK=MCLK=XT2

	UCSCTL5 = DIVPA0;
	//	  UCSCTL5 =	DIVPA2;	// divided by 4

}


//******************************************************************************
// Function name:    SetSystemDeepSleep
//******************************************************************************
// Description:      set the system in deep sleep and cofigure wake up on interrupt from the REED_SWITCH
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void SetSystemDeepSleep(void)
{
	SetMPS430PinsInPowerDown();
	SysTimerDisable();
	DisablePeriferals();
	SetReedSwitchInterrupt();
	Set_System_Clock_LPM();
    PJDIR	= 0Xff;
    PJOUT	= 0x00;
	__bis_SR_register(LPM4_bits + GIE);       // Enter LPM4 w/interrupt
  	__no_operation();                         // For debugger
	DisableReedSwitchInterrupt();


}


//******************************************************************************
// Function name:    Set_System_Clock_LPM
//******************************************************************************
// Description:      initialize the MCU clock for low power mode
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void Set_System_Clock_LPM (void)
{
	  P11DIR = BIT0;                       // P11.1-2 to output direction
	  P11SEL = 0;                      // P11.1-2 to output SMCLK,MCLK



//	  UCSCTL3 = SELREF__REFOCLK;                      // FLLref =  REFOCLK.
	  UCSCTL3 = 0;                      // FLLref =  REFOCLK.
	  UCSCTL4 = SELM_1 + SELS_1 + SELA_1;       // MCLK = SMCLK = ACLK = VLO

	  UCSCTL6 |= XT2OFF;                       // Disable XT2
	  UCSCTL8 = 0;
	  P5SEL = 0x00;                            // Port select XT2
//	  UCSCTL5 =	DIVPA2;	// divided by 4

}



//******************************************************************************
// Function name:    SetWatchdog
//******************************************************************************
// Description:      set the watchdog to 1sec interval
//					assuming the ACLK is sourced from REF0 	32768 Hz
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void SetWatchdog(void)
{
#ifdef WATCHDOG_ENABLE
//	WDTCTL = WDT_ARST_1000;
	WDTCTL = WDT_ARST_8M;
#endif
}

//******************************************************************************
// Function name:    ResetWatchdog
//******************************************************************************
// Description:      clear the watchdog counter
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void ResetWatchdog(void)
{
#ifdef WATCHDOG_ENABLE
//	WDTCTL = WDT_ARST_1000;
	WDTCTL = WDT_ARST_8M;
#endif
}

//******************************************************************************
// Function name:    StopWatchdog
//******************************************************************************
// Description:      hold the watchdog
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void StopWatchdog(void)
{
	WDTCTL = WDTPW + WDTHOLD;
}

/*	to do
 * To get the cause of the last reset(s), use the reset vector generator SYSRSTIV.
 * It will give you on each reading the highest priority reset cause that hasn't already read.
 * see http://e2e.ti.com/support/microcontrollers/msp430/f/166/t/99283.aspx
 */
BYTE GetLastReset(void)
{
    BYTE result;
    switch(lastReset)
    {
        case 0x00:
            result = 0x01;
        break;
        case 0x02:
            result = 0x03;
        break;
        case 0x04:
            result = 0x01;
        break;
        case 0x16:
            result = 0x02;
        break;
        case 0x06:
            result = 0x03;
        break;
        case 0x0A:
            result = 0x03;
        break;
        case 0x0C:
            result = 0x01;
        break;

        default:
            result = 0;
            break;
    }

	return result;
}


void ReadLastResetCause(void)
{
	lastReset = (BYTE)SYSRSTIV;
}


void DisablePeriferals(void)
{
	// disable ADC12
	ADC12CTL0 = 0;
	// Disable 2.5V shared reference,
	REFCTL0 = 0;

	TA1CTL = 0;
	UCA0CTL1 |= UCSWRST;
	UCA1CTL1 |= UCSWRST;
	UCA2CTL1 |= UCSWRST;
	UCA3CTL1 |= UCSWRST;
	UCB0CTL1 |= UCSWRST;
	UCB1CTL1 |= UCSWRST;
	UCB2CTL1 |= UCSWRST;
	UCB3CTL1 |= UCSWRST;

}





