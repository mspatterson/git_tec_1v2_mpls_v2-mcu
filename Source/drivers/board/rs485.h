#ifndef RS485_MODULE_H
#define RS485_MODULE_H

#include "..\drivers.h"

#define BAUD_RATE_PIB   4800
#define BAUD_RATE_TEC   19200
#define BAUD_RATE_PLUG  19200

#define RS845_TX_EN     1
#define RS845_RX_EN     0


void DisableRS485(void);
void EnRXRS485(void);
void EnTXRS485(void);
void EnTXRXRS485(void);
BOOL InitRs485Interface(WORD baudRate, BOOL txEn);
void atRs485TxISR(void);
void atRs485RxISR(void);
BOOL rs485ReceiverHasData(void);
WORD rs485ReadReceiverData(BYTE *pData, WORD maxSize);
BOOL rs485SendBinaryData(BYTE* pData, WORD wDataLen);
BOOL rs485SendCommand(BYTE* pData, WORD wDataLen );
BOOL IsRs485Sending(void);


#endif /*RS485_MODULE_H*/
