#ifndef TI_RADIO_MODULE_H_
#define TI_RADIO_MODULE_H_

#include "..\drivers.h"


BOOL InitRadioInterface( void);
void atZbTxISR( void );
void atZbRxISR( void );
BOOL ReceiverHasData(void);
WORD ReadReceiverData(BYTE *pData, WORD maxSize);
void ZbEnableFlowControl(void);
BOOL ZbTiSendBinaryData( BYTE* pData, WORD wDataLen );
BOOL ZbTiSendCommand( BYTE* pData, WORD wDataLen  );
BOOL IsZbTiSending(void);
BYTE GetRfChannel(void);
void PowerDownRadio(void);
void SetRadioChannel(void);
void IncreaseZbChannel(void);
void ResetRadio(void);
BOOL IsRadioActive(void);
void SetChangeChannelPending(BYTE channel);
BOOL IsChannelChangePending(void);
void SetNewChannel(void);
void SetRadioPower(BYTE txPower);
void SetRadioReceiverOff(void);
void SetRadioReceiverOn(void);
void SetRadioOffTimed(WORD msTimeoff);
void SetRadioTimeout(WORD msTimeout);

void SetRadioChannelForOAP(void);

void mplSetDoNotUseChannel(void);

#endif /*TI_RADIO_MODULE_H_*/
