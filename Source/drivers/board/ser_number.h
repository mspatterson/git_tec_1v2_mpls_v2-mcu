#ifndef SER_NUBMER_H_
#define SER_NUBMER_H_

#include "..\drivers.h"

#define SERNUMBER_DEVICE_PN		DS28CM00

#define SERNUMBER_ADDRESS	0x50

#define SN_RESET_ADDR				0X00


#define SerNumber_POWER_UP_TIME		50
#define SerNumber_WRITE_TIME			10
#define NBR_FIELD_RANGE				7

void SerNumberInit(void);
void SerNumberReadDataStart(void);
BOOL SerNumberDataReady(void);
BOOL SerNumberTxDone(void);
void SerNumberRegisterDump(void);
void SerNumberGetRawData(BYTE *pData, WORD maxSize);
void SerNumberEnable(void);
void SerNumberDisable(void);
BYTE* SerNumberGetDataPointer(void);
int* SerNumberGetDataPointerInt(void);
void SerNumberReadId(void);
void SerNumberPowerDown(void);
BYTE CalcSnCRC(BYTE *pData, WORD size);

#endif /*SER_NUBMER_H_*/
