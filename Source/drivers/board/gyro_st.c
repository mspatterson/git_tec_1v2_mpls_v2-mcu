/*
 * gyro_st.c
 *
 *  Created on: 2012-06-11
 *      Author: Owner
 */

#include "gyro_st.h"

#define ST_GYRO_250DPS_SC	875
#define ST_GYRO_UPDATE_FREQ	106	//100
#define ST_GYRO_ZERO_RATE_P	300
#define ST_GYRO_ZERO_RATE_N	-300
#define SELF_TEST_VALUE		13000000
#define SELF_TEST_VALUE_2K	53000000

unsigned char SToutBuff[MAX_ST_OUT_SIZE];
unsigned char SToutPtr;
unsigned char STtxSize;			

unsigned char STinBuff[MAX_ST_IN_SIZE];
unsigned char STinPtr;

static int	zeroStValue;
static long i32ZeroLevel;
static int	iTempValue;
static char	stGyroID;
//static WORD wCounter;
float currentSTz;							// hold new value of every ST z update
float accSTz;								// hold incremented value of z when moving. Is reported when movement stops
float avgSTz;
float STzBuff[ST_AVG_BUFF_SIZE];			// holds the last x "currentSTz" values
float zeroAvgz;								// average value of the device when not moving.
signed long i32Temp;
signed long coeff130dps;
signed long coeffLSB;
static int iStZeroRateP;
static int iStZeroRateN;
static long rpmStGyro;

typedef struct ST_GYRO_SELECT
{
	BYTE pinSel;
	BYTE pinDrdy;
	BYTE pinInt;
}ST_GYRO_SEL;

static const ST_GYRO_SEL stGyroSel ={L3G_CS_1,L3G_1_DRDY,L3G_1_INT};
static TIMERHANDLE thGyroTimer = 0;
 
WORD digGyroMoveCounter;
WORD digGyroZeroCounter;
WORD digGyroLastMovePoint;
extern WORD digGyroSampleCounter;

void initST(void)
{
	//unsigned char cnt;
	SetPinAsOutput(L3G_CS_1);
	SetPinAsInput(L3G_1_DRDY);
	
	SetOutputPin(L3G_CS_1, TRUE);			// disable chip select

	rpmStGyro = 0;
	
	SpiB0Initialize();
	if(thGyroTimer ==0)
	{
		thGyroTimer = RegisterTimer();
	}
	ResetTimer( thGyroTimer, ST_WAKEUP_PERIOD );
	StartTimer( thGyroTimer ); 
	while(!TimerExpired( thGyroTimer))
	{
		ResetWatchdog();
	}
	setSTreg1();
	setSTreg3();

	setSTNormalMode();

}


void startSPI4ST(void)
{
	SToutPtr = 0;
	STinPtr = 0;
//	P2OUT &= ~ST_CSn;
//	SetOutputPin(L3G_CS_1, FALSE);			// enable chip select
	SpiB0TxRxPacket((BYTE *)SToutBuff, (WORD)STtxSize);
//	SetOutputPin(L3G_CS_1, TRUE);			// disable chip select
	
}
	
void readSTtemp(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = ST_READ + GET_TEMP_CMD;
	SToutBuff[STtxSize++] = FILLER;
	startSPI4ST();
}

void setSTreg1(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = SET_REG1;
	SToutBuff[STtxSize++] = 0x0C;		// set PD and Zen to 1
	SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
	startSPI4ST();
	SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
}

void setSTreg3(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = SET_REG3;
	SToutBuff[STtxSize++] = 0x0C;		// Enable Data ready and watermark
//	SToutBuff[STtxSize++] = 0x08;		// Enable Data ready
//	SToutBuff[STtxSize++] = 0x04;		// Enable watermark 
	SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
	startSPI4ST();
	SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
}

void setSTreg3NormalMode(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = SET_REG3;
	SToutBuff[STtxSize++] = 0x04;		// Enable watermark 
	SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
	startSPI4ST();
	SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
}

void PowerDownStGyro(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = SET_REG1;
	SToutBuff[STtxSize++] = 0x00;		// set Power Down
	SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
	startSPI4ST();
	SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
}

void setSTreg5(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = SET_REG5;
	SToutBuff[STtxSize++] = FIFO_ENABLE;		// enable FIFO
	SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
	startSPI4ST();
	SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
}


void WakeUpStGyro(void)
{
	setSTreg1();
	setSTreg3();
}

void setSTSelfTest(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = SET_REG4;
#ifdef ST_GYRO_250DPS
	SToutBuff[STtxSize++] = SET_SELF_TEST;		// set PD and Zen to 1
#endif
#ifdef ST_GYRO_2000DPS	
	SToutBuff[STtxSize++] = SET_SELF_TEST_2K;	// set to 2K dps self test
#endif	 
	SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
	startSPI4ST();
	SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
}

void setSTNormalMode(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = SET_REG4;
	#ifdef ST_GYRO_250DPS
	SToutBuff[STtxSize++] = 0x00;			// set 250 dps
	#endif
	#ifdef ST_GYRO_2000DPS		
	SToutBuff[STtxSize++] = SET_2K_DPS;		// set 2000dps
	#endif
	SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
	startSPI4ST();
	SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
}

void setSTfifo(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = SET_FIFO;
	SToutBuff[STtxSize++] = 0x21;		// FIFO mode 1 and watermark level1
//	SToutBuff[STtxSize++] = 0x22;		// FIFO mode 1 and watermark level 2
	SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
	startSPI4ST();	
	SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
}

void getSTz(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = OUT_Z_L + ST_READ + ST_MULTI_ADS;
	SToutBuff[STtxSize++] = FILLER;
	SToutBuff[STtxSize++] = FILLER;
	startSPI4ST();	
}


void calibrateStGyro(void)
{
	int i;
	
	i=0;
	zeroStValue = 0;
	i32ZeroLevel = 0;
	i32Temp = 0;
	iTempValue = 0;
	setSTNormalMode();
	ResetTimer( thGyroTimer, 100 );
	StartTimer( thGyroTimer ); 
	while(!TimerExpired( thGyroTimer))
	{
#ifdef WATCHDOG_ENABLE
			ResetWatchdog();
#endif
	}
//	while (i<1000)
	while (i<500)
	{
		while( ReadInputPin(stGyroSel.pinDrdy) == FALSE )
		{
			ResetWatchdog();
		}
		ResetTimer( thGyroTimer, 2);
		StartTimer( thGyroTimer ); 
		while(!TimerExpired( thGyroTimer));
		SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
		getSTz();
		SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
		iTempValue = SToutBuff[2] << 8;
		iTempValue += SToutBuff[1];
		i++;
		i32Temp =(signed long)iTempValue;
		i32Temp *=coeffLSB;
		i32Temp /= 100;
		i32ZeroLevel = (i32ZeroLevel*15+i32Temp)/16;

//		i32ZeroLevel = (i32ZeroLevel*63+i32Temp)/64;
//		lZeroTmp= (lZeroTmp*15 + iTempValue)/16;
//		zeroStValue = (zeroStValue*3 +iTempValue)/4;
//		zeroStValue = (zeroStValue +iTempValue)/2; 
		ResetWatchdog();

	}
	zeroStValue = (int)i32ZeroLevel; 
//	zeroStValue = (int)lZeroTmp; 
//	iStZeroRateP = zeroStValue + zeroStValue *((zeroStValue>0)? 5: -5);
//	iStZeroRateN = zeroStValue + zeroStValue *((zeroStValue>0)? -5: 5);
	iStZeroRateP = zeroStValue + zeroStValue *((zeroStValue>0)? 5: -5);
	iStZeroRateN = zeroStValue + zeroStValue *((zeroStValue>0)? -5: 5);

}


void readSTid(void)
{
	STtxSize = 0;
	SToutBuff[STtxSize++] = ID_REG + ST_READ;
	SToutBuff[STtxSize++] = FILLER;
	SToutBuff[STtxSize++] = FILLER;
	SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
	startSPI4ST();
	SetOutputPin(stGyroSel.pinSel, TRUE);			// disable chip select
	stGyroID = 	SToutBuff[1];
	
}


char gedStId(void)
{
	return stGyroID;
}

// gNumber is 0,1,2
BOOL stGyroHasData(void)
{
	return (ReadInputPin(stGyroSel.pinDrdy));
}

// result in mdps
// return TRUE if it's moving
// read 2 times and add - to be done!!!
//BOOL stGyroReadData( int *pResultRPM,int *pResultDPS, BYTE gNumber)
//{
//	BOOL bMoving = FALSE;
//	iTempValue = 0;
//	SetOutputPin(stGyroSel[gNumber].pinSel, FALSE);			// enable chip select
//	getSTz();
//	SetOutputPin(stGyroSel[gNumber].pinSel, TRUE);			// disable chip select
//	iTempValue = SToutBuff[2] << 8;
//	iTempValue += SToutBuff[1];
////	if (iTempValue > iStZeroRateP)
////		bMoving = TRUE;
////	if (iTempValue < iStZeroRateN)
////		bMoving = TRUE;
////
////	if(bMoving == FALSE)
////		return bMoving;
//
////	iTempValue -= zeroStValue;
//
////	if(iTempValue <0)
////		iTempValue = iTempValue *(-1);
//
//	i32Temp = (signed long)iTempValue;
//	i32Temp *=coeffLSB;
//	i32Temp /= 100;
//	i32Temp -=zeroStValue;
//	if (i32Temp> 1100)
//		bMoving = TRUE;
//	if (i32Temp < -1100)
//		bMoving = TRUE;
//
//	*pResultRPM = (int)(i32Temp/6);
////	if(*pResultRPM <0)
////		*pResultRPM =(*pResultRPM) *(-1);
//
//	i32Temp /= ST_GYRO_UPDATE_FREQ;
//
//	*pResultDPS = (int)i32Temp;
//
//	return bMoving;
//}

BOOL stGyroReadData( int *pResultRPM)
{
	BOOL bMoving = FALSE;
	iTempValue = 0;
	SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
	getSTz();
	SetOutputPin(stGyroSel.pinSel, TRUE);			// disable chip select
	iTempValue = SToutBuff[2] << 8;
	iTempValue += SToutBuff[1];
//	if (iTempValue > iStZeroRateP)
//		bMoving = TRUE;
//	if (iTempValue < iStZeroRateN)
//		bMoving = TRUE;
//	
//	if(bMoving == FALSE)
//		return bMoving;
		
//	iTempValue -= zeroStValue;

//	if(iTempValue <0)
//		iTempValue = iTempValue *(-1);
				
	i32Temp = (signed long)iTempValue;
	i32Temp *=coeffLSB;
	i32Temp /= 100;
	i32Temp -=zeroStValue;
	if (i32Temp> 1100)
		bMoving = TRUE;
	if (i32Temp < -1100)
		bMoving = TRUE;
	
//	*pResultRPM = (int)(i32Temp/6);
	*pResultRPM = (int)(i32Temp/6000);	// convert miliDPS in RPM

	if(*pResultRPM <0)
		*pResultRPM =(*pResultRPM) *(-1);
		

	return bMoving;	 
}


void SelfTestStGyro(void)
{
	int i;
	
	i=0;
	coeff130dps = 0;
	iTempValue = 0;

	setSTSelfTest();
	ResetTimer( thGyroTimer, 100 );
	StartTimer( thGyroTimer ); 
	while(!TimerExpired( thGyroTimer))
	{
			ResetWatchdog();
	}
	while (i<50)
	{
		while( ReadInputPin(stGyroSel.pinDrdy) == FALSE )
		{
			ResetWatchdog();
		}
		ResetTimer( thGyroTimer, 2);
		StartTimer( thGyroTimer ); 
		while(!TimerExpired( thGyroTimer));
		SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
		getSTz();
		SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
		i++;
		ResetWatchdog();
	}
	i=0;
	while (i<200)
	{
		while( ReadInputPin(stGyroSel.pinDrdy) == FALSE )
		{
			ResetWatchdog();
		}
//		thGyroTimer = RegisterTimer();	
		ResetTimer( thGyroTimer, 2);
		StartTimer( thGyroTimer ); 
		while(!TimerExpired( thGyroTimer));
		SetOutputPin(stGyroSel.pinSel, FALSE);			// enable chip select
		getSTz();
		SetOutputPin(stGyroSel.pinSel, TRUE);			// enable chip select
		iTempValue = SToutBuff[2] << 8;
		iTempValue += SToutBuff[1];
		i++;
//		coeff130dps= (coeff130dps +iTempValue)/2; 
//		coeff130dps= (coeff130dps*15 +iTempValue)/16;
		coeff130dps= (coeff130dps*7 +iTempValue)/8;
		ResetWatchdog();
	}
	
	#ifdef ST_GYRO_250DPS
	coeff130dps = SELF_TEST_VALUE /coeff130dps;
	#endif
	#ifdef ST_GYRO_2000DPS
	coeff130dps = SELF_TEST_VALUE_2K /coeff130dps;
	#endif
	if(coeff130dps<0)
		coeffLSB = (int)(coeff130dps *(-1));
	else
		coeffLSB = (int)coeff130dps;
		


}



void GetSTGyroData(void)
{

	int iStGyroRPMtmp = 0;
	if (stGyroReadData(&iStGyroRPMtmp))
	{
//		rpmStGyro = (rpmStGyro*3+iStGyroRPMtmp) /4;
		rpmStGyro = iStGyroRPMtmp;
//			digGyroMoveCounter++;
//			digGyroLastMovePoint = digGyroSampleCounter;
	}
	else
	{
		rpmStGyro = 0;
//			digGyroZeroCounter++;
	}
}

BYTE GetRPM(void)
{

	return (BYTE) rpmStGyro;

}




// Port 1 interrupt service routine
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{

//  if(P1IFG&0x80)							// interrupt from P1.7-L3G_DRDY/INT2
//  {
//
//  }
//
//  P1IFG &= ~0x80;                          // P1.7 IFG cleared
}

// setup interrupt on P1.7 - L3G_DRDY/INT2
void SetSTGyroInterrupt(void)
{
	__bic_SR_register(GIE);
	P1IE |= 0x80;                             // P1.7 interrupt enabled
	P1IES &= ~0x80;                           // P1.7 Lo/Hi edge
  	P1IFG &= ~0x80;                           // P1.7 IFG cleared
  	__bis_SR_register(GIE);
}
