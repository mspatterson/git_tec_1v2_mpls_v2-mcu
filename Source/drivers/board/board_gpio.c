/*
 * board_gpio.c
 *
 *  Created on: 2012-12-03
 *      Author: DD
 */
#include "board_gpio.h"

#define DEBOUNCE_TIME	500
#define SWITCH_ON	0x00
#define SWITCH_OFF	0xFF
#define SWITCH_HIGH		TRUE
#define SWITCH_LOW		FALSE
static BYTE swOnState;
//static BYTE swOffState;
static BYTE swOldState;
static BYTE swNewState;
static TIMERHANDLE thDebounce = 0;
static BOOL swDebounced;

DEVICE_ROLE boardDeviceRole;

//******************************************************************************
//
//  Function: ReedSwitchInit
//
//  Arguments:  none
//
//  Returns: none
//
//  Description: This function initialize the switch input
//               and register a timer for debouncing
//               
//
//******************************************************************************
void ReedSwitchInit(void)
{
	// set the pin as input
	SetPinAsInput(REED_SWITCH);	
	if(thDebounce==0)
	{
		thDebounce = RegisterTimer();
	}
	//	swNewState = swOldState = SWITCH_ON;

	GetSwitchInitState();
	swDebounced = TRUE;
}

//******************************************************************************
//
//  Function: GetSwitchInitState
//
//  Arguments:	none
//
//  Returns:	none
//
//  Description: This function determine the initial state of the
//               switch - HIGH/LOW
//
//
//******************************************************************************
void GetSwitchInitState(void)
{
	if(ReadInputPin(REED_SWITCH))
	{
		swNewState = SWITCH_HIGH;
		swOldState = SWITCH_HIGH;
	}
	else
	{
		swNewState = SWITCH_LOW;
		swOldState = SWITCH_LOW;
	}
	ResetTimer( thDebounce, DEBOUNCE_TIME );
	StartTimer(thDebounce);
	while(!TimerExpired( thDebounce))
	{
		if(ReadInputPin(REED_SWITCH))
		{
			swNewState = SWITCH_HIGH;
		}
		else
		{
			swNewState = SWITCH_LOW;
		}

		if(swNewState!=swOldState)
		{
			ResetTimer( thDebounce, DEBOUNCE_TIME );
			swOldState = swNewState;
		}
		ResetWatchdog();
	}

	swOnState = swNewState;
}


//******************************************************************************
//
//  Function: IsSwitchOn
//
//  Arguments:
//    none
//
//  Returns: TRUE if the switch has not changed his state and
//			 FALSE if the switch has changed his state
//  Description: This function read and debounce the SWITCH input
//               When the switch has changed his state the board goes to power down
//               
//
//******************************************************************************
BOOL IsSwitchOn(void)
{

	if(ReadInputPin(REED_SWITCH))
	{
		swNewState = SWITCH_HIGH;
	}	
	else
	{
		swNewState = SWITCH_LOW;
	}

	if(swNewState != swOldState)
	{
		ResetTimer( thDebounce, DEBOUNCE_TIME );
		StartTimer(thDebounce);
		swOldState = swNewState ;
		swDebounced = FALSE;
	}
	else
	{
		if(TimerExpired( thDebounce))
			swDebounced = TRUE;
	}

	if(swDebounced)
	{
		if(swNewState == swOnState)
			return TRUE; 
		else
			return FALSE;

	}
	else
		return TRUE;		// during transition reports Switch On
}


//******************************************************************************
//
//  Function: SetReedSwitchInterrupt
//
//  Arguments: none
//
//  Returns: none
//
//  Description: setup interrupt on REED_SWITCH (P2.5) toggle.
//               check the current switch state and setup the edge accordingly.
//				 If the input is high - setup High/Low edge
//				 If the input is low  - setup Low/High edge
//******************************************************************************
void SetReedSwitchInterrupt(void)
{
	__bic_SR_register(GIE);
	P2IE |= 0x20;                             // P2.5 interrupt enabled
	if(ReadInputPin(REED_SWITCH) == TRUE)
		P2IES |= 0x20;                           // set P2.5 High/Low edge
	else
		P2IES &= ~0x20;                           // set P2.5 Low/High edge

	P2IFG = 0x00;                           // P2.5 IFG cleared
	__bis_SR_register(GIE);
}

//******************************************************************************
//
//  Function: DisableReedSwitchInterrupt
//
//  Arguments: none
//
//  Returns: none
//
//  Description: disable interrupt on REED_SWITCH (P2.5) toggle.
//
//******************************************************************************
void DisableReedSwitchInterrupt(void)
{
	__bic_SR_register(GIE);
	P2IE &= ~0x20;                             // P2.5 interrupt disabled
	//	P2IES &= ~0x20;                           // P2.5 Lo/Hi edge
	P2IFG &= ~0x20;                           // P2.5 IFG cleared
	__bis_SR_register(GIE);
}

//******************************************************************************
//
//  Function: Port_2
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Port 2 interrupt service routine
//
//******************************************************************************
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{

	if(P2IFG&0x20)							// interrupt from P2.5-REED_SWITCH
	{
		P2IFG &= ~0x20;                          // P2.5 IFG cleared
		LPM4_EXIT;
	}
	else
	{
		P2IFG = 0;
	}

}


//******************************************************************************
//
//  Function: EnableAnalogPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Enable Analog Power Supply
//
//******************************************************************************
void EnableAnalogPower(void)
{
	SetPinAsOutput(EN_VA);
	SetOutputPin(EN_VA,TRUE);
}

//******************************************************************************
//
//  Function: DisableAnalogPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Disable Analog Power Supply
//
//******************************************************************************
void DisableAnalogPower(void)
{
	SetPinAsOutput(EN_VA);
	SetOutputPin(EN_VA,FALSE);
}

//******************************************************************************
//
//  Function: EnableDigitalPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Enable Digital Power Supply
//
//******************************************************************************
void EnableDigitalPower(void)
{
	SetPinAsOutput(EN_VD);
	SetOutputPin(EN_VD,TRUE);
}

//******************************************************************************
//
//  Function: DisableDigitalPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Disable Digital Power Supply
//
//******************************************************************************
void DisableDigitalPower(void)
{
	SetPinAsOutput(EN_VD);
	SetOutputPin(EN_VD,FALSE);
}

//******************************************************************************
//
//  Function: EnableRadioPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Enable Radio Power Supply
//
//******************************************************************************
void EnableRadioPower(void)
{
	SetPinAsOutput(EN_VR);
	SetOutputPin(EN_VR,TRUE);
}

//******************************************************************************
//
//  Function: DisableRadioPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Disable Radio Power Supply
//
//******************************************************************************
void DisableRadioPower(void)
{
	SetPinAsOutput(EN_VR);
	SetOutputPin(EN_VR,FALSE);
}

//******************************************************************************
//
//  Function: EnableAnalogGyro
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Enable Analog Gyro Power Supply
//
//******************************************************************************
void EnableAnalogGyro(void)
{
	SetOutputPin(EN_AG,TRUE);
}


//******************************************************************************
//
//  Function: DisableAnalogGyro
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Disable Analog Gyro Power Supply
//
//******************************************************************************
void DisableAnalogGyro(void)
{
	SetOutputPin(EN_AG,FALSE);
}

//******************************************************************************
//
//  Function: GetHardwareRev
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Read the 3 HW_REV inputs to determine the hardware revision
//
//******************************************************************************
BYTE GetHardwareRev(void)
{
	static BYTE byVar;
	byVar = 0;
	SetPinAsInput(HWD_REV_IN1);
	SetPinAsInput(HWD_REV_IN2);
	SetPinAsInput(HWD_REV_IN0);

	if(ReadInputPin(HWD_REV_IN0))
		byVar = 1;
	if(ReadInputPin(HWD_REV_IN1))
		byVar |= 1<<1;
	if(ReadInputPin(HWD_REV_IN2))
		byVar |= 1<<2;

	//*pByte = byVar;
	return byVar;
}

//******************************************************************************
//
//  Function: EnableAllPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Enable All Power Supplies
//
//******************************************************************************
void EnableAllPower(void)
{
	EnableAnalogPower();
	EnableDigitalPower();
	EnableRadioPower();
//	EnableAnalogGyro();

}

//******************************************************************************
//
//  Function: DisableAllPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Disable All Power Supplies
//
//******************************************************************************
void DisableAllPower(void)
{
	DisableAnalogPower();
	DisableDigitalPower();
	DisableRadioPower();
	DisableAnalogGyro();
}


//******************************************************************************
//
//  Function: EnableBatSense
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Enable Battery Sense Voltage Divider
//
//******************************************************************************
void EnableBatSense(void)
{
	SetPinAsOutput(BAT_SENSE_EN);
	SetOutputPin(BAT_SENSE_EN,TRUE);
}

//******************************************************************************
//
//  Function: DisableBatSense
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Disable Battery Sense Voltage Divider
//
//******************************************************************************
void DisableBatSense(void)
{
	SetPinAsOutput(BAT_SENSE_EN);
	SetOutputPin(BAT_SENSE_EN,FALSE);
}


//******************************************************************************
//
//  Function: LedSwPowerUpSequence
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Flash the switch LED for power up
//
//******************************************************************************
void LedSwPowerUpSequence(void)
{
	BOOL boolVar;
	BYTE bVar;
	boolVar = FALSE;
	bVar = 6;
	if(thDebounce==0)
	{
		thDebounce = RegisterTimer();
	}
	while(bVar--)
	{
		boolVar ^=1;
		ResetTimer( thDebounce, 500 );
		StartTimer( thDebounce );
		while(TimerExpired( thDebounce)==FALSE)
		{
			ResetWatchdog();
		}
		SetOutputPin(SW_LED, boolVar );
	}
	SetOutputPin(SW_LED, FALSE );
}

//******************************************************************************
//
//  Function: LedSwPowerDownSequence
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Flash the switch LED for power down - 3 times, once per second
//
//******************************************************************************
void LedSwPowerDownSequence(void)
{
	WORD cycleCounter;
	register WORD pwmCounter = 0;
	volatile WORD pwmOnCycle;
	cycleCounter = 368;
	if(thDebounce==0)
	{
		thDebounce = RegisterTimer();
	}
	pwmOnCycle = 740;
	StartTimer( thDebounce );
	while(cycleCounter--)
	{
		ResetTimer( thDebounce, 10 );
		while(TimerExpired( thDebounce)==FALSE)
		{
			ResetWatchdog();
			pwmCounter++;
			if(pwmCounter>pwmOnCycle)
				SetOutputPin(SW_LED, FALSE );
		}

		SetOutputPin(SW_LED, TRUE );
		pwmCounter = 0;
		pwmOnCycle -=2;

	}
	SetOutputPin(SW_LED, FALSE );
}
//void LedSwPowerDownSequence(void)
//{
//	BOOL boolVar;
//	BYTE bVar;
//	boolVar = FALSE;
//	bVar = 6;
//	if(thDebounce==0)
//	{
//		thDebounce = RegisterTimer();
//	}
//	while(bVar--)
//	{
//		boolVar ^=1;
//		ResetTimer( thDebounce, 500 );
//		StartTimer( thDebounce );
//		while(TimerExpired( thDebounce)==FALSE)
//		{
//			ResetWatchdog();
//		}
//		SetOutputPin(SW_LED, boolVar );
//	}
//	SetOutputPin(SW_LED, FALSE );
//}

//******************************************************************************
//
//  Function: EnableExtPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Enable External Power Supply
//
//******************************************************************************
void EnableExtPower(void)
{
	SetPinAsOutput(EN_V_EXT);
	SetOutputPin(EN_V_EXT,TRUE);
	SetPinAsOutput(RS485_PWR_EN);
    SetOutputPin(RS485_PWR_EN,FALSE);
}



//******************************************************************************
//
//  Function: DisableExtPower
//
//  Arguments: none
//
//  Returns: none
//
//  Description:  Disable External Power Supply
//
//******************************************************************************
void DisableExtPower(void)
{
	SetPinAsOutput(EN_V_EXT);
	SetOutputPin(EN_V_EXT,FALSE);
}



//******************************************************************************
//
//  Function: ReadDeviceRole
//
//  Arguments: none
//
//  Returns:
//
//  Description:  reads pin SW1 determine the device role: MPLS_MASTER(pin is '1') or MPLS_SLAVE(pin is '0').
//				This function must be called only at power up.
//******************************************************************************
void ReadDeviceRole(void)
{
#ifdef  MASTER_TEC
    boardDeviceRole = MPLS_MASTER;
#elif SLAVE_TEC
    boardDeviceRole = MPLS_SLAVE;

#else

	if(ReadInputPin(MS_SETUP_IN))
	{
		boardDeviceRole = MPLS_MASTER;
	}
	else
	{
		boardDeviceRole = MPLS_SLAVE;
	}
#endif
}


//******************************************************************************
//
//  Function: GetDeviceRole
//
//  Arguments: none
//
//  Returns: MPLS_MASTER or MPLS_SLAVE
//
//  Description:  return the device role variable set in ReadDeviceRole
//
//******************************************************************************
BOOL IsDeviceMaster(void)
{

    if(boardDeviceRole == MPLS_MASTER)
    {
        return TRUE;
    }

    return FALSE;

}

