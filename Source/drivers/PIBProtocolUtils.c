//******************************************************************************
//
// PIBProtocolUtils.c: PIB Communication protocol routines
//
//  Copyright (c) 2018, Microlynx Systems Ltd.
//  ALL RIGHTS RESERVED
//
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2016-Oct-04     DD        Initial Version
//  2018-Nov-23     DD         Updated for MPLS firmware.
//******************************************************************************

#include "PIBProtocolUtils.h"

//#pragma package(smart_init)


//
// Private Declarations
//

#define DEFAULT_SOL_TIMEOUT		10		// seconds

#define MPLS_NBR_RSP			12   // Total number of slave board responses... defines
#define SLAVE_TEC_CMD_NBR       3   // Total number of Slave TEC commands... defines
#define RS485_PKT_NBR			80

#define MPL_COMM_TIMEOUT		110	//ms
#define MPL_COMM_GAP            20 //ms
#define MPL_BOOT_TIME           3000
#define MPL_SLAVE_BOOT_TIME     100


extern UVOLTS uvResultAverage[ADS1220_TOTAL];

static TIMERHANDLE thPibTimer;

typedef struct {
    BYTE pktCommHdr;        // Expected header byte
    BYTE cmdRespByte;   	// Could be command, event or response
    BYTE expDataLen;		// Expected Payload Length
} PIB_PKT_INFO;

// Set of command responses received by the Master TEC
static const PIB_PKT_INFO m_pktInfo[ MPLS_NBR_RSP] = {
	{ TEC_S_RESP_HDR, TEC_S_GET_DATA,   sizeof(TEC_S_GET_DATA_RSP_PKT) },
    { TEC_S_RESP_HDR, TEC_S_SET_PARAMS, sizeof(TEC_S_SET_PARAM_RSP_PKT) },
	{ TEC_S_RESP_HDR, COM_GET_VERSIONS, sizeof(COM_GET_VERSIONS_RSP_PKT) },
	{ PIB_M_RESP_HDR, PIB_SET_PARAMS,   sizeof(PIB_RSP_PKT) },
    { PIB_M_RESP_HDR, PIB_GET_DATA,     sizeof(PIB_RSP_PKT) },
	{ PIB_M_RESP_HDR, COM_GET_VERSIONS, sizeof(COM_GET_VERSIONS_RSP_PKT) },
	{ PIB_S_RESP_HDR, PIB_SET_PARAMS,   sizeof(PIB_RSP_PKT) },
    { PIB_S_RESP_HDR, PIB_GET_DATA,     sizeof(PIB_RSP_PKT) },
	{ PIB_S_RESP_HDR, COM_GET_VERSIONS, sizeof(COM_GET_VERSIONS_RSP_PKT) },
	{ PTS_RESP_HDR,   PTS_GET_DATA,     sizeof(PTS_GET_DATA_RSP_PKT) },
	{ PTS_RESP_HDR,   PTS_SET_PARAMS,   sizeof(PTS_SET_PARAM_RSP_PKT) },
	{ PTS_RESP_HDR,   COM_GET_VERSIONS, sizeof(COM_GET_VERSIONS_RSP_PKT) },
};

// Set of command responses received by the Slave TEC
static const PIB_PKT_INFO m_pktInfoSlaveTEC[ SLAVE_TEC_CMD_NBR] = {
    { TEC_S_CMD_HDR,  TEC_S_GET_DATA,   0 },
    { TEC_S_CMD_HDR,  TEC_S_SET_PARAMS, sizeof(TEC_S_SET_PARAM_CMD_PKT) },
    { TEC_S_CMD_HDR,  COM_GET_VERSIONS, 0 },
};


static BYTE rs485Packet[RS485_PKT_NBR];
static BOOL havePIBPkt = FALSE;
static WORD tmpWord;

static COMM_PKT_HDR *pktCommHdr;
static BYTE *pktPibData;
static BYTE *pCommCRC;

static PIB_SET_PARAM_CMD_PKT *pPibParamCmd;
static PTS_SET_PARAM_CMD_PKT *pPtsParamCmd;
static TEC_S_SET_PARAM_RSP_PKT *pTecParamCmd;

static BYTE pibCommBuff[128];

static BYTE dbgArray[64];
static BYTE dbgCnt;
// Master TEC wired communication state machine.
typedef enum{
	MTEC_INIT,
	MTEC_MASTER_PIB_INIT_CMD,
	MTEC_MASTER_PIB_INIT_RESP,
	MTEC_SLAVE_TEC_INIT_CMD,
	MTEC_SLAVE_TEC_INIT_RESP,
	MTEC_SLAVE_PIB_INIT_CMD,
	MTEC_SLAVE_PIB_INIT_RESP,
	MTEC_PLUG_SENS_INIT_CMD,
	MTEC_PLUG_SENS_INIT_RESP,
	MTEC_COMM_IDLE,
	MTEC_MASTER_PIB_GET_DATA_CMD,
	MTEC_MASTER_PIB_GET_DATA_RESP,
	MTEC_MASTER_PIB_SET_PARAMS_CMD,
	MTEC_MASTER_PIB_SET_PARAMS_RESP,
	MTEC_SLAVE_PIB_GET_DATA_CMD,
	MTEC_SLAVE_PIB_GET_DATA_RESP,
	MTEC_SLAVE_PIB_SET_PARAMS_CMD,
	MTEC_SLAVE_PIB_SET_PARAMS_RESP,
	MTEC_SLAVE_TEC_GET_DATA_CMD,
	MTEC_SLAVE_TEC_GET_DATA_RESP,
	MTEC_SLAVE_TEC_SET_PARAMS_CMD,
	MTEC_SLAVE_TEC_SET_PARAMS_RESP,
	MTEC_PTS_GET_DATA_CMD,
	MTEC_PTS_GET_DATA_RESP,
	MTEC_PTS_SET_PARAMS_CMD,
	MTEC_PTS_SET_PARAMS_RESP,
	NBR_MTEC_COMM_STATES
}MTEC_COMM_STATE;

MTEC_COMM_STATE	mplCommState;


typedef enum {
    MTEC_CTRL_IDLE,
    MTEC_CTRL_PIB_M,
    MTEC_CTRL_PTS,
    MTEC_CTRL_TEC_S,
    MTEC_CTRL_PIB_S,
    MTEC_CTRL_PIB_M_PARAMS,
    MTEC_CTRL_PIB_S_PARAMS,
    MTEC_CTRL_EXT_CMD,
    NBR_MTEC_CTRL_STATES
}MTEC_CONTROL_STATE;

MTEC_CONTROL_STATE mtecCtrlState;

// OCT 30 2018
const BYTE pibHeaderSel[NBR_PIBS] = {PIB_M_CMD_HDR,PIB_S_CMD_HDR};
const BYTE slaveBoardHdr[TOTAL_NBR_SLAVES] =  { PIB_M_CMD_HDR, PIB_S_CMD_HDR,TEC_S_CMD_HDR, PTS_CMD_HDR};
BOOL slaveBoardsAvailAtPowerOn[TOTAL_NBR_SLAVES];
BOOL slaveBoardsAvail[TOTAL_NBR_SLAVES];

COM_GET_VERSIONS_RSP_PKT slaveBoardsRevs[TOTAL_NBR_SLAVES];

//static BYTE SlaveTecAdcAveraging[MASTER_ADC_CHNLS];
static BOOL haveRs485Cmd = FALSE;

/******************************************************************************
 * Variables holding the slave boards' data and parameters.
 *******************************************************************************/
static PIB_RSP_PKT	            pibBoardsData[NBR_PIBS];
static PIB_SET_PARAM_CMD_PKT    sPibParamSet[NBR_PIBS];
static TEC_S_GET_DATA_RSP_PKT   sSlaveTECData;
static TEC_S_SET_PARAM_CMD_PKT  sSlaveTecParamsSet;
static TEC_S_SET_PARAM_RSP_PKT  sSlaveTecParamsRsp;
static PTS_GET_DATA_RSP_PKT     sPtsDataRsp;    // response from PTS over RS485
static PTS_SET_PARAM_CMD_PKT    sPtsParamSet;   // set from the PC over the radio
static PTS_SET_PARAM_RSP_PKT    sPtsParamRsp;   // response from PTS over RS485.

/******************************************************************************
 * Variables controlling the commands responses and data gathering process.
 *******************************************************************************/
static BYTE CommandRs485Post;   // This holds the next RS485 command than needs to be executed.
static BYTE CommandRs485Rsp;    // This holds the response from the last RS485 command.
static BOOL StartDataCycle;     // When set the Master TEC will start collecting data from slaves.

static BYTE retryCnt;


/***********************************************************************************
 *  Local functions
 ***********************************************************************************/

void ParsePIBRsp(PIB_RSP_PKT *pibRespData,SlaveBoardsT pibBoard);
void ParseSlaveTecData(TEC_S_GET_DATA_RSP_PKT * pktData);
void ParseSlaveTecParams(TEC_S_SET_PARAM_RSP_PKT * pktData);
void ParsePtsData(PTS_GET_DATA_RSP_PKT * pktData);
void ParsePtsSetParams(PTS_SET_PARAM_RSP_PKT* pktData);








//******************************************************************************
//
//  Function: PostRs484Command
//
//  Arguments: BYTE bCmd - command type
//
//  Returns: none
//
//  Description: This function will be called from the Radio interface when RS485 command needs to be executed.
//
//******************************************************************************
void PostRs485Command(BYTE bCmd)
{
    CommandRs485Post = bCmd;
    CommandRs485Rsp = 0;
}

//******************************************************************************
//
//  Function: IsRs485RspAvail
//
//  Arguments: none
//
//  Returns: BYTE Response from the last RS485 command. 0 if none.
//
//  Description: Returns the status of RS485 response
//
//******************************************************************************
BYTE IsRs485RspAvail(void)
{
    return CommandRs485Rsp;
}

//******************************************************************************
//
//  Function: StartDataCollection
//
//  Arguments: none
//
//  Returns: none
//
//  Description: Will start a data collection cycle from all slave boards.
//
//******************************************************************************
void StartDataCollection(void)
{
    StartDataCycle = TRUE;
}

//******************************************************************************
//
//  Function: InitPIBInterface
//
//  Arguments: none
//
//  Returns: none
//
//  Description: Initialization routines.
//
//******************************************************************************
void InitPIBInterface(void)
{
    WORD i;

    if(IsDeviceMaster())
    {
        InitRs485Interface(BAUD_RATE_TEC, RS845_TX_EN);
    }
    else
    {
        InitRs485Interface(BAUD_RATE_TEC, RS845_RX_EN);
    }

	thPibTimer = RegisterTimer();
	StartTimer( thPibTimer );

	mplCommState = MTEC_INIT;
    if(IsDeviceMaster())
    {
        ResetTimer(thPibTimer,MPL_BOOT_TIME);
    }
    else
    {
        ResetTimer(thPibTimer,MPL_SLAVE_BOOT_TIME);
    }

#ifdef	PLUG_EMULATION

    for(i=0;i<PTS_DATA_SIZE;i++)
    {
        sPtsDataRsp.sensData[i] = i;

    }

#endif
}



//******************************************************************************
//
//  Function: PibGetData
//
//  Arguments: none
//
//  Returns: none
//
//  Description: send command "Get PIB Data". It's zero data length command.
//
//******************************************************************************
void PibGetData(BYTE pibSelect)
{
	if((pibSelect != PIB_M_BOARD)&&(pibSelect !=  PIB_S_BOARD))
		return;

	InitRs485Interface(BAUD_RATE_PIB, RS845_TX_EN);

	// Initialize the pointers
	pktCommHdr = (COMM_PKT_HDR*)rs485Packet;
	pCommCRC	= (BYTE*)&rs485Packet[sizeof(COMM_PKT_HDR)+0];


	pktCommHdr->pktHdr = slaveBoardHdr[pibSelect];
	pktCommHdr->pktType = PIB_GET_DATA;
	pktCommHdr->pktLength = COMM_PKT_LEN_NO_DATA;

	*pCommCRC = CalculateCRC(rs485Packet, sizeof(COMM_PKT_HDR)+0);

	rs485SendBinaryData(rs485Packet,  sizeof(COMM_PKT_HDR)+0+1);

}

//******************************************************************************
//
//  Function: PibSetParam
//
//  Arguments: 	BYTE pibSelect	- master(0) or slave(1) PIB.
//				BYTE solCtrl	- activate/deactivate PIB solenoids
//			 	BYTE contCtrl	- activate/deactivate PIB contacts
//				BYTE solTimeout	 - maximum time a solenoid can be in active state, measured in seconds. Default is 10 sec.
//
//  Returns: none
//
//  Description: send command "Set PIB Parameters".
//
//******************************************************************************
void PibSetParamRs485(SlaveBoardsT pibSelect)
{
//	BYTE pibBoard;

    if((pibSelect != PIB_M_BOARD)&&(pibSelect !=  PIB_S_BOARD))
        return;

	// Initialize the pointers
	pktCommHdr   = (COMM_PKT_HDR*)rs485Packet;
	pPibParamCmd = (PIB_SET_PARAM_CMD_PKT*)&rs485Packet[sizeof(COMM_PKT_HDR)];
	pCommCRC	 = (BYTE*)&rs485Packet[sizeof(COMM_PKT_HDR)+sizeof(PIB_SET_PARAM_CMD_PKT)];

	pktCommHdr->pktHdr    = slaveBoardHdr[pibSelect];
	pktCommHdr->pktType   = PIB_SET_PARAMS;
	pktCommHdr->pktLength = COMM_PKT_LEN_NO_DATA + sizeof(PIB_SET_PARAM_CMD_PKT);

	pPibParamCmd->solenoidCtrl    = sPibParamSet[pibSelect].solenoidCtrl ;
	pPibParamCmd->contactCtrl     = sPibParamSet[pibSelect].contactCtrl ;
	pPibParamCmd->solenoidTimeout = sPibParamSet[pibSelect].solenoidTimeout;

	*pCommCRC = CalculateCRC(rs485Packet, sizeof(COMM_PKT_HDR)+sizeof(PIB_SET_PARAM_CMD_PKT));

	rs485SendBinaryData(rs485Packet,  sizeof(COMM_PKT_HDR)+sizeof(PIB_SET_PARAM_CMD_PKT)+1);

}

//******************************************************************************
//
//  Function: PibSetParamFromRF
//
//  Arguments: 	SlaveBoardsT pibSelect	- master(0) or slave(1) PIB.
//				BYTE solCtrl	- activate/deactivate PIB solenoids
//			 	BYTE contCtrl	- activate/deactivate PIB contacts
//				BYTE solTimeout	 - maximum time a solenoid can be in active state, measured in seconds. Default is 10 sec.
//
//  Returns: none
//
//  Description: This functions is called from the Wireless radio protocol file to set PIB parameters,
//				which will be passed over the RS485 interface to the corresponding PIB board.
//******************************************************************************
void PibSetParamFromRf( BYTE targetPib, BYTE solCtrl, BYTE contCtrl, BYTE solTimeout)
{
    switch (targetPib)
    {
    case SET_MASTER_PIB:
        sPibParamSet[MASTER_PIB_BOARD].solenoidCtrl     = solCtrl & 0x0F;
        sPibParamSet[MASTER_PIB_BOARD].contactCtrl      = contCtrl & 0x0F;
        sPibParamSet[MASTER_PIB_BOARD].solenoidTimeout  = solTimeout;
        break;
    case SET_SLAVE_PIB:
//        sPibParamSet[SLAVE_PIB_BOARD].solenoidCtrl      = (solCtrl & 0xF0)>>4;
//        sPibParamSet[SLAVE_PIB_BOARD].contactCtrl       = (contCtrl & 0xF0)>>4;
        sPibParamSet[SLAVE_PIB_BOARD].solenoidCtrl      = (solCtrl & 0x0F);
        sPibParamSet[SLAVE_PIB_BOARD].contactCtrl       = (contCtrl & 0x0F);
        sPibParamSet[SLAVE_PIB_BOARD].solenoidTimeout   = solTimeout;
        break;
    default:
        break;
    }

}


//******************************************************************************
//
//  Function: PtsSetParamRs485
//
//  Arguments: none
//
//  Returns: none
//
//  Description: send command "Set PTS Parameters" over RS485 interface.
//
//******************************************************************************
void PtsSetParamRs485( void)
{

    InitRs485Interface(BAUD_RATE_PLUG, RS845_TX_EN);

	// Initialise the pointers
	pktCommHdr   = (COMM_PKT_HDR*)rs485Packet;
	pPtsParamCmd = (PTS_SET_PARAM_CMD_PKT*)&rs485Packet[sizeof(COMM_PKT_HDR)];
	pCommCRC	 = (BYTE*)&rs485Packet[sizeof(COMM_PKT_HDR)+sizeof(PTS_SET_PARAM_CMD_PKT)];

	pktCommHdr->pktHdr    = PTS_CMD_HDR;
	pktCommHdr->pktType   = PTS_SET_PARAMS;
	pktCommHdr->pktLength = COMM_PKT_LEN_NO_DATA + sizeof(PTS_SET_PARAM_CMD_PKT);

	//	pPtsParamCmd->readingPerSecond = sPtsParamSet.readingPerSecond;
	pPtsParamCmd->magnetSampleRate = sPtsParamSet.magnetSampleRate;
	pPtsParamCmd->averagingFactor  = sPtsParamSet.averagingFactor;

	*pCommCRC = CalculateCRC(rs485Packet, sizeof(COMM_PKT_HDR)+sizeof(PTS_SET_PARAM_CMD_PKT));

	rs485SendBinaryData(rs485Packet,  sizeof(COMM_PKT_HDR)+sizeof(PTS_SET_PARAM_CMD_PKT)+1);
}

//******************************************************************************
//
//  Function: PtsSetParamRf
//
//  Arguments:
//              BYTE mgtSampleRate - magnetometer sample rate
//              BYTE avgFactor - averaging factor
//              BYTE resetM  - reset min/max values
//
//  Returns: none
//
//  Description: Set PTS parameters from PC over the radio. These will be transmitted to PTS over RS485 when the state machine is IDLE.
//
//******************************************************************************
void PtsSetParamRf( BYTE magnetSampleRate, BYTE averagingFactor)
{
//    sPtsParamSet.readingPerSecond = PLUG_SENS_NBR_READINGS;
    sPtsParamSet.magnetSampleRate = magnetSampleRate;
    sPtsParamSet.averagingFactor  = averagingFactor;
}

//******************************************************************************
//
//  Function: PtsGetData
//
//  Arguments: 	none
//
//  Returns: none
//
//  Description: send command "Get PTS data". It's zero data length command.
//
//******************************************************************************
void PtsGetDataRs485(void)
{
    InitRs485Interface(BAUD_RATE_PLUG, RS845_TX_EN);

	// Initialise the pointers
	pktCommHdr = (COMM_PKT_HDR*)rs485Packet;
	pCommCRC   = (BYTE*)&rs485Packet[sizeof(COMM_PKT_HDR)+0];

	pktCommHdr->pktHdr    = PTS_CMD_HDR;
	pktCommHdr->pktType   = PTS_GET_DATA;
	pktCommHdr->pktLength = COMM_PKT_LEN_NO_DATA;

	*pCommCRC = CalculateCRC(rs485Packet, sizeof(COMM_PKT_HDR)+0);

	rs485SendBinaryData(rs485Packet,  sizeof(COMM_PKT_HDR)+0+1);
}


//******************************************************************************
//
//  Function: TecSlaveGetData
//
//  Arguments: 	none
//
//  Returns: none
//
//  Description: send command "Set Slave TEC Parameters". It's zero data length command.
//
//******************************************************************************
void TecSlaveGetData(void)
{

    InitRs485Interface(BAUD_RATE_TEC, RS845_TX_EN);

	// Initialise the pointers
	pktCommHdr = (COMM_PKT_HDR*)rs485Packet;
	pCommCRC   = (BYTE*)&rs485Packet[sizeof(COMM_PKT_HDR)+0];

	pktCommHdr->pktHdr    = TEC_S_CMD_HDR;
	pktCommHdr->pktType   = TEC_S_GET_DATA;
	pktCommHdr->pktLength = COMM_PKT_LEN_NO_DATA;

	*pCommCRC = CalculateCRC(rs485Packet, sizeof(COMM_PKT_HDR)+0);

	rs485SendBinaryData(rs485Packet, sizeof(COMM_PKT_HDR)+0+1);
}


//******************************************************************************
//
//  Function: TecSlaveSetParam
//
//  Arguments: none
//
//
//  Returns: none
//
//  Description: send command "Set Slave TEC Parameters".
//
//******************************************************************************
void TecSlaveSetParamRs485( void)
{
    InitRs485Interface(BAUD_RATE_TEC, RS845_TX_EN);

	// Initialize the pointers
	pktCommHdr   = (COMM_PKT_HDR*)rs485Packet;
	pTecParamCmd = (TEC_S_SET_PARAM_RSP_PKT*)&rs485Packet[sizeof(COMM_PKT_HDR)];
	pCommCRC	 = (BYTE*)&rs485Packet[sizeof(COMM_PKT_HDR)+sizeof(TEC_S_SET_PARAM_RSP_PKT)];

	pktCommHdr->pktHdr    = TEC_S_CMD_HDR;
	pktCommHdr->pktType   = TEC_S_SET_PARAMS;
	pktCommHdr->pktLength = COMM_PKT_LEN_NO_DATA + sizeof(TEC_S_SET_PARAM_RSP_PKT);

	pTecParamCmd->numberADC   = sSlaveTecParamsSet.numberADC;
	pTecParamCmd->currentOutput   = sSlaveTecParamsSet.currentOutput;
	pTecParamCmd->avgFactor = sSlaveTecParamsSet.avgFactor;

	*pCommCRC = CalculateCRC(rs485Packet, sizeof(COMM_PKT_HDR)+sizeof(TEC_S_SET_PARAM_RSP_PKT));

	rs485SendBinaryData(rs485Packet, sizeof(COMM_PKT_HDR)+sizeof(TEC_S_SET_PARAM_RSP_PKT)+1);
}

//******************************************************************************
//
//  Function: TecSlaveSetParamRf
//
//  Arguments:
//			 	BYTE adcChannel   - ADC channel.
//				BYTE adcCurrent   - Constant current source.
//				BYTE adcAveraging - ADC averaging coefficient.
//
//  Returns: none
//
//  Description: send command "Set PTS Parameters".
//
//******************************************************************************
void TecSlaveSetParamRf( BYTE adcChannel, BYTE adcCurrent, BYTE adcAveraging)
{

	sSlaveTecParamsSet.numberADC   = adcChannel;
	sSlaveTecParamsSet.currentOutput   = adcCurrent;
	sSlaveTecParamsSet.avgFactor = adcAveraging;

}


//******************************************************************************
//
//  Function: GetCommHwFwVer
//
//  Arguments: 	SlaveBoardsT boardSel - used to select the HEADER
//
//  Returns: none
//
//  Description: send command "Get HW FW SN". It's zero data length command.
//
//******************************************************************************
void GetCommHwFwVer(SlaveBoardsT boardSel)
{

    switch(boardSel)
    {
    case PIB_M_BOARD:
    case PIB_S_BOARD:
        InitRs485Interface(BAUD_RATE_PIB, RS845_TX_EN);
        break;
    case TEC_S_BOARD:
        InitRs485Interface(BAUD_RATE_TEC, RS845_TX_EN);
        break;
    case PTS_BOARD:
        InitRs485Interface(BAUD_RATE_PLUG, RS845_TX_EN);
        break;
    default:
        return;
    }

	// Initialise the pointers
	pktCommHdr = (COMM_PKT_HDR*)rs485Packet;
	pCommCRC   = (BYTE*)&rs485Packet[sizeof(COMM_PKT_HDR)+0];

	pktCommHdr->pktHdr    = slaveBoardHdr[boardSel];
	pktCommHdr->pktType   = COM_GET_VERSIONS;
	pktCommHdr->pktLength = COMM_PKT_LEN_NO_DATA;

	*pCommCRC = CalculateCRC(rs485Packet, sizeof(COMM_PKT_HDR)+0);

	rs485SendBinaryData(rs485Packet,  sizeof(COMM_PKT_HDR)+0+1);
}


//******************************************************************************
//
//  Function: ParseGetCommHwFwVer
//
//  Arguments: 	SlaveBoardsT boardSel - used to select the HEADER
//
//  Returns: none
//
//  Description:
//
//******************************************************************************
void ParseGetCommHwFwVer(SlaveBoardsT boardSel, BYTE * pktData)
{
	memcpy(&slaveBoardsRevs[boardSel],pktData,sizeof(COM_GET_VERSIONS_RSP_PKT));
}


//******************************************************************************
//
//  Function: ParseSlaveTecData
//
//  Arguments: 	SlaveBoardsT boardSel - used to select the HEADER
//
//  Returns: none
//
//  Description:
//
//******************************************************************************
void ParseSlaveTecData(TEC_S_GET_DATA_RSP_PKT * pktData)
{
	memcpy(&sSlaveTECData,pktData,sizeof(TEC_S_GET_DATA_RSP_PKT));
}


//******************************************************************************
//
//  Function: ParseSlaveTecParams
//
//  Arguments: 	TEC_S_SET_PARAM_RSP_PKT pktData - pointer to received data
//
//  Returns: none
//
//  Description:
//
//******************************************************************************
void ParseSlaveTecParams(TEC_S_SET_PARAM_RSP_PKT * pktData)
{
	memcpy(&sSlaveTecParamsRsp,pktData,sizeof(TEC_S_SET_PARAM_RSP_PKT));
}

//******************************************************************************
//
//  Function: ParsePtsData
//
//  Arguments: 	PTS_GET_DATA_RSP_PKT * pktData - pointer to received data
//
//  Returns: none
//
//  Description:
//
//******************************************************************************
void ParsePtsData(PTS_GET_DATA_RSP_PKT * pktData)
{
	memcpy(&sPtsDataRsp,pktData,sizeof(PTS_GET_DATA_RSP_PKT));
}

//******************************************************************************
//
//  Function: ParsePtsSetParams
//
//  Arguments:  PTS_SET_PARAM_RSP_PKT* pktData - pointer to received data
//
//  Returns: none
//
//  Description:
//
//******************************************************************************
void ParsePtsSetParams(PTS_SET_PARAM_RSP_PKT* pktData)
{
    memcpy(&sPtsParamRsp,pktData,sizeof(PTS_SET_PARAM_RSP_PKT));
}

//******************************************************************************
//
//  Function: ParsePIBRsp
//
//  Arguments:  PIB_RSP_PKT *pibRespData - pointer to received data
//              SlaveBoardsT pibBoard - PIB Master or PIB Slave
//
//  Returns: none
//
//  Description:
//
//******************************************************************************
void ParsePIBRsp(PIB_RSP_PKT *pibRespData,SlaveBoardsT pibBoard)
{
    switch(pibBoard)
    {
    case PIB_M_BOARD:
        memcpy(&pibBoardsData[MASTER_PIB_BOARD],pibRespData,sizeof(PIB_RSP_PKT));
        break;
    case PIB_S_BOARD:
        memcpy(&pibBoardsData[SLAVE_PIB_BOARD],pibRespData,sizeof(PIB_RSP_PKT));
        break;
    default:
        break;
    }

}


//******************************************************************************
//
//  Function: HavePIBPacket
//
//  Arguments: 	BYTE *pBuff - pointer to incomming data buffer
//				WORD buffLen - length of the data buffer to check.
//  Returns: BOOL - TRUE if a valid packet is found, FALSE otherwise
//
//  Description: examine the incoming data for valid data packet.
//
//******************************************************************************
BOOL HavePIBPacket(BYTE *pBuff, WORD buffLen)
{
	// Scans the passed buffer for a PIB command or response. Returns true if
	// a packet is found, in which case pktCommHdr and pktPibData pointers are asigned.
	// Returns false if no packet is found.
    WORD bytesSkipped = 0;

    BOOL foundInfoItem = FALSE;
    int  dataLen       = 0;
    int  expPktLen     = 0;
    unsigned int iInfoItem = 0;
    BYTE expCRC;

    havePIBPkt = FALSE;

    if( buffLen < (WORD)MIN_COM_PKT_LEN )
        return FALSE;

    while( bytesSkipped + (WORD)MIN_COM_PKT_LEN <= buffLen )
       {
    		ResetWatchdog();
           // Have enough bytes for a packet. Header byte must be the first
           // byte we see
           if(( pBuff[bytesSkipped] != TEC_S_RESP_HDR )&&(pBuff[bytesSkipped] != PIB_M_RESP_HDR ) &&(pBuff[bytesSkipped] != PIB_S_RESP_HDR )&&(pBuff[bytesSkipped] != PTS_RESP_HDR ))
           {
               bytesSkipped++;
               continue;
           }

           // Looks like the start of a header. See if the rest of the packet
           // makes sense.
           pktCommHdr = (COMM_PKT_HDR*)&(pBuff[bytesSkipped]);

           for(iInfoItem = 0; iInfoItem < MPLS_NBR_RSP; iInfoItem++ )
           {
               if( m_pktInfo[iInfoItem].cmdRespByte != pktCommHdr->pktType )
                   continue;

               // Found a hdr / type pair match. Calc the number of bytes required
               // for this packet. That would be a header, the data portion, and
               // a checksum byte.
               dataLen   = m_pktInfo[iInfoItem].expDataLen;
               expPktLen = MIN_COM_PKT_LEN  + dataLen;

               foundInfoItem = TRUE;
               break;
           }

           if( !foundInfoItem )
           {
               bytesSkipped++;
               continue;
           }

           // If we have enough bytes, check the CRC. Otherwise, we have to
           // wait for more bytes to come in
           if( bytesSkipped + expPktLen > buffLen )
               break;

           // Have enough bytes - validate the CRC
           expCRC = CalculateCRC( &( pBuff[bytesSkipped] ), expPktLen - 1 );

           if( pBuff[ bytesSkipped + expPktLen - 1 ] == expCRC )
           {
               // Success!
              // memcpy( &pktCommHdr, &( pBuff[bytesSkipped] ), sizeof( WTTTS_PKT_HDR ) );
        	   pktCommHdr = (COMM_PKT_HDR*)&pBuff[bytesSkipped];

               if( dataLen > 0 )
               {
                  pktPibData = &pBuff[bytesSkipped + sizeof( COMM_PKT_HDR )] ;
               }
               // Remove this packet from the buffer
               bytesSkipped += expPktLen;

               havePIBPkt = TRUE;
               break;
           }

           // CRC error - continue scanning
           bytesSkipped++;
       }

    return havePIBPkt;
}


//******************************************************************************
//
//  Function: HavePIBPacket
//
//  Arguments:  BYTE *pBuff - pointer to incomming data buffer
//              WORD buffLen - length of the data buffer to check.
//  Returns: BOOL - TRUE if a valid packet is found, FALSE otherwise
//
//  Description: examine the incoming data for valid data packet.
//
//******************************************************************************
BOOL SlaveTecHavePIBPacket(BYTE *pBuff, WORD buffLen)
{
    // Scans the passed buffer for a PIB command or response. Returns true if
    // a packet is found, in which case pktCommHdr and pktPibData pointers are asigned.
    // Returns false if no packet is found.
    WORD bytesSkipped = 0;

    BOOL foundInfoItem = FALSE;
    int  dataLen       = 0;
    int  expPktLen     = 0;
    unsigned int iInfoItem = 0;
    BYTE expCRC;

    havePIBPkt = FALSE;

    if( buffLen < (WORD)MIN_COM_PKT_LEN )
        return FALSE;

    while( bytesSkipped + (WORD)MIN_COM_PKT_LEN <= buffLen )
       {
            ResetWatchdog();
           // Have enough bytes for a packet. Header byte must be the first
           // byte we see
           if( pBuff[bytesSkipped] != TEC_S_CMD_HDR )
           {
               bytesSkipped++;
               continue;
           }

           // Looks like the start of a header. See if the rest of the packet
           // makes sense.
           pktCommHdr = (COMM_PKT_HDR*)&(pBuff[bytesSkipped]);

           for(iInfoItem = 0; iInfoItem < SLAVE_TEC_CMD_NBR; iInfoItem++ )
           {
               if( m_pktInfoSlaveTEC[iInfoItem].cmdRespByte != pktCommHdr->pktType )
                   continue;

               // Found a hdr / type pair match. Calc the number of bytes required
               // for this packet. That would be a header, the data portion, and
               // a checksum byte.
               dataLen   = m_pktInfoSlaveTEC[iInfoItem].expDataLen;
               expPktLen = MIN_COM_PKT_LEN  + dataLen;

               foundInfoItem = TRUE;
               break;
           }

           if( !foundInfoItem )
           {
               bytesSkipped++;
               continue;
           }

           // If we have enough bytes, check the CRC. Otherwise, we have to
           // wait for more bytes to come in
           if( bytesSkipped + expPktLen > buffLen )
               break;

           // Have enough bytes - validate the CRC
           expCRC = CalculateCRC( &( pBuff[bytesSkipped] ), expPktLen - 1 );

           if( pBuff[ bytesSkipped + expPktLen - 1 ] == expCRC )
           {
               // Success!
              // memcpy( &pktCommHdr, &( pBuff[bytesSkipped] ), sizeof( WTTTS_PKT_HDR ) );
               pktCommHdr = (COMM_PKT_HDR*)&pBuff[bytesSkipped];

               if( dataLen > 0 )
               {
                  pktPibData = &pBuff[bytesSkipped + sizeof( COMM_PKT_HDR )] ;
               }
               // Remove this packet from the buffer
               bytesSkipped += expPktLen;

               havePIBPkt = TRUE;
               break;
           }

           // CRC error - continue scanning
           bytesSkipped++;
       }

    return havePIBPkt;
}






//******************************************************************************
//
//  Function: updateMplCommState
//
//  Arguments:  none
//
//  Returns:
//
//  Description: Main State Machine Controlling the Commands and Responses over the RS485 interface.
//
//******************************************************************************
void UpdateMplCommState( void )
{
    switch ( mplCommState )
    {

        case MTEC_INIT:

            if ( TimerExpired( thPibTimer ) )
            {
                mtecCtrlState = MTEC_CTRL_IDLE;
                mplCommState = MTEC_MASTER_PIB_INIT_CMD;
            }

            break;

        case MTEC_MASTER_PIB_INIT_CMD:

            if ( TimerExpired( thPibTimer ) )
            {
                GetCommHwFwVer( PIB_M_BOARD );
                ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
                mplCommState = MTEC_MASTER_PIB_INIT_RESP;
            }

            break;

        case MTEC_MASTER_PIB_INIT_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == PIB_M_RESP_HDR ) && ( pktCommHdr->pktType == COM_GET_VERSIONS ) )
                    {
                        ParseGetCommHwFwVer( PIB_M_BOARD, pktPibData );
                        slaveBoardsAvailAtPowerOn[PIB_M_BOARD] = TRUE;
                        mplCommState = MTEC_SLAVE_TEC_INIT_CMD;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }

                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    if ( retryCnt < COM_RETRIES )
                    {
                        mplCommState = MTEC_MASTER_PIB_INIT_CMD;
                        retryCnt++;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }
                    else
                    {
                        slaveBoardsAvailAtPowerOn[PIB_M_BOARD] = FALSE;
                        memset( &slaveBoardsRevs[PIB_M_BOARD], 0x00, sizeof(COM_GET_VERSIONS_RSP_PKT) );
                        mplCommState = MTEC_SLAVE_TEC_INIT_CMD;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                        retryCnt = 0;
                    }
                }
            }

            break;

        case MTEC_SLAVE_TEC_INIT_CMD:

            if ( TimerExpired( thPibTimer ) )
            {
                GetCommHwFwVer( TEC_S_BOARD );
                ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
                mplCommState = MTEC_SLAVE_TEC_INIT_RESP;
            }

            break;

        case MTEC_SLAVE_TEC_INIT_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == TEC_S_RESP_HDR ) && ( pktCommHdr->pktType == COM_GET_VERSIONS ) )
                    {
                        ParseGetCommHwFwVer( TEC_S_BOARD, pktPibData );
                        slaveBoardsAvailAtPowerOn[TEC_S_BOARD] = TRUE;
                        mplCommState = MTEC_SLAVE_PIB_INIT_CMD;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }

                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    if ( retryCnt < COM_RETRIES )
                    {
                        mplCommState = MTEC_SLAVE_TEC_INIT_CMD;
                        retryCnt++;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }
                    else
                    {
                        slaveBoardsAvailAtPowerOn[TEC_S_BOARD] = FALSE;
                        memset( &slaveBoardsRevs[TEC_S_BOARD], 0x00, sizeof(COM_GET_VERSIONS_RSP_PKT) );
                        mplCommState = MTEC_SLAVE_PIB_INIT_CMD;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                        retryCnt = 0;
                    }
                }
            }
            break;

        case MTEC_SLAVE_PIB_INIT_CMD:

            if ( TimerExpired( thPibTimer ) )
            {
                GetCommHwFwVer( PIB_S_BOARD );
                ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
                mplCommState = MTEC_SLAVE_PIB_INIT_RESP;
            }

            break;

        case MTEC_SLAVE_PIB_INIT_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == PIB_S_RESP_HDR ) && ( pktCommHdr->pktType == COM_GET_VERSIONS ) )
                    {
                        ParseGetCommHwFwVer( PIB_S_BOARD, pktPibData );
                        slaveBoardsAvailAtPowerOn[PIB_S_BOARD] = TRUE;
                        mplCommState = MTEC_PLUG_SENS_INIT_CMD;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }
                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    if ( retryCnt < COM_RETRIES )
                    {
                        mplCommState = MTEC_SLAVE_PIB_INIT_CMD;
                        retryCnt++;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }
                    else
                    {
                        slaveBoardsAvailAtPowerOn[PIB_S_BOARD] = FALSE;
                        memset( &slaveBoardsRevs[PIB_S_BOARD], 0x00, sizeof(COM_GET_VERSIONS_RSP_PKT) );
                        mplCommState = MTEC_PLUG_SENS_INIT_CMD;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                        retryCnt = 0;
                    }
                }
            }

            break;

        case MTEC_PLUG_SENS_INIT_CMD:

            if ( TimerExpired( thPibTimer ) )
            {
                GetCommHwFwVer( PTS_BOARD );
                ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
                mplCommState = MTEC_PLUG_SENS_INIT_RESP;
            }

            break;

        case MTEC_PLUG_SENS_INIT_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == PTS_RESP_HDR ) && ( pktCommHdr->pktType == COM_GET_VERSIONS ) )
                    {
                        ParseGetCommHwFwVer( PTS_BOARD, pktPibData );
                        slaveBoardsAvailAtPowerOn[PTS_BOARD] = TRUE;
                        mplCommState = MTEC_COMM_IDLE;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }
                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    if ( retryCnt < COM_RETRIES )
                    {
                        mplCommState = MTEC_PLUG_SENS_INIT_CMD;
                        retryCnt++;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }
                    else
                    {
                        slaveBoardsAvailAtPowerOn[PTS_BOARD] = FALSE;
                        memset( &slaveBoardsRevs[PTS_BOARD], 0x00, sizeof(COM_GET_VERSIONS_RSP_PKT) );
                        mplCommState = MTEC_COMM_IDLE;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                        retryCnt = 0;
                    }
                }
            }

            break;

        case MTEC_COMM_IDLE:

            switch ( mtecCtrlState )
            {
                case MTEC_CTRL_IDLE:

                    if ( CommandRs485Post != NO_POST_CMD )
                    {
                        switch ( CommandRs485Post )
                        {
                            case POST_MPIB_SET_PRMS:
                                mtecCtrlState = MTEC_CTRL_PIB_M_PARAMS;
                                mplCommState = MTEC_MASTER_PIB_SET_PARAMS_CMD;
                                break;
                            case POST_SPIB_SET_PRMS:
                                mtecCtrlState = MTEC_CTRL_PIB_S_PARAMS;
                                mplCommState = MTEC_SLAVE_PIB_SET_PARAMS_CMD;
                                break;
                            case POST_STEC_SET_PRMS:
                                mtecCtrlState = MTEC_CTRL_EXT_CMD;
                                mplCommState = MTEC_SLAVE_TEC_SET_PARAMS_CMD;
                                break;
                            case POST_PLUG_SET_PRMS:
                                mtecCtrlState = MTEC_CTRL_EXT_CMD;
                                mplCommState = MTEC_PTS_SET_PARAMS_CMD;
                                break;
                            default:
                                break;
                        }
                        CommandRs485Post = NO_POST_CMD;
                    }
                    else
                    {
                        if ( StartDataCycle )
                        {
//	                mtecCtrlState = MTEC_CTRL_PIB_M;
//	                mplCommState = MTEC_MASTER_PIB_GET_DATA_CMD;
                            StartDataCycle = FALSE;
                            mtecCtrlState = MTEC_CTRL_PTS;
                            mplCommState = MTEC_PTS_GET_DATA_CMD;

                        }
                    }

                    break;

                case MTEC_CTRL_PIB_M:

                    if ( TimerExpired( thPibTimer ) )
                    {
//	            mtecCtrlState = MTEC_CTRL_PTS;          // TODO - UNCOMMENT
//	            mplCommState = MTEC_PTS_GET_DATA_CMD;   // TODO - UNCOMMENT
                        mtecCtrlState = MTEC_CTRL_TEC_S;
                        mplCommState = MTEC_SLAVE_TEC_GET_DATA_CMD;

                    }

                    break;

                case MTEC_CTRL_PTS:

                    if ( TimerExpired( thPibTimer ) )
                    {

//	            mtecCtrlState = MTEC_CTRL_TEC_S;
//	            mplCommState = MTEC_SLAVE_TEC_GET_DATA_CMD;

                        mtecCtrlState = MTEC_CTRL_PIB_M;
                        mplCommState = MTEC_MASTER_PIB_GET_DATA_CMD;
                    }

                    break;

                case MTEC_CTRL_TEC_S:

                    if ( TimerExpired( thPibTimer ) )
                    {

                        mtecCtrlState = MTEC_CTRL_PIB_S;
                        mplCommState = MTEC_SLAVE_PIB_GET_DATA_CMD;
                    }

                    break;

                case MTEC_CTRL_PIB_S:

                    if ( TimerExpired( thPibTimer ) )
                    {

                        mtecCtrlState = MTEC_CTRL_IDLE;
                        mplCommState = MTEC_COMM_IDLE; // end of get data from slave boards cycle.
                    }

                    break;

                case MTEC_CTRL_PIB_M_PARAMS:

                    mtecCtrlState = MTEC_CTRL_IDLE;

                    break;

                case MTEC_CTRL_PIB_S_PARAMS:

                    mtecCtrlState = MTEC_CTRL_IDLE;

                    break;

                case MTEC_CTRL_EXT_CMD:

                    mtecCtrlState = MTEC_CTRL_IDLE;

                    break;

                default:

                    mtecCtrlState = MTEC_CTRL_IDLE;

                    break;

            }

            break;

        case MTEC_MASTER_PIB_GET_DATA_CMD:

            PibGetData( PIB_M_BOARD );
            ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
            mplCommState = MTEC_MASTER_PIB_GET_DATA_RESP;

            break;

        case MTEC_MASTER_PIB_GET_DATA_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == PIB_M_RESP_HDR ) && ( pktCommHdr->pktType == PIB_GET_DATA ) )
                    {
                        // parse data
                        ParsePIBRsp( (PIB_RSP_PKT *) pktPibData, PIB_M_BOARD );
                        slaveBoardsAvail[PIB_M_BOARD] = TRUE;
                        mplCommState = MTEC_COMM_IDLE;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }
                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    slaveBoardsAvail[PIB_M_BOARD] = FALSE;
                    // should i set PIB DATA [PIB_M_BOARD] to 0xFF or something else -- TODO
                    mplCommState = MTEC_COMM_IDLE;
                }
            }

            break;

        case MTEC_MASTER_PIB_SET_PARAMS_CMD:

            PibSetParamRs485( PIB_M_BOARD );
            ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
            mplCommState = MTEC_MASTER_PIB_SET_PARAMS_RESP;

            break;

        case MTEC_MASTER_PIB_SET_PARAMS_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == PIB_M_RESP_HDR ) && ( pktCommHdr->pktType == PIB_GET_DATA ) )
                    {
                        ParsePIBRsp( (PIB_RSP_PKT *) pktPibData, PIB_M_BOARD );
                        slaveBoardsAvail[PIB_M_BOARD] = TRUE;
                        mplCommState = MTEC_COMM_IDLE;
                        CommandRs485Rsp = PIB_SET_ANY_RSP;
                    }
                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    slaveBoardsAvail[PIB_M_BOARD] = FALSE;
                    // should i set PIB DATA [PIB_M_BOARD] to 0xFF or something else -- TODO
                    mplCommState = MTEC_COMM_IDLE;
                    CommandRs485Rsp = SET_RSP_TIMEOUT;
                }
            }

            break;

            // SLAVE PIB
        case MTEC_SLAVE_PIB_GET_DATA_CMD:

            PibGetData( PIB_S_BOARD );
            ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
            mplCommState = MTEC_SLAVE_PIB_GET_DATA_RESP;

            break;

        case MTEC_SLAVE_PIB_GET_DATA_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );
                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == PIB_S_RESP_HDR ) && ( pktCommHdr->pktType == PIB_GET_DATA ) )
                    {
                        // parse data
                        ParsePIBRsp( (PIB_RSP_PKT *) pktPibData, PIB_S_BOARD );
                        slaveBoardsAvail[PIB_S_BOARD] = TRUE;
                        mplCommState = MTEC_COMM_IDLE;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }
                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    slaveBoardsAvail[PIB_S_BOARD] = FALSE;
                    // should i set PIB DATA [PIB_S_BOARD] to 0xFF or something else -- TODO
                    mplCommState = MTEC_COMM_IDLE;
                }
            }

            break;

        case MTEC_SLAVE_PIB_SET_PARAMS_CMD:

            PibSetParamRs485( PIB_S_BOARD );
            ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
            mplCommState = MTEC_SLAVE_PIB_SET_PARAMS_RESP;

            break;

        case MTEC_SLAVE_PIB_SET_PARAMS_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );
                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == PIB_S_RESP_HDR ) && ( pktCommHdr->pktType == PIB_GET_DATA ) )
                    {
                        ParsePIBRsp( (PIB_RSP_PKT *) pktPibData, PIB_S_BOARD );
                        slaveBoardsAvail[PIB_S_BOARD] = TRUE;
                        mplCommState = MTEC_COMM_IDLE;
//					CommandRs485Rsp |= PIB_SET_S_RSP;
                        CommandRs485Rsp = PIB_SET_ANY_RSP;
                    }
                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    slaveBoardsAvail[PIB_S_BOARD] = FALSE;
                    mplCommState = MTEC_COMM_IDLE;
                    //			CommandRs485Rsp |= SET_RSP_TIMEOUT<<4;
                    CommandRs485Rsp = SET_RSP_TIMEOUT;
                }
            }

            break;

            // SLAVE TEC
        case MTEC_SLAVE_TEC_GET_DATA_CMD:

            TecSlaveGetData();
            ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
            mplCommState = MTEC_SLAVE_TEC_GET_DATA_RESP;

            break;

        case MTEC_SLAVE_TEC_GET_DATA_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == TEC_S_RESP_HDR ) && ( pktCommHdr->pktType == TEC_S_GET_DATA ) )
                    {
                        ParseSlaveTecData( (TEC_S_GET_DATA_RSP_PKT*) pktPibData );
                        slaveBoardsAvail[TEC_S_BOARD] = TRUE;
                        mplCommState = MTEC_COMM_IDLE;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }
                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    slaveBoardsAvail[TEC_S_BOARD] = FALSE;
                    mplCommState = MTEC_COMM_IDLE;
                }
            }

            break;

        case MTEC_SLAVE_TEC_SET_PARAMS_CMD:

            TecSlaveSetParamRs485();
            ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
            mplCommState = MTEC_SLAVE_TEC_SET_PARAMS_RESP;

            break;

        case MTEC_SLAVE_TEC_SET_PARAMS_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == TEC_S_RESP_HDR ) && ( pktCommHdr->pktType == TEC_S_SET_PARAMS ) )
                    {
                        ParseSlaveTecParams( (TEC_S_SET_PARAM_RSP_PKT*) pktPibData );
                        slaveBoardsAvail[TEC_S_BOARD] = TRUE;
                        mplCommState = MTEC_COMM_IDLE;
                        CommandRs485Rsp = TEC_SET_S_RSP;
                    }
                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    slaveBoardsAvail[TEC_S_BOARD] = FALSE;
                    mplCommState = MTEC_COMM_IDLE;
                    CommandRs485Rsp = SET_RSP_TIMEOUT;
                }
            }

            break;

        case MTEC_PTS_GET_DATA_CMD:

            PtsGetDataRs485();
            ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
            mplCommState = MTEC_PTS_GET_DATA_RESP;
            tmpWord = 0;

            break;

        case MTEC_PTS_GET_DATA_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == PTS_RESP_HDR ) && ( pktCommHdr->pktType == PTS_GET_DATA ) )
                    {
                        ParsePtsData( (PTS_GET_DATA_RSP_PKT*) pktPibData );
                        slaveBoardsAvail[PTS_BOARD] = TRUE;
                        mplCommState = MTEC_COMM_IDLE;
                        ResetTimer( thPibTimer, MPL_COMM_GAP );
                    }
                }
                else
                {
                    dbgArray[dbgCnt++] = tmpWord;
                    if(dbgCnt>63)
                        dbgCnt = 0;
                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    slaveBoardsAvail[PTS_BOARD] = FALSE;
                    // should i set response packet to 0xFF or something else -- TODO
                    memset(&sPtsDataRsp,0,sizeof(PTS_GET_DATA_RSP_PKT));
                    mplCommState = MTEC_COMM_IDLE;
                }
            }

            break;

        case MTEC_PTS_SET_PARAMS_CMD:

            PtsSetParamRs485();
            ResetTimer( thPibTimer, MPL_COMM_TIMEOUT );
            mplCommState = MTEC_PTS_SET_PARAMS_RESP;

            break;

        case MTEC_PTS_SET_PARAMS_RESP:

            if ( rs485ReceiverHasData() )
            {
                tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

                if ( HavePIBPacket( pibCommBuff, tmpWord ) )
                {
                    if ( ( pktCommHdr->pktHdr == PTS_RESP_HDR ) && ( pktCommHdr->pktType == PTS_SET_PARAMS ) )
                    {
                        ParsePtsSetParams( (PTS_SET_PARAM_RSP_PKT*) pktPibData );
                        slaveBoardsAvail[PTS_BOARD] = TRUE;
                        mplCommState = MTEC_COMM_IDLE;
                        CommandRs485Rsp = PTS_SET_RSP;
                    }
                }
            }
            else
            {
                if ( TimerExpired( thPibTimer ) )
                {
                    slaveBoardsAvail[PTS_BOARD] = FALSE;
                    mplCommState = MTEC_COMM_IDLE;
                    CommandRs485Rsp = SET_RSP_TIMEOUT;
                }
            }

            break;

        default:
            mplCommState = MTEC_COMM_IDLE;
            break;

    }

}




void GetPibSlaveVoltage(WORD *pSlaveVoltage)
{
    BYTE *pByteData;
    pByteData = (BYTE*)pSlaveVoltage;

    *pByteData = pibBoardsData[SLAVE_PIB_BOARD].batVoltage[LOW_BYTE];
    pByteData++;
    *pByteData = pibBoardsData[SLAVE_PIB_BOARD].batVoltage[HIGH_BYTE];

}

void GetPibMasterVoltage(WORD *pMasterVoltage)
{
    BYTE *pByteData;
    pByteData = (BYTE*)pMasterVoltage;

    *pByteData = pibBoardsData[MASTER_PIB_BOARD].batVoltage[LOW_BYTE];
    pByteData++;
    //*pByteData = (BYTE)(pibBoardsData[MASTER_PIB_BOARD].batVoltage>>8);
    *pByteData = pibBoardsData[MASTER_PIB_BOARD].batVoltage[HIGH_BYTE];

}

// When copying words the MSP430 requires the source and the destination to be at a 2 byte boundary.
// Otherwise it gives wrong  result.
// That is why all function that copy WORD data will copy the data as separate bytes.
void GetPtsData(WORD *pToDataArray)
{
    WORD i;
    BYTE *pByteData;

    pByteData = (BYTE*)pToDataArray;

    for(i=0;i<PTS_DATA_SIZE;i++)
    {
        *pByteData  = (BYTE)sPtsDataRsp.sensData[i];
        pByteData++;
        *pByteData  = (BYTE)(sPtsDataRsp.sensData[i]>>8);
        pByteData++;

    }
}

void GetSlaveTankPressure(BYTE *pToDataArray)
{
    WORD i;
    for(i=0;i<ADC_BYTES;i++)
    {
        *pToDataArray = sSlaveTECData.ch1[i];
        pToDataArray++;
    }

}

void GetSlaveRegPressure(BYTE *pToDataArray)
{
    WORD i;
    for(i=0;i<ADC_BYTES;i++)
    {
        *pToDataArray = sSlaveTECData.ch2[i];
        pToDataArray++;
    }

}



BYTE GetPibSolenoidStatus(void)
{
    BYTE solStatus;
    solStatus =  pibBoardsData[SLAVE_PIB_BOARD].solenoidStatus & 0x0F;
    solStatus <<= 4;
    solStatus |=  pibBoardsData[MASTER_PIB_BOARD].solenoidStatus & 0x0F;
    return solStatus;
}

BYTE GetPibContactStatus(void)
{
    BYTE solStatus;
    solStatus =  pibBoardsData[SLAVE_PIB_BOARD].contactStatus & 0x0F;
    solStatus <<= 4;
    solStatus |=  pibBoardsData[MASTER_PIB_BOARD].contactStatus & 0x0F;
    return solStatus;
}

BYTE GetPibSlaveCurrent(void)
{

    return  pibBoardsData[SLAVE_PIB_BOARD].solenoidCurrent;
}


BYTE GetPibMasterCurrent(void)
{

    return  pibBoardsData[MASTER_PIB_BOARD].solenoidCurrent;
}


BYTE GetSlaveBatteryType(void)
{
    return sSlaveTECData.batType;
}



BOOL PowerUpBoardPresent(SlaveBoardsT board)
{
    return slaveBoardsAvailAtPowerOn[board];
}


BOOL CommBoardNotPresent(SlaveBoardsT board)
{
//    return !(slaveBoardsAvail[board]);
    if( slaveBoardsAvail[board] )
        return FALSE;
    else
        return TRUE;

}


BOOL IsWireCommIdle(void)
{
    if (mplCommState == MTEC_COMM_IDLE)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}





void GetSlaveHwFwVer(SlaveBoardsT boardSel, BYTE * destData)
{
    memcpy(destData, &slaveBoardsRevs[boardSel],sizeof(COM_GET_VERSIONS_RSP_PKT));
}


void GetSlaveTecHwVer(BYTE * destData)
{
    memcpy(destData, &slaveBoardsRevs[TEC_S_BOARD].hardVer[0],4);
}

void GetSlaveTecFwVer(BYTE * destData)
{
    memcpy(destData, &slaveBoardsRevs[TEC_S_BOARD].frmVers[0],4);
}

void GetSlaveTecSerNum(BYTE * destData)
{
    memcpy(destData, &slaveBoardsRevs[TEC_S_BOARD].devSerNum[0],32);
}


void GetPlugHwVer(BYTE * destData)
{
    memcpy(destData, &slaveBoardsRevs[PTS_BOARD].hardVer[0],4);
}

void GetPlugFwVer(BYTE * destData)
{
    memcpy(destData, &slaveBoardsRevs[PTS_BOARD].frmVers[0],4);
}

void GetPlugSerNum(BYTE * destData)
{
    memcpy(destData, &slaveBoardsRevs[PTS_BOARD].devSerNum[0],32);
}




void GetPibHwVer(SlaveBoardsT boardSel,BYTE * destData)
{
    memcpy(destData, &slaveBoardsRevs[boardSel].hardVer[0],4);
}

void GetPibFwVer(SlaveBoardsT boardSel,BYTE * destData)
{
    memcpy(destData, &slaveBoardsRevs[boardSel].frmVers[0],4);
}



BYTE GetPtsFlagBit(void)
{
    if(sPtsDataRsp.magnetSampleRate & 0x80)
    {
        return (BYTE)PTS_SWITCH_TRIPPED;
    }
    else
    {
        return (BYTE)PTS_SWITCH_RESET;
    }
}

//******************************************************************************
//
//  Function: UpdateSlaveTecCommState
//
//  Arguments:  none
//
//  Returns:
//
//  Description: Main State Machine Controlling the Commands and Responses over the RS485 interface.
//
//******************************************************************************
void UpdateSlaveTecCommState( void )
{
    TEC_S_SET_PARAM_CMD_PKT *pSetTecPkt;

    if ( rs485ReceiverHasData() )
    {
        tmpWord = rs485ReadReceiverData( pibCommBuff, sizeof( pibCommBuff ) );

        if ( SlaveTecHavePIBPacket( pibCommBuff, tmpWord ) )
        {
            haveRs485Cmd = TRUE;
            switch ( pktCommHdr->pktType )
            {
                case TEC_S_GET_DATA:
                    AverageTheResults();
                    SlaveTecGetDataRspRs485();
                    InitStrainGaugesReading();
                    break;
                case TEC_S_SET_PARAMS:
                    pSetTecPkt = (TEC_S_SET_PARAM_CMD_PKT*) pktPibData;
                    if ( pSetTecPkt->numberADC == 0 )
                    {
                        // set all adc - master and slave
                        BYTE adcNumber;
                        for ( adcNumber = ADC_USED_FIRST; adcNumber < MASTER_ADC_CHNLS; adcNumber++ )
                        {
                            SetAdcCurrent( adcNumber, pSetTecPkt->currentOutput );
                            ADS1220Reset( adcNumber );
                            ADS1220Configure( ADS1220_IN_AIN1_AIN2, adcNumber, (ADS1220_IDAC_CURRENT) pSetTecPkt->currentOutput );
                            SetAdcAverage( adcNumber, pSetTecPkt->avgFactor );
                        }
                    }
                    else
                    {
                        BYTE slaveAdcNbr;
                        slaveAdcNbr = pSetTecPkt->numberADC - SLAVE_ADC_NBR_OFFSET; // In the command data packet the slave ADC addressing starts from 6 ( ch0 is 6, ch1 is 7 ...)
                        SetAdcCurrent( slaveAdcNbr, pSetTecPkt->currentOutput );
                        ADS1220Reset( slaveAdcNbr );
                        ADS1220Configure( ADS1220_IN_AIN1_AIN2, slaveAdcNbr, (ADS1220_IDAC_CURRENT) pSetTecPkt->currentOutput );
                        SetAdcAverage( slaveAdcNbr, pSetTecPkt->avgFactor );
                    }

                    SlaveTecSetParamsRspRs485();
                    break;
                case COM_GET_VERSIONS:
                    SlaveTecVerRspRs485();
                    break;
                default:
                    break;
            }
        }
    }

}


//******************************************************************************
//
//  Function: SlaveTecVerRspRs485
//
//  Arguments: none
//
//  Returns: none
//
//  Description: send response to Get versions command
//
//******************************************************************************
void SlaveTecVerRspRs485( void)
{

    COM_GET_VERSIONS_RSP_PKT *pVerRsp;


    // Initialize the pointers
    pktCommHdr = (COMM_PKT_HDR*)rs485Packet;
    pVerRsp = (COM_GET_VERSIONS_RSP_PKT*)&rs485Packet[sizeof(COMM_PKT_HDR)];
    pCommCRC = (BYTE*)&rs485Packet[sizeof(COMM_PKT_HDR)+sizeof(COM_GET_VERSIONS_RSP_PKT)];

    pktCommHdr->pktHdr = TEC_S_RESP_HDR;
    pktCommHdr->pktType = COM_GET_VERSIONS;
    pktCommHdr->pktLength = COMM_PKT_LEN_NO_DATA + sizeof(COM_GET_VERSIONS_RSP_PKT);

    //GetSerNumber(&pVerRsp->devSerNum[0], CMD_SER_NUM_BYTES );
    ReadPageFromFlash(VERS_PAGE_LOW, &pVerRsp->devSerNum[0], (WORD)NBR_BYTES_PER_WTTTS_PAGE);       // TODO - check the page number with Ken
    ReadPageFromFlash(VERS_PAGE_HIGH, &pVerRsp->devSerNum[16], (WORD)NBR_BYTES_PER_WTTTS_PAGE);

    pVerRsp->frmVers[0] = MAJOR_VERSION;
    pVerRsp->frmVers[1] = MINOR_VERSION;
    pVerRsp->frmVers[2] = BUILD_NUMBER;
    pVerRsp->frmVers[3] = REL_DBG;

    pVerRsp->hardVer[0] = GetHardwareRev();
    pVerRsp->hardVer[1] = 0;
    pVerRsp->hardVer[2] = 0;
    pVerRsp->hardVer[3] = 0;

    *pCommCRC = CalculateCRC(rs485Packet, sizeof(COMM_PKT_HDR)+sizeof(COM_GET_VERSIONS_RSP_PKT));

    rs485SendBinaryData(rs485Packet,  sizeof(COMM_PKT_HDR)+sizeof(COM_GET_VERSIONS_RSP_PKT)+1);
}


//******************************************************************************
//
//  Function: SlaveTecSetParamsRspRs485
//
//  Arguments: none
//
//  Returns: none
//
//  Description: send response to Set Parameters command
//
//******************************************************************************
void SlaveTecSetParamsRspRs485( void)
{

    TEC_S_SET_PARAM_RSP_PKT *pParamsRsp;
    TEC_S_SET_PARAM_CMD_PKT *pSetTecCmdPkt;

    // Initialize the pointers
    pSetTecCmdPkt = (TEC_S_SET_PARAM_CMD_PKT*) pktPibData;
    pktCommHdr = (COMM_PKT_HDR*)rs485Packet;
    pParamsRsp = (TEC_S_SET_PARAM_RSP_PKT*)&rs485Packet[sizeof(COMM_PKT_HDR)];
    pCommCRC = (BYTE*)&rs485Packet[sizeof(COMM_PKT_HDR)+sizeof(TEC_S_SET_PARAM_RSP_PKT)];

    pktCommHdr->pktHdr = TEC_S_RESP_HDR;
    pktCommHdr->pktType = TEC_S_SET_PARAMS;
    pktCommHdr->pktLength = COMM_PKT_LEN_NO_DATA + sizeof(TEC_S_SET_PARAM_RSP_PKT);

    pParamsRsp->numberADC = pSetTecCmdPkt->numberADC;
    pParamsRsp->currentOutput = pSetTecCmdPkt->currentOutput;
    pParamsRsp->avgFactor = pSetTecCmdPkt->avgFactor;

    *pCommCRC = CalculateCRC(rs485Packet, sizeof(COMM_PKT_HDR)+sizeof(TEC_S_SET_PARAM_RSP_PKT));

    rs485SendBinaryData(rs485Packet,  sizeof(COMM_PKT_HDR)+sizeof(TEC_S_SET_PARAM_RSP_PKT)+1);
}


//******************************************************************************
//
//  Function: SlaveTecGetDataRspRs485
//
//  Arguments: none
//
//  Returns: none
//
//  Description: send response to Get Data command
//
//******************************************************************************
void SlaveTecGetDataRspRs485( void)
{

    TEC_S_GET_DATA_RSP_PKT *pDataRsp;

    // Initialize the pointers

    pktCommHdr = (COMM_PKT_HDR*)rs485Packet;
    pDataRsp = (TEC_S_GET_DATA_RSP_PKT*)&rs485Packet[sizeof(COMM_PKT_HDR)];
    pCommCRC = (BYTE*)&rs485Packet[sizeof(COMM_PKT_HDR)+sizeof(TEC_S_GET_DATA_RSP_PKT)];

    pktCommHdr->pktHdr = TEC_S_RESP_HDR;
    pktCommHdr->pktType = TEC_S_GET_DATA;
    pktCommHdr->pktLength = COMM_PKT_LEN_NO_DATA + sizeof(TEC_S_GET_DATA_RSP_PKT);

    ConvertLongTo3Bytes(uvResultAverage[0], pDataRsp->ch1);
    ConvertLongTo3Bytes(uvResultAverage[1], pDataRsp->ch2);
    ConvertLongTo3Bytes(uvResultAverage[2], pDataRsp->ch3);
    ConvertLongTo3Bytes(uvResultAverage[3], pDataRsp->ch4);
    ConvertLongTo3Bytes(uvResultAverage[4], pDataRsp->ch5);
    ConvertLongTo3Bytes(uvResultAverage[5], pDataRsp->ch6);

    pDataRsp->batType = GetBatType();

    *pCommCRC = CalculateCRC(rs485Packet, sizeof(COMM_PKT_HDR)+sizeof(TEC_S_GET_DATA_RSP_PKT));

    rs485SendBinaryData(rs485Packet,  sizeof(COMM_PKT_HDR)+sizeof(TEC_S_GET_DATA_RSP_PKT)+1);
}


BOOL IsRs485CmdRcvd(void)
{
    if(haveRs485Cmd)
    {
        haveRs485Cmd = FALSE;
        return TRUE;
    }

    return FALSE;
}
