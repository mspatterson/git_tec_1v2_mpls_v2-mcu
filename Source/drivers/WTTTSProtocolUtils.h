//******************************************************************************
//
//  WTTTSProtocolUtils.h:
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-     		KM          Initial Implementation
//	2013-Sep-18 	DD			Update RFC packet structure
//	2013-Nov-04		DD 			Update RFC packet structure - RFC now reports voltage and mA/h used.
//*******************************************************************************


#ifndef WTTTSProtocolUtilsH
#define WTTTSProtocolUtilsH

#include "drivers.h"

//
// Packet Definitions for MPLS Communications
// Refer to Specification 153-0084 "Multi Plug Launcher Wireless Protocol Specification" for further info
//
// Note: the capability exists to perform over-the-air updating of
// the WTTTS firmware. This capability is only implemented in a
// specialized software program. Therefore, all definitions related
// to that ability have been excluded from this file.
//


// Packet formats (inbound and outbound). All structures are byte-aligned
//#pragma pack(push, 1)

// Packet header
typedef struct {
    BYTE pktHdr;       // WTTTS_HDR_... define
    BYTE pktType;      // WTTTS_CMD_... or WTTTS_RESP_... define
    BYTE seqNbr;       // Incremented on each tx packet. Rolls over to zero at 255
    BYTE dataLen;      // Number of bytes of packet data to follow; can be zero
    WORD timeStamp;    // Packet time, in 10's of msec, since device power-up
} WTTTS_PKT_HDR;



// Request for command packet
#define NBR_WTTTS_PRESSURE_BYTES  3

// Payload for set rate command.
typedef enum {
    SRT_RFC_RATE,                   // Sets RFC tx rate, in msecs
    SRT_RFC_TIMEOUT,                // Sets how long the WTTTS receiver remains active after sending a RFC
    SRT_STREAM_RATE,                // Sets the rate at which stream packets are reported
    SRT_STREAM_TIMEOUT,             // Sets how long the WTTTS will send stream packets before automatically stopping
    SRT_PAIR_TIMEOUT,               // Sets how long before the WTTTS enters 'deep sleep' if it can't connect
	SRT_SOLENOID_TIMEOUT,			// Used by PIB board to determine the max time a solenoid can be in the active state
    NBR_SET_RATE_TYPES
} WTTTS_RATE_TYPE;

typedef struct {
    WORD rfcRate;                 // One of the SRT_ enums from above
    WORD rfcTimeout;                      // Must always be zero
    WORD pairTimeout;                 // Value in secs or msecs, see enum defs above
} WTTTS_RATE_PKT;

// The following payloads are used to get / set configuration information.
// Configuration data is stored in in pages. Each page consists of 16 bytes
// of data. All pages can stored in the WTTTS can be read. However, the host
// can only write to a subset of all pages.
#define NBR_WTTTS_CFG_PAGES        32
#define WTTTS_CFG_FIRST_HOST_PAGE  16
#define NBR_BYTES_PER_WTTTS_PAGE   16

#define WTTTS_PG_RESULT_SUCCESS    0
#define WTTTS_PG_RESULT_BAD_PG     1

typedef struct {
    BYTE pageNbr;
} WTTTS_CFG_ITEM;

typedef struct {
    BYTE pageNbr;
    BYTE result;    // Always zero host to WTTTS; from WTTTS: 0 = success, 1 = invalid page
    BYTE pageData[NBR_BYTES_PER_WTTTS_PAGE];
} WTTTS_CFG_DATA;

// Unit version information.
#define WTTTS_FW_VER_LEN   4

typedef struct {
    BYTE hardwareSettings;          // Physical jumper settings on sub
    BYTE fwVer[WTTTS_FW_VER_LEN];   // Firmware version info
} WTTTS_VER_PKT;

// Set RF Channel command
typedef struct {
    BYTE chanNbr;     // 11 through 26
} WTTTS_SET_CHAN_PKT;

// Set RF Channel command
typedef struct {
    BYTE deviceSel;     // -    1 = get master devices IDs, -   2 = get slave device IDs
} WTTTS_GET_VER_PKT;



// MPLS RFC - updated Nov 12 2018
typedef struct {
    WORD slaveBattVoltage;      // Voltage in mV (eg 3.742V would be 3742)
    BYTE slaveTankPressure[NBR_WTTTS_PRESSURE_BYTES];   //24 bit raw A/D value for pressure.
    BYTE slaveRegPressure[NBR_WTTTS_PRESSURE_BYTES];    //24 bit raw A/D value for pressure.
    WORD masterBattVoltage;     //Voltage in mV (eg 3.742V would be 3742)
    BYTE masterTankPressure[NBR_WTTTS_PRESSURE_BYTES];  //24 bit raw A/D value for pressure.
    BYTE masterRegPressure[NBR_WTTTS_PRESSURE_BYTES];   //24 bit raw A/D value for pressure.
    BYTE rpm;                   // current RPM
    BYTE temperature;           // signed value, 0.5C per count with, -64C to +63.5C
//  BYTE numberPlugReadings;    // number of readings received from Plug Detector
    WORD plugData[32];          // 32 readings, 2 bytes each.
    BYTE systemStatusBits[WORD_SIZE];      // 1 = power up, 2 = WDT, 3 = brownout reset, Master Battery Type, Slave Battery type
    BYTE solenoidState;         // Lower nibble for the Master PIB. Upper nibble for Slave PIB solenoid used. Only one Solenoid can be active.
    BYTE contactState;          // Lower nibble for Master outputs. Upper nibble for Slave outputs in a bit format where 1=ON.
    BYTE slaveSolenoidCurrent;  // integer value in mA.
    BYTE masterSolenoidCurrent; // integer value in mA.
    BYTE rfuArray[4];           // reserved for future use - 4 bytes
} MPL_RFC_PKT;

//typedef struct {
//    BYTE masterTECSerNum[4];
//    BYTE masterTECFwVer[4];
//    BYTE masterTECHwVer[4];
//    BYTE slaveTECSerNum[4];
//    BYTE slaveTECFwVer[4];
//    BYTE slaveTECHwVer[4];
//    BYTE masterPIBSerNum[4];
//    BYTE masterPIBFwVer[4];
//    BYTE masterPIBHwVer[4];
//    BYTE slavePIBSerNum[4];
//    BYTE slavePIBFwVer[4];
//    BYTE slavePIBHwVer[4];
//    BYTE plugDetSerNum[4];
//    BYTE plugDetFwVer[4];
//    BYTE plugDetHwVer[4];
//}MPL_VERSION_PKT;

typedef struct {
    BYTE devId;
    BYTE tecSerNum[32];
    BYTE tecHwVer[4];
    BYTE tecFwVer[4];
    BYTE pibHwVer[4];
    BYTE pibFwVer[4];
    BYTE plugDetSerNum[32];
    BYTE plugDetHwVer[4];
    BYTE plugDetFwVer[4];
}MPL_VERSION_PKT;


typedef struct {
    BYTE targetPIB; // 1 = Master PIB, 2 = Slave PIB
    BYTE solenoidState;     //0 = turn off all solenoids, 1-5 = turn on solenoid, 0xFF = do not change solenoid state.
    BYTE solenoidTimeout; // PIB will disable the solenoid after 1 to 255 seconds.
    BYTE contactState;  //Bit flags indicating the desired state of the output contacts (1 = contact closed, 0 = contact open). Bits D0 through D3 are valid.   Bits D4 through D7 must always be zero.
}MPL_SET_PIB_CONTACTS_PKT;

// Set TEC parameters.
typedef struct {
    BYTE numberADC; //Address of the A/D be set. 0x01 to 0x0C.The first 6 are the Master A/D�s and the next 6 are the Slave A/D�s. A value of 0 sets all A/D to the same settings below
    BYTE currentOutput;
    BYTE avgFactor;
}MPL_SET_TEC_PARAM_PKT;

// Set Plug Detector Sensor parameters.
typedef struct {
    BYTE magnSampleRate;
    BYTE magnAvg;
}MPL_SET_PLUG_DET_PKT;


typedef enum{
    RF_RESP_IDLE,
    RF_RESP_SET_PIB,
    RF_RESP_SET_TEC,
    RF_RESP_SET_PLUG,
}MPL_RF_RESP_ST;


// All packet payloads are defined in the following union
typedef union {
    MPL_RFC_PKT        rfcPkt;
    WTTTS_CFG_DATA     cfgData;
    WTTTS_CFG_ITEM     cfgRequest;
    MPL_VERSION_PKT    verPkt;
    WTTTS_RATE_PKT     ratePkt;
    WTTTS_SET_CHAN_PKT chanPkt;
} WTTTS_DATA_UNION;

//#pragma pack(pop)

// Acknowledge to set command
typedef struct {
    BYTE cmdType;
    BYTE ackResp;     // 11 through 26
} WTTTS_ACK_RESP_PKT;


#define SIZEOF_WTTTS_CHECKSUM  1

#define MIN_WTTTS_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + SIZEOF_WTTTS_CHECKSUM )
#define MAX_WTTTS_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + sizeof( WTTTS_DATA_UNION ) + SIZEOF_WTTTS_CHECKSUM )


// Packet header defines
//#define WTTTS_HDR_RX   0x29       // inbound from WTTTS
//#define WTTTS_HDR_TX   0x46       // outbound to WTTTS
#define MPL_HDR_FROM_HOST	0x81	// packet sent by Host
#define MPL_HDR_FROM_DEVICE	0x15	// packet sent by Device



// Packet type definitions: outbound to WTTTS.
#define WTTTS_CMD_NO_CMD           0x80
#define WTTTS_CMD_SET_RATE         0x86
#define WTTTS_CMD_QUERY_VER        0x8A
#define WTTTS_CMD_SET_RF_CHANNEL   0x8E
#define WTTTS_CMD_SET_RF_POWER     0x8F
#define WTTTS_CMD_SET_CFG_DATA     0xA0
#define WTTTS_CMD_GET_CFG_DATA     0xA2


// Packet type definitions: inbound from WTTTS
// updated Nov 28 2018
#define WTTTS_RESP_REQ_FOR_CMD     	0x31
#define WTTTS_RESP_VER_PKT         	0x33
#define WTTTS_RESP_CFG_DATA        	0x35
#define WTTTS_RESP_SET_CMD        	0x37	// SET in response to any set

// packet type definitions for Multi-Plug Launcher
#define MPL_CMD_CHANNEL_IN_USE		0xA6
#define MPL_CMD_SET_PIB_CONTACTS	0xA8
#define MPL_CMD_SET_TEC_PARAM		0xAC	// Sets MPLS TEC  parameters.
#define MPL_CMD_SET_PLUG_DET	    0xAE	// Clears the �set� indication on either a PIB board switch input or a sensor board digital input.
//#define MPL_RESP_RFC_PIB			0x2E


#define SET_CMD_ACK             0x00    // Operation Successful
#define SET_CMD_NACK            0x02    // Slave Error: communication timeout
#define SET_CMD_NACK_PARAM_ERR  0x01    // Parameter error
#define SET_CMD_NACK_OTHER      0X03    //

#define MASTER_ADC_CHNLS    6

#define SET_TEC_MASTER     0x80     // D7: bit set if Master TEC A/Ds to be set
#define SET_TEC_SLAVE      0x40     // D6: bit set if Slave TEC A/Ds to be set
#define SET_MASTER_PIB      1
#define SET_SLAVE_PIB       2

#define VER_SEL_MASTER      1
#define VER_SEL_SLAVE       2

#define VERS_PAGE_LOW       9
#define VERS_PAGE_HIGH      10

void UpdateTimeParamsFromFlash(void);

void SetTimeParameter(BYTE *paramValues);

WORD GetTimeParameter(const WTTTS_RATE_TYPE paramNum);

void SendCfgEvent(void);

void SendRfc(void);

BOOL HaveWtttsPacket(BYTE *pBuff, WORD buffLen);

void ExecuteCommand(void);

void SendVersionInfo(BYTE* devId);

void SetNoCommand(BOOL bVal);

void SetPowerDown(BOOL bVal);

BOOL GetPowerDownStatus(void);


void ConvertLongTo3Bytes(signed long slValue, BYTE *pData);

void ConvertLongTo4Bytes(signed long slValue, BYTE *pData);

BOOL GetCommandStatus(void);

BOOL GetRadioPacketStatus(void);

void ClearRadioPacketStatus(void);

BOOL IsRadioBusy(void);

void SetRadioBusy(BOOL bValue);

void SaveSerNumber(BYTE *pData);

void mplSendMinMax(void);

void mplSetPibContacts(BYTE bmContacts);

void mplSetPibSolenoid(BYTE nbrSolenoid, BYTE stateSolenoid);

void mplSendRfcPIB(void);

void* getPibParamAddr(void);

void* getPTSParamAddr(BYTE nbrPts);

//void mplResetPibInput(BYTE devAddress, BYTE switchFlag);


void UpdateRfRespState(void);

void SendAckResp(BYTE cmdType, BYTE ackResp);

BYTE getAdcAverage(BYTE adcNbr);

BYTE CalculateCRC(BYTE *bPointer, WORD wLength);


void GetSerNumber(BYTE *pDest, BYTE nbrBytes);


void SetAdcAverage(BYTE adcNbr, BYTE value);


void mplResendRFCpib(void);


BOOL IsCmdWaitRespIdle(void);

#endif



