#ifndef UTILS_H_
#define UTILS_H_

#include "drivers.h"



//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

//#define IsNum( aChar )      ( ( ( aChar >= '0' ) && ( aChar <= '9' ) ) ? TRUE : FALSE )
//#define CharToInt( aChar )  ( ( ( aChar >= '0' ) && ( aChar <= '9' ) ) ? aChar - '0' : aChar )

#define NIBBLES_PER_BYTE    ( sizeof( BYTE )  * 2 /* nibbles/byte */ )
#define NIBBLES_PER_WORD    ( sizeof( WORD )  * 2 /* nibbles/byte */ )
#define NIBBLES_PER_DWORD   ( sizeof( DWORD ) * 2 /* nibbles/byte */ )


//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS PROTOTYPES
//------------------------------------------------------------------------------

//******************************************************************************
//
//  Function: IsHexChar
//
//  Arguments:
//    IN  byChar - An ASCII character to test
//
//  Returns: TRUE if the char is a valid hex character, FALSE otherwise
//
//  Description: Checks that the passed char is 0-9, a-f, or A-F
//
//******************************************************************************
BOOL IsHexChar( BYTE byChar );


//******************************************************************************
//
//  Function: HexToByte
//
//  Arguments:
//    IN  aChar - hex char to convert
//
//  Returns: binary value of passed hex char
//
//  Description: Converts the passed hex char to its binary value
//
//******************************************************************************
BYTE HexToByte( char aChar );


//******************************************************************************
//
//  Function: AsciiToByte
//
//  Arguments:
//    IN  byChar - An ASCII character to convert from.
//
//  Returns: A byte value representation of the ASCII character on success.
//           0 on failure.
//
//  Description: This function converts the passed ASCII character
//               to a byte value representation (supports both hex and
//               decimal values).
//
//******************************************************************************
BYTE AsciiToByte( BYTE byChar );


//******************************************************************************
//
//  Function: CountDelimitersInString
//
//  Arguments:
//    IN  inputLine     - null terminated line to parse
//    IN  delimiter     - character delimiter
//
//  Returns: number of delimited strings in the passed inputLine
//
//  Description: This function counts how many substrings exist in
//               inputLine where the delimiter char defines each
//               substring. Note that back-to-back delimiters count
//               as a line (albeit a zero-length line).
//
//               Note: if the delimiter is CR or LF, CR/LF pairs
//               (in any order) are treated as a single delim char.
//
//******************************************************************************
BYTE CountDelimitersInString( const char* inputLine, char delimiter );


//******************************************************************************
//
//  Function: CountDelimitersInBuff
//
//  Arguments:
//    IN  inputBuffer   - array of bytes to check
//    IN  inputCount    - number of bytes in the array
//    IN  delimiter     - character delimiter
//
//  Returns: number of delimited strings in the passed inputLine
//
//  Description: This function counts how many substrings exist in
//               inputLine where the delimiter char defines each
//               substring. Note that back-to-back delimiters count
//               as a line (albeit a zero-length line).
//
//               Note: if the delimiter is CR or LF, CR/LF pairs
//               (in any order) are treated as a single delim char.
//
//******************************************************************************
BYTE CountDelimitersInBuff( const BYTE* inputBuffer, WORD inputCount, char delimiter );


//******************************************************************************
//
//  Function: ParseItemsFromString
//
//  Arguments:
//    OUT parsedLines   - array of pointers to parsed strings in inputLine
//    IN  maxLines      - max nbr of items in parsedlines array
//    IN  inputLine     - null terminated line to parse
//    IN  delimiter     - character delimiter
//
//  Returns: the number of items parsed
//
//  Description: This function parses an input line into individual lines.
//               Each time the delimiter is encountered, a pointer to the
//               start of the characters is saved in the parsedLines array
//               and the delimiter is replaced by a null-term char. If
//               back-to-back delimiters, one of the parseLines pointers
//               will point to a zero-length string. At most maxLines are
//               parsed out of the inputLine.
//
//               Note 1: if the delimiter is CR or LF, CR/LF pairs
//               (in any order) are treated as a single delim char.
//
//               Note 2: THIS FUNCTION CHANGES THE CONTENTS OF inputLine
//
//               Note 3: unused entries in parsedLines will be set to
//                       a NULL pointer
//
//******************************************************************************
BYTE ParseItemsFromString( PCHAR parsedLines[], BYTE maxLines, char* inputLine, char delimiter );


//******************************************************************************
//
//  Function: ParseItemsFromBuff
//
//  Arguments:
//    OUT parsedLines   - array of char arrays to receive parsed data
//    IN  maxLines      - max nbr of items in parsedlines array
//    IN  inputLine     - array of bytes to parse
//    IN  buffLen       - number of bytes in the input line
//    IN  delimiter     - character delimiter
//
//  Returns: the number of items parsed
//
//  Description: This function parses an input line into individual lines.
//               Each time the delimiter is encountered, a pointer to the
//               start of the characters is saved in the parsedLines array
//               and the delimiter is replaced by a null-term char. If
//               back-to-back delimiters, one of the parseLines pointers
//               will point to a zero-length string. At most maxLines are
//               parsed out of the inputLine.
//
//               Note 1: if the delimiter is CR or LF, CR/LF pairs
//               (in any order) are treated as a single delim char.
//
//               Note 2: THIS FUNCTION CHANGES THE CONTENTS OF inputLine
//
//               Note 3: unused entries in parsedLines will be set to
//                       a NULL pointer
//
//******************************************************************************
BYTE ParseItemsFromBuff( PCHAR parsedLines[], BYTE maxLines, BYTE* inputLine, WORD inputLength, char delimiter );



void convInt32ToText(signed long inputValue,BYTE* pValueToTextBuffer);
void convIntToText(int inputValue,BYTE* pValueToTextBuffer);

void RegisterDebugTimer(void);
void StartDebugTimer(void);
void StopDebugTimer(void);
WORD GetDebugTimer(void);
//BYTE CalculateCRC(BYTE *bPointer, WORD wLength);
void EvalBatType(void);
BYTE GetBatType(void);


#endif /*UTILS_H_*/
