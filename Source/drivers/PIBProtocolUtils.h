//******************************************************************************
//
//  PIBProtocolUtils.h:
//
//      Copyright (c) 2016, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//	2016-Oct-04		DD 			Initial Implementation
//	2018-Oct-30		DD			Update for MPLS firmware.
//*******************************************************************************


#ifndef PIBProtocolUtilsH
#define PIBProtocolUtilsH

#include "drivers.h"


#define PTS_DATA_SIZE   32

// Packet formats (inbound and outbound). All structures are byte-aligned
//#pragma pack(push, 1)

// Packet header
typedef struct {
    BYTE pktHdr;       // Packet HDR_... define
    BYTE pktType;      // defines what the packet is for. Command or Response...
    BYTE pktLength;    // Is the number of bytes remaining in the packet, including the CRC not including Header and itself
} COMM_PKT_HDR;


// Get PIB CMD packet consist only of header and checksum

typedef struct {
	BYTE solenoidCtrl;	// bit flags to activate or deactivate the corresponding solenoid
	BYTE contactCtrl;	// bit flags to close or open the contacts.
	BYTE solenoidTimeout;	// 0 to 255 seconds. The maximum time a solenoid can be in active state, measured in seconds. Default is 10 sec.
} PIB_SET_PARAM_CMD_PKT;


typedef struct {
    BYTE batVoltage[WORD_SIZE]; 		// Battery voltage in mV
	BYTE solenoidStatus;	// Solenoid number 0 to 5. 0 = all solenoids off
	BYTE solenoidTimeout;	// The maximum time a solenoid can be in active state, measured in seconds. Default is 10 sec. Range is 0 to 255 seconds.
	BYTE solenoidCurrent;	// Solenoid current in mA
	BYTE contactStatus;		// Lower four bits represents outputs 1, 2, 3 or 4. 0=off, 1=on.
} PIB_RSP_PKT;


typedef struct {
	BYTE ch1[ADC_BYTES];
	BYTE ch2[ADC_BYTES];
	BYTE ch3[ADC_BYTES];
	BYTE ch4[ADC_BYTES];
	BYTE ch5[ADC_BYTES];
	BYTE ch6[ADC_BYTES];
	BYTE batType;
} TEC_S_GET_DATA_RSP_PKT;


typedef struct {
	BYTE numberADC;
	BYTE currentOutput;		//
	BYTE avgFactor;		// The number of readings done per RFC then averaged.
} TEC_S_SET_PARAM_CMD_PKT;

typedef struct {
	BYTE numberADC;
	BYTE currentOutput;		//
	BYTE avgFactor;		// The number of readings done per RFC then averaged.
} TEC_S_SET_PARAM_RSP_PKT;


//typedef struct {
//	BYTE magnetSampleRate;	// magnetometer sample rate
//	BYTE averagingFactor;	// Averaging coefficient �alpha�. Must be one of 1, 2, 4, 8, 16, 32, 64, or 128.
//	BYTE readingPerSecond;
//	BYTE nbrDataRead;		//Number of readings taken since the last poll.
//	WORD sensData[PTS_DATA_SIZE];
//} PTS_GET_DATA_RSP_PKT;

// Updated on Dec 05 2018 as per Firmware spec. Rev.E
typedef struct {
    BYTE magnetSampleRate;  // Bits 0 to 6 are magnetometer sample rate. Bit 7 is Flag status bit.
    BYTE averagingFactor;   // Averaging coefficient �alpha�. Must be one of 1, 2, 4, 8, 16, 32, 64, or 128.
    WORD sensData[PTS_DATA_SIZE];
} PTS_GET_DATA_RSP_PKT;



typedef struct {
	BYTE magnetSampleRate;	// magnetometer sample rate
	BYTE averagingFactor;	// Averaging coefficient �alpha�. Must be one of 1, 2, 4, 8, 16, 32, 64, or 128.
} PTS_SET_PARAM_CMD_PKT;

typedef struct {
	BYTE magnetSampleRate;	// magnetometer sample rate
	BYTE averagingFactor;	// Averaging coefficient �alpha�. Must be one of 1, 2, 4, 8, 16, 32, 64, or 128.
} PTS_SET_PARAM_RSP_PKT;

typedef struct {
    BYTE hardVer[4];
    BYTE frmVers[4];        //
    BYTE devSerNum[32];      //
} COM_GET_VERSIONS_RSP_PKT;




#define SIZEOF_COMM_CHECKSUM  1

#define MIN_COM_PKT_LEN      ( sizeof( COMM_PKT_HDR ) + SIZEOF_COMM_CHECKSUM )
#define MAX_COM_PKT_LEN      ( sizeof( COMM_PKT_HDR ) + sizeof( PTS_GET_DATA_RSP_PKT ) + SIZEOF_COMM_CHECKSUM )



#define TEC_S_CMD_HDR	0x20	// Slave TEC Command Header. Used in packets sent by the Master TEC board.
#define PIB_M_CMD_HDR	0x10	// Master PIB Command Header. Used in packets sent by the Master TEC board.
#define PIB_S_CMD_HDR	0x30	// Slave PIB Command Header. Used in packets sent by the Master TEC board.
#define PTS_CMD_HDR		0x40	// PTS Command Header. Used in packets sent by the Master TEC board.

#define TEC_S_RESP_HDR	0x25	// Slave TEC Response Header. Used in packets sent by the Slave TEC board.
#define PIB_M_RESP_HDR	0x15	// Master PIB Response Header. Used in packets sent by the Master PIB board.
#define PIB_S_RESP_HDR	0x35	// Slave PIB Response Header. Used in packets sent by the Master PIB board.
#define PTS_RESP_HDR	0x45	// PTS Response Header. Used in packets sent by the PTS board.

// Packet type definitions - commands:
#define PIB_GET_DATA		0x50
#define PIB_SET_PARAMS		0x52
#define TEC_S_GET_DATA		0x54
#define TEC_S_SET_PARAMS	0x56
#define PTS_GET_DATA		0x58
#define PTS_SET_PARAMS		0x5A
#define COM_GET_VERSIONS	0x5C

#define NO_POST_CMD         0
#define POST_MPIB_SET_PRMS  0x91
#define POST_SPIB_SET_PRMS  0x92
#define POST_STEC_SET_PRMS  0x94
#define POST_PLUG_SET_PRMS  0x96

//#define PIB_SET_M_RSP   0x03
//#define PIB_SET_S_RSP   0x30
//#define PIB_SET_ALL_RSP 0x33

#define PIB_SET_ANY_RSP 0x33

#define TEC_SET_S_RSP   0x05
#define PTS_SET_RSP     0x06
#define SET_RSP_TIMEOUT 0x0F

#define COMM_PKT_LEN_NO_DATA	0   //- updated on Dec 05 2018 to 0 as per firmware specification RevE. It was 2 before.

#define MASTER_PIB_BOARD    0
#define SLAVE_PIB_BOARD     1

#define PLUG_SENS_NBR_READINGS  32

#define PTS_SWITCH_TRIPPED  1
#define PTS_SWITCH_RESET    0

#define COM_RETRIES     3

typedef enum slave_boards{ PIB_M_BOARD, PIB_S_BOARD,TEC_S_BOARD, PTS_BOARD, TOTAL_NBR_SLAVES} SlaveBoardsT;

void InitPIBInterface(void);
void PibGetData(BYTE pibSelect);
void PibSetParamRs485(SlaveBoardsT pibSelect);
void PtsSetParamRs485(void);
void PtsSetParamRf( BYTE magnetSampleRate, BYTE averagingFactor);
void PtsGetDataRs485(void);
void TecSlaveGetData(void);
void TecSlaveSetParamRs485( void);
void TecSlaveSetParamRf( BYTE adcChannel, BYTE adcCurrent, BYTE adcAveraging);

void PibSetParamFromRf(BYTE targetPib, BYTE solCtrl, BYTE contCtrl, BYTE solTimeout);

void GetCommHwFwVer(SlaveBoardsT boardSel);


BOOL HavePIBPacket(BYTE *pBuff, WORD buffLen);

BOOL SlaveTecHavePIBPacket(BYTE *pBuff, WORD buffLen);


void UpdateMplCommState(void);

void ResetPibInputs(BYTE devAddress, BYTE switchFlag);

void PostRs485Command(BYTE bCmd);
BYTE IsRs485RspAvail(void);
void StartDataCollection(void);

void GetPibSlaveVoltage(WORD *pSlaveVoltage);
void GetPibMasterVoltage(WORD *pMasterVoltage);
void GetPtsData(WORD *pToDataArray);
void GetSlaveTankPressure(BYTE *pToDataArray);
void GetSlaveRegPressure(BYTE *pToDataArray);
BYTE GetPibSolenoidStatus(void);
BYTE GetPibContactStatus(void);
BYTE GetPibSlaveCurrent(void);
BYTE GetPibMasterCurrent(void);
BYTE GetSlaveBatteryType(void);
BOOL PowerUpBoardPresent(SlaveBoardsT board);
BOOL CommBoardNotPresent(SlaveBoardsT board);
BOOL IsWireCommIdle(void);
void GetSlaveHwFwVer(SlaveBoardsT boardSel, BYTE * destData);

BYTE GetPtsFlagBit(void);

void UpdateSlaveTecCommState(void);
void SlaveTecVerRspRs485( void);
void SlaveTecSetParamsRspRs485( void);
void SlaveTecGetDataRspRs485( void);
BOOL IsRs485CmdRcvd(void);

void GetSlaveTecHwVer(BYTE * destData);
void GetSlaveTecFwVer(BYTE * destData);
void GetSlaveTecSerNum(BYTE * destData);
void GetPlugHwVer(BYTE * destData);
void GetPlugFwVer(BYTE * destData);
void GetPlugSerNum(BYTE * destData);
void GetPibHwVer(SlaveBoardsT boardSel,BYTE * destData);
void GetPibFwVer(SlaveBoardsT boardSel,BYTE * destData);


#endif	/*PIBProtocolUtilsH*/
