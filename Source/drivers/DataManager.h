#ifndef DATA_MAN_H_
#define DATA_MAN_H_

#include "drivers.h"

#define NUM_OF_DATA		10

// parameter number definitions - to be updated
#define PARAM_0	0
#define PARAM_1	1
#define PARAM_2	2
#define PARAM_3	3
#define PARAM_4	4
#define PARAM_5	5
#define PARAM_6	6
#define PARAM_7	7
#define PARAM_8	8
#define PARAM_9	9

#define MAX_NUMBER_PAGES	32
#define PROTECTED_PAGES		8
#define BYTES_IN_PAGE		16
#define PAGES_IN_SEGMENT	8
#define BYTES_IN_SEGMENT	128


#define UPDATE_RATE_PAGE_IN_FLASH	8


BYTE WritePageInFlash(BYTE page, BYTE *pData, WORD length);
BOOL ReadPageFromFlash(BYTE page, BYTE *pData, WORD length);


#endif /*DATA_MAN_H_*/

