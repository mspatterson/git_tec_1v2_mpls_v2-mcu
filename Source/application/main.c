//******************************************************************************
//
// main.c
//
//      Copyright (c) 2012, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2012-     		DD          Initial Implementation
//	....
//
//	2013-09-18 		DD			Update RFC packet structure
//	2013-10-22		DD			Turn On/Off the Strain gauges in Idle mode. Strain gauges ADCs initialised in InitIdleMode() only.
//	2014-07-17		DD			use ADC internal IDAC instead of external Vref.
//	2018-11-25		DD			MPLS TEC firmware.
//  2019-02-12      DD          MPLS TEC firmware - 0v08- radio ON/OFF in master mode.
//*******************************************************************************


#include "..\drivers\drivers.h"


#define ZB_TX_PERIOD		5		// +1ms to read the compass 			//in ms
#define IGNORE_FIRST_SAMPLES	500 //2000
#define OFFSET_CALIBRATION_SAMPLES	10
#define COUNTS_PER_POUND	64
#define ADC_OFFSET_TIMEOUT	10000

#define ALL_CHANNELS_OFFSET_DONE	0x7F	//0x3F
#define ZB_NETWORK_TIMEOUT		10
#define SAMPLES_IN_PACKET		2

#define ADC_OFFSET_SAMPLES		1000

MAGNETIC_DATA	sMagnetData;

ACCELEROMETOR_DATA	sAccelData;


typedef enum
{
	ST_INIT,
	ST_HUNT,
	ST_IDLE,
	ST_DEEP_SLEEP
}WTTTS_STATES;

WTTTS_STATES currentState;

const WTTTS_RATE_TYPE getRfcRate = SRT_RFC_RATE;
const WTTTS_RATE_TYPE getRfcTimeout = SRT_RFC_TIMEOUT;
const WTTTS_RATE_TYPE getStreamRate = SRT_STREAM_RATE;
const WTTTS_RATE_TYPE getStreamTimeout = SRT_STREAM_TIMEOUT;
const WTTTS_RATE_TYPE getPairTimeout = SRT_PAIR_TIMEOUT;
const UVOLTS SG_DIVIDER_VALUE = 2050000l;


//char * pcString;
static BYTE adcNumber;
static ADS1220_SOURCE analogChannel;
static BYTE bDataArray[30] = {0};
static UVOLTS uvResultTemp[ADS1220_TOTAL] = {0};
UVOLTS uvResultAverage[ADS1220_TOTAL];
UVOLTS uvResultAverageTemp[ADS1220_TOTAL];


//static UVOLTS uvOffsetAverage[ADS1220_TOTAL];

static TIMERHANDLE thGenTimer, thGenTimer1,thGenTimer2;
static TIMERHANDLE thTimerSec1,thTimerSec2;

//WORD	wCounter;
static BYTE bStGyroId;


static BYTE pUartRxData[BUF_LENGTH];

signed long gyroSumAverage;
signed long slVar;
static WORD nbrTransmission;
static WORD nbrResponses;
static WORD maxPktLength;

WORD SamplesCounter[ADC_USED_LAST+2] = {0};
static BYTE AdcReadingDone = 0;
static BYTE firstSGSample[ADS1220_TOTAL] = {0};
static WORD batSampleCounter;
static BYTE takeSample;
//static BYTE firstGyroSample;
static BOOL readTorque;
//static BOOL readTension;
//static BOOL readCompass;
//static BOOL readAccelerometer;
//static BOOL radioRxOn;
static WORD errorPackets;

BOOL digGyroOnOff;
BOOL tensionChOn;

WORD wGyroReadCounter;
WORD digGyroSampleCounter;

ADS1220_IDAC_CURRENT idacCurrent250ua = AD1220_IDAC_CURRENT_250uA;
ADS1220_IDAC_CURRENT idacCurrent1000ua = AD1220_IDAC_CURRENT_1000uA;
ADS1220_IDAC_CURRENT idacCurrent1500ua = AD1220_IDAC_CURRENT_1500uA;
ADS1220_IDAC_CURRENT idacCurrent500ua = AD1220_IDAC_CURRENT_500uA;
ADS1220_IDAC_CURRENT idacCurrent[ADS1220_TOTAL]={AD1220_IDAC_CURRENT_100uA};



///////////////////////////////////////////////////////////
//
// Local function declaration
//
///////////////////////////////////////////////////////////

void SampleData(BYTE bIndex);

//void EpsonGyroRead(void);
void StrainGaugeRead(void);
void ReadExtGyro(void);

void SystemPowerDown(void);
void TestPowerDownMode(void);
void SystemInit(void);
void TestCompass(void);
void TestGasGauge(void);

void InitStreamMode(void);
void InitIdleMode(void);
void RfCommTask(void);
void WtttsMainTask(void);
void StreamingTask(void);
void IdleTask(void);
void PowerDownTask(void);

BOOL ReadCompass(void);

void InitHuntingMode(void);
void ConnectingTask(void);
void InitGasGauge(void);
BOOL StrainGaugeReadInIdleMode(void);
void TurnOffAllAdcRef(void);

//void TestStGyro(void);

void InitADCInRFC(void);
void PowerDownADC(void);
void ReadSerNumber(void);
//void TestSerNumber(void);
void TestSensorReadings(void);
void TestMcuAdc(void);

void StrainGaugeReadInIdleModeV1(void);
void StrainGaugeReadInIdleModeV2(void);

void StrainGaugeDummyRead(void);
void PowerDownMCU(void);
void StrainGaugeReadInIdleModeMPLS(void);
void WtttsSlaveTecTask(void);
void IdleTaskSlave(void);


void main(void)
{
	initMPS430Pins();
    EnableExtPower();
	currentState = ST_INIT;
	ReadLastResetCause();


	memset(&sMagnetData, 0,sizeof(sMagnetData));
	memset(&pUartRxData, 0,sizeof(pUartRxData));
	memset(&uvResultAverageTemp, 0,sizeof(uvResultAverageTemp));
	
	StopWatchdog();
	SetWatchdog();
//	__delay_cycles(5000);

	ResetWatchdog();
	Set_System_Clock();
	SpiA0Initialize();
	InitTimers();
	__bis_SR_register(GIE);
	EnableAllPower();
	thGenTimer = RegisterTimer();
	thGenTimer1 = RegisterTimer();
	thGenTimer2 = RegisterTimer();
	StartTimer( thGenTimer );
	StartTimer( thGenTimer2 );
	thTimerSec1 = RegisterSecTimer();
	thTimerSec2 = RegisterSecTimer();
	StartSecTimer(thTimerSec1);
	StartSecTimer(thTimerSec2);

	ResetWatchdog();
    ReadDeviceRole();

	// only the master will have radio working.
	if(IsDeviceMaster())
	{
	    InitRadioInterface();
	    // wait for the system to boot up and all slave boards to initialize.
	    ResetTimer( thGenTimer, MPLS_INIT_TIME );
	    while(!TimerExpired( thGenTimer))
	    {
	        ResetWatchdog();
	    }
	    SetRadioChannel();
	}
	else
	{
	    SetOutputPin(ZB_RSTn, FALSE);
	    //DisableRadioPower();
	}

//	ReedSwitchInit();
	SetOutputPin(SW_LED, TRUE );

	InitPIBInterface();

	EvalBatType();

	MeasureBatVoltageAdc();

	#if ST_GYRO_ENABLE
	initST();
	readSTid();
	bStGyroId = gedStId();
	SelfTestStGyro();
	calibrateStGyro();
	#endif


	SetOutputPin(SW_LED, FALSE );
	ResetTimer( thGenTimer, ZB_TX_PERIOD );
//	InitGasGauge();

//	TestStGyro();
//	TestSensorReadings();
//	TestMcuAdc();

	ReadSerNumber();
	StopRtc();
	errorPackets = 0;
	ResetSecTimer( thTimerSec2, 10 );



	if(IsDeviceMaster())
	{   // Master TEC
	    while(1)
	    {
	        ResetWatchdog();
	        RfCommTask();
	        UpdateRfRespState();
	        WtttsMainTask();
	        UpdateMplCommState();
	        // check for low battery voltage and power down
//	        if(GetBatVoltageAdc()<UVLO_LOCKOUT_MV)
//	        {
//	            SystemPowerDown();
//	        }
	    }

	}
	else
	{   // Slave TEC
	    while(1)
	    {
	        ResetWatchdog();

	        WtttsSlaveTecTask();
	        UpdateSlaveTecCommState();

	        // check for low battery voltage and power down
//	        if(GetBatVoltageAdc()<UVLO_LOCKOUT_MV)
//	        {
//	            SystemPowerDown();
//	        }

	    }
	}

}



//******************************************************************************
// Function name:    InitStrainGaugesReading
//******************************************************************************
// Description:      initialize the strain gauges reading in Idle mode
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void InitStrainGaugesReading(void)
{
	memset(&SamplesCounter, 0,sizeof(SamplesCounter));
	memset(&firstSGSample, 0,sizeof(firstSGSample));

	AdcReadingDone = 0;
	adcNumber = ADC_USED_FIRST;
}

//******************************************************************************
// Function name:    StrainGaugeReadInIdleMode
//******************************************************************************
// Description:      read all strain gauges ADC on a rotational basis "IDLE_MODE_ADC_SAMPLES" times and average them.
//
// parameters:       none
//
// Returned value:   TRUE - when all strain gauges ADC are sampled "IDLE_MODE_ADC_SAMPLES" times, FALSE otherwise
//
//******************************************************************************
BOOL StrainGaugeReadInIdleMode(void)
{

	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);

		if(firstSGSample[adcNumber] == 0)
		{
			uvResultAverage[adcNumber]=uvResultTemp[adcNumber];		// don't average the first sample
			firstSGSample[adcNumber] = 1;
		}
		else
		{
			uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]+uvResultTemp[adcNumber])/2;
		}

		SamplesCounter[adcNumber]++;
	}

	if(SamplesCounter[adcNumber] >= IDLE_MODE_ADC_SAMPLES)
	{
		AdcReadingDone|=1<< adcNumber;
	}

	adcNumber++;
	// sample all ADC except the Gyro ADC on a rotational basis
	if(adcNumber>ADC_USED_LAST)
		adcNumber = ADC_USED_FIRST;

	if(AdcReadingDone!= ALL_SIX_ADC_READ)
		return FALSE;
	else
		return TRUE;
}

//******************************************************************************
// Function name:    StrainGaugeReadInIdleMode
//******************************************************************************
// Description:      read all strain gauges ADC on a rotational basis "IDLE_MODE_ADC_SAMPLES" times and average them.
//
// parameters:       none
//
// Returned value:   TRUE - when all strain gauges ADC are sampled "IDLE_MODE_ADC_SAMPLES" times, FALSE otherwise
//
//******************************************************************************
void StrainGaugeReadInIdleModeV1(void)
{

	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);

		if(firstSGSample[adcNumber] == 0)
		{
			uvResultAverage[adcNumber]=uvResultTemp[adcNumber];		// don't average the first sample
			firstSGSample[adcNumber] = 1;
		}
		else
		{
			uvResultAverage[adcNumber]=(uvResultAverage[adcNumber]*7+uvResultTemp[adcNumber])/8;
		}

	}


	adcNumber++;
	// sample all ADC except the Gyro ADC on a rotational basis
	if(adcNumber>ADC_USED_LAST)
		adcNumber = ADC_USED_FIRST;

}

//******************************************************************************
// Function name:    StrainGaugeReadInIdleMode
//******************************************************************************
// Description:      read all strain gauges ADC on a rotational basis and accumulate them over one transmission period.
//					 Count number of samples.
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void StrainGaugeReadInIdleModeV2(void)
{

	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);

		if(firstSGSample[adcNumber] == 0)
		{
			uvResultAverage[adcNumber]=uvResultTemp[adcNumber];		// don't average the first sample
			firstSGSample[adcNumber] = 1;
		}
		else
			uvResultAverage[adcNumber] += uvResultTemp[adcNumber];

		SamplesCounter[adcNumber]++;
	}

	adcNumber++;
	// sample all ADC except the Gyro ADC on a rotational basis
	if(adcNumber>ADC_USED_LAST)
		adcNumber = ADC_USED_FIRST;

}

//******************************************************************************
// Function name:    StrainGaugeReadInIdleMode
//******************************************************************************
// Description:      read all strain gauges ADC on a rotational basis and accumulate them over one transmission period.
//                   Count number of samples.
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void StrainGaugeReadInIdleModeMPLS(void)
{
    // if the number of readings is less than total readings per cycle
    if(SamplesCounter[adcNumber] < getAdcAverage(adcNumber))
    {

        if(ADS1220HasResult(adcNumber))
        {
            ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);

            if(firstSGSample[adcNumber] == 0)
            {
                uvResultAverage[adcNumber]=uvResultTemp[adcNumber];     // don't average the first sample
                firstSGSample[adcNumber] = 1;
            }
            else
                uvResultAverage[adcNumber] += uvResultTemp[adcNumber];

            SamplesCounter[adcNumber]++;
        }
    }

    adcNumber++;
    // sample all ADC except the Gyro ADC on a rotational basis
    if(adcNumber>ADC_USED_LAST)
        adcNumber = ADC_USED_FIRST;


}


//******************************************************************************
// Function name:    AverageTheResults
//******************************************************************************
// Description:      average the ADC readings.
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void AverageTheResults(void)
{
	int i;

	// average the results if data has been read from the ADC
	// otherwise send the old values.

	for(i=ADC_USED_FIRST;i<=ADC_USED_LAST;i++)
	{
		if(SamplesCounter[i])		// we read at least one data from the ADC.
		{
			uvResultAverage[i] /= SamplesCounter[i];
		}
	}


}


//******************************************************************************
// Function name:    StrainGaugeDummyRead
//******************************************************************************
// Description:      read the SG in a temporary array
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void StrainGaugeDummyRead(void)
{
    // sample all ADC except the Gyro ADC on a rotational basis
    if(adcNumber>ADC_USED_LAST)
        adcNumber = ADC_USED_FIRST;

	if(ADS1220HasResult(adcNumber))
	{
		ADS1220ReadVoltageCont(adcNumber, &uvResultTemp[adcNumber]);
	}

	adcNumber++;

}


//******************************************************************************
// Function name:    SystemPowerDown
//******************************************************************************
// Description:      Power down all components on the board.
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void SystemPowerDown(void)
{
	//SetOutputPin(SW_LED, TRUE );
	DisableAnalogPower();
	DisableDigitalPower();
	DisableRadioPower();
	DisableAnalogGyro();

	LedSwPowerDownSequence();

	ResetTimer( thGenTimer, 1000 );
	StartTimer( thGenTimer );
	while(TimerExpired( thGenTimer)==FALSE)
	{
		ResetWatchdog();
	}
	SetOutputPin(SW_LED, FALSE );

	StopWatchdog();

	SetSystemDeepSleep();
//	SystemInit();
}


void SystemInit(void)
{
	currentState = ST_INIT;
	SetOutputPin(SW_LED, TRUE );
	memset(&sMagnetData, 0,sizeof(sMagnetData));
	memset(&pUartRxData, 0,sizeof(pUartRxData));

	//StopWatchdog();
#ifdef WATCHDOG_ENABLE
		ResetWatchdog();
#endif

	initMPS430Pins();
	EnableAllPower();
	Set_System_Clock();
	SpiA0Initialize();
//	InitTimers();
	SysTimerConfig();
	__bis_SR_register(GIE);


    if(IsDeviceMaster())
    {
        InitRadioInterface();
    }

	ReedSwitchInit();
	LedSwPowerUpSequence();

	MeasureBatVoltageAdc();
	#if COMPASS_ENABLE
	CompassInit();
//	while(CompassTxDone()==FALSE);
	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
//	CompassRegisterDump();
//	while(CompassDataReady()==FALSE);
//	CompassGetRawData(bDataArray,sizeof(bDataArray));
	#endif

	#if ST_GYRO_ENABLE
	initST();
	readSTid();
	bStGyroId = gedStId();
	SelfTestStGyro();
	calibrateStGyro();
	#endif

	ResetTimer( thGenTimer, ZB_TX_PERIOD );
	ResetWatchdog();

	memset(&firstSGSample, 0,sizeof(firstSGSample));

	SetOutputPin(SW_LED, FALSE );
}




void RfCommTask(void)
{
	static WORD pktLength;

	if (ReceiverHasData())
	{
		pktLength = ReadReceiverData(pUartRxData, sizeof(pUartRxData));
		if(pktLength >sizeof(pUartRxData))
		{
			__no_operation();
		}
		if(pktLength >maxPktLength)
		{
			maxPktLength = pktLength;// debug purpose only
		}
		if(HaveWtttsPacket(pUartRxData, pktLength))
		{
			ExecuteCommand();

			// after command different from "NO_COMMAND" send RFC after RFC_RESPONSE_TIME
			// does not send right away because the radio relies on 2 ms interval between the packets
//			if(GetCommandStatus()==FALSE)
//			{
//				ResetTimer( thGenTimer, RFC_RESPONSE_TIME );
//			}
		}
		else
		{
			errorPackets++;
		}

	}
}


void WtttsMainTask(void)
{
	switch(currentState)
	{
	case ST_INIT:
		InitHuntingMode();
		if(IsWireCommIdle()==TRUE)
		{
		    currentState = ST_HUNT;
		}
		break;
	case ST_HUNT:
		ConnectingTask();
		break;
	case ST_IDLE:
		IdleTask();
		break;
	case ST_DEEP_SLEEP:
		PowerDownTask();
		break;
	default:
		currentState = ST_INIT;
		break;

	}
}


//******************************************************************************
//
//  Function: WtttsSlaveTecTask
//
//  Arguments: none
//
//  Returns: none
//
//  Description: Slave TEC main state machine. It does not support radio communication.
//
//
//******************************************************************************
void WtttsSlaveTecTask(void)
{
    switch(currentState)
    {
    case ST_INIT:
        InitHuntingMode();
        currentState = ST_HUNT;
        break;
    case ST_HUNT:
        InitIdleMode();
        currentState = ST_IDLE;
        break;
    case ST_IDLE:
        IdleTaskSlave();
        break;
    case ST_DEEP_SLEEP:
        PowerDownTask();
        break;
    default:
        currentState = ST_INIT;
        break;

    }
}




//******************************************************************************
//
//  Function: InitHuntingMode
//
//  Arguments: none
//
//  Returns: none
//
//  Description: Initialise the board for hunting mode. It's the same as InitIdleMode for now.
//
//
//
//******************************************************************************
void InitHuntingMode(void)
{
	EnableAllPower();

    if(IsDeviceMaster())
    {
        InitRadioInterface();
    }
	ResetWatchdog();
	UpdateTimeParamsFromFlash();

	// set the compass in power down with command. Do not turn of the power
	//because it pulls low the I2C bus
	// TO DO

	// TO DO - ST gyro init or not ?

	// turn off the reference voltage
	TurnOffAllAdcRef();

	ResetWatchdog();

	// configure ADC6 as temperature sensor
	ADS1220ReadAllRegs(ADC_USED_AS_TEMP, bDataArray, sizeof(bDataArray));
	ResetTimer( thGenTimer, 5 );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}

	ADS1220ConfigAsTemperatureSensor(ADC_USED_AS_TEMP);
	ResetTimer( thGenTimer, 5);
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ADS1220ReadAllRegs(ADC_USED_AS_TEMP, bDataArray,sizeof(bDataArray));
	ADS1220StartConversion(ADC_USED_AS_TEMP);

	// configure the timers
	ResetTimer( thGenTimer, RFC_HUNTING_PERIOD_MS );
	ResetTimer( thGenTimer1, CHANNEL_CYCLE_MS );
	ResetSecTimer( thTimerSec1, GetTimeParameter(getPairTimeout) );
	StartTimer( thGenTimer );
	StartTimer( thGenTimer1 );
	StartSecTimer( thTimerSec1);
	nbrTransmission = 0;
	nbrResponses = 0;

}

//******************************************************************************
//
//  Function: ConnectingTask
//
//  Arguments: none
//
//  Returns: none
//
//  Description: read temperature, RPM and gas gauge. sends RFC every 100 ms.
// 				If it doesn't get response cycle the channels every UPDATE_CHANNEL_NBR_TR transmission
//				If it doesn't get response after HUNTING_MODE_TIMEOUT_SEC go to sleep
//				If it gets response go to IDLE mode
//******************************************************************************
void ConnectingTask(void)
{

	// if a valid radio packet has been received increment nbrResponses
	if(GetRadioPacketStatus()==TRUE)
	{
		ClearRadioPacketStatus();
		nbrResponses++;
	}

	// if response (NoCommand) is received more than NBR_RESP_CONNECTION on one channel
	// the correct channel is found - go to IDLE mode
	if(nbrResponses>=NBR_RESP_CONNECTION)
	{
		currentState = ST_IDLE;
		InitIdleMode();
		return;
	}

	// RFC timeout - send the message
	if(TimerExpired( thGenTimer))
	{
		ResetTimer( thGenTimer, RFC_HUNTING_PERIOD_MS );
		ReadTemperature();
        // wait until the UART finishes sending the previous packet
		mplSendRfcPIB();
		nbrTransmission++;
	}

	// channel cycle timeout - change the channel
	if(nbrTransmission>=UPDATE_CHANNEL_NBR_TR)
	{
		// wait until the UART finishes sending the previous packet
		while(IsZbTiSending())
		{
			ResetWatchdog();
		}

		// wait for the radio to send the data over the air and get ready for the packet
		//ResetTimer( thGenTimer1, RFC_RESPONSE_TIME );
		ResetTimer( thGenTimer1,10 );
		while(TimerExpired( thGenTimer1)==FALSE)
		{
			ResetWatchdog();
		}
		IncreaseZbChannel();							// increment the channel
		SetRadioChannel();								// set the new channel in the radio
        ResetTimer( thGenTimer1,8 );
        while(TimerExpired( thGenTimer1)==FALSE)
        {
            ResetWatchdog();
        }

		nbrTransmission = 0;
		nbrResponses = 0;
	}

	// read St Gyro data
	if(stGyroHasData())
	{
		GetSTGyroData();
	}

	// check for power down timeout
	if(SecTimerExpired( thTimerSec1))
	{
		currentState = ST_DEEP_SLEEP;
	}

}



void InitIdleMode(void)
{
	DisableAnalogGyro();
	initST();
#if COMPASS_ENABLE
	CompassInit();					// initialize the compass and wait 10ms
	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
#endif


	if(IsDeviceMaster())
	{
	    SetRadioReceiverOn();
	}



// after version 2v06 we do not use external Vref, because they tend to break
// when subjected to temperature stress, due to the different temp.coeff between the potting material and the PCB.
// We use the ADC internal current reference

	// configure all ADCs connected to strain gauge
//	for(adcNumber = ADC_USED_FIRST; adcNumber<=ADC_USED_LAST; adcNumber++)
//	{
//
//		ADS1220Reset(adcNumber);
//
//		ADS1220ReadAllRegs(adcNumber, bDataArray,sizeof(bDataArray));
//		ResetTimer( thGenTimer, ZB_TX_PERIOD );
//		StartTimer( thGenTimer );
//		while(!TimerExpired( thGenTimer))
//		{
//			ResetWatchdog();
//		}
//		analogChannel = ADS1220_IN_AIN1_AIN2;
//		//analogChannel = ADS1220_IN_AVDD_BY_4;	// measures AVDD
//		//analogChannel = ADS1220_IN_VREF;	// measures Vref
//
//		ADS1220Configure(analogChannel,adcNumber, idacCurrent[adcNumber]);
//		ResetTimer( thGenTimer, ZB_TX_PERIOD );
//		StartTimer( thGenTimer );
//		while(!TimerExpired( thGenTimer))
//		{
//			ResetWatchdog();
//		}
//		ADS1220ReadAllRegs(adcNumber, bDataArray,sizeof(bDataArray));
//		ADS1220StartConversion(adcNumber);
//	}

	// configure ADC7 as temperature sensor
	ADS1220ReadAllRegs(ADC_USED_AS_TEMP, bDataArray,sizeof(bDataArray));
	ResetTimer( thGenTimer, ZB_TX_PERIOD );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ADS1220ConfigAsTemperatureSensor(ADC_USED_AS_TEMP);
	ResetTimer( thGenTimer, ZB_TX_PERIOD );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ADS1220ReadAllRegs(ADC_USED_AS_TEMP, bDataArray,sizeof(bDataArray));
	ADS1220StartConversion(ADC_USED_AS_TEMP);

//	ADS1220Powerdown(ADC_NOT_USED);

	ResetTimer( thGenTimer, GetTimeParameter(getRfcRate) );
	ResetTimer( thGenTimer1, GetTimeParameter(getRfcRate) );
	ResetTimer( thGenTimer2, GetTimeParameter(getRfcRate)/2 );
//	ResetTimer( thGenTimer, INIT_IDLE_PERIOD );
	ResetSecTimer( thTimerSec1, GetTimeParameter(getPairTimeout) );
	ResetSecTimer( thTimerSec2, 0 );
	nbrTransmission = 0;
	batSampleCounter = SAMPLE_BATTERY_TIME_SEC;
	takeSample = 0;
	digGyroSampleCounter = 0;
	digGyroOnOff = TRUE;
	SetPinAsOutput(P9_TP39 );
	InitStrainGaugesReading();
	InitADCInRFC();
	readTorque = FALSE;
}




//******************************************************************************
//
//  Function: IdleTask
//
//  Arguments: none
//
//  Returns: none
//
//  Description: read temperature, RPM and gas gauge. sends RFC every RFC_RATE ms.
//	Power down radio between transmissions.
//
//******************************************************************************
void IdleTask(void)
{

	ResetWatchdog();
	if(batSampleCounter>=SAMPLE_BATTERY_TIME_SEC)
	{
		batSampleCounter = 0;
		takeSample=1;
	}

	if(IsCmdWaitRespIdle()==FALSE)
    {
	    ResetTimer( thGenTimer1, GetTimeParameter(getRfcTimeout) );
    }


	if( IsRadioActive())
	{
		if(TimerExpired( thGenTimer1))
		{
			PowerDownRadio();
			if(digGyroOnOff==FALSE)
			{
				WakeUpStGyro();
			}
			digGyroOnOff ^=1;
		}
		else
		{
			if(GetCommandStatus()==TRUE)
			{
				PowerDownRadio();
				SetNoCommand(FALSE);
				ResetSecTimer( thTimerSec1, GetTimeParameter(getPairTimeout) );
				nbrTransmission = 0;
				if(digGyroOnOff==FALSE)
				{
					WakeUpStGyro();
				}
				digGyroOnOff ^=1;
			}
			else
			{
				if(GetRadioPacketStatus())
				{
					ResetSecTimer( thTimerSec1, GetTimeParameter(getPairTimeout) );
					ClearRadioPacketStatus();
					nbrTransmission = 0;
				}

			}

		}
	}

	// read the gyro and the slave devices in the second half
	if(readTorque == TRUE)
	{
		if(digGyroOnOff==TRUE)
		{
			if(stGyroHasData())
			{
				digGyroSampleCounter++;
				GetSTGyroData();
			}
		}
	}
	else
	{
		if(TimerExpired( thGenTimer2))
		{
			if(takeSample)
			{
				ADS1220ConfigAsTemperatureSensor(ADC_USED_AS_TEMP);
				ADS1220StartConversion(ADC_USED_AS_TEMP);
			}

			readTorque = TRUE;

			// start reading from PIB and sensor boards. The actual reading is done in updateMplCommState(), which needs to be called periodically.
			StartDataCollection();
		}

	}

	StrainGaugeReadInIdleModeMPLS();

	if(TimerExpired( thGenTimer))
	{

		if(digGyroOnOff==TRUE)
		{
			PowerDownStGyro();
		}

		// turn on the radio
		if(IsRadioActive()==FALSE)
		{
			ResetRadio();
			ResetTimer( thGenTimer1, 5 );		// wait 3ms to wake up the radio
			while(!TimerExpired( thGenTimer1));
			SetRadioChannel();
			ResetTimer( thGenTimer1, 10 );		// wait 2ms to execute the command
			while(!TimerExpired( thGenTimer1));
		}
		else
		{
			ResetTimer( thGenTimer1, 5 );		// wait to read the gas gauge
			while(!TimerExpired( thGenTimer1));
		}

		if(takeSample)
		{
			ReadTemperature();

			MeasureBatVoltageAdc();
		}

        AverageTheResults();
		mplSendRfcPIB();
		InitStrainGaugesReading();
		nbrTransmission++;
		SetNoCommand(FALSE);
		if(IsChannelChangePending()== TRUE)
		{
			ResetTimer( thGenTimer1, 10 );		// wait 10ms to for the RFC to be sent
			while(!TimerExpired( thGenTimer1));
			SetNewChannel();
		}

		ResetTimer( thGenTimer, (GetTimeParameter(getRfcRate) - (IDLE_TASK_TIME_MS + RFC_TX_TIME)) );
		ResetTimer( thGenTimer1, GetTimeParameter(getRfcTimeout) );
		ResetTimer( thGenTimer2, GetTimeParameter(getRfcRate) - SBOARDS_COMM_TIME);

		batSampleCounter++;
		takeSample = 0;
		readTorque = FALSE;
	}


	// check for communication lost timeout
	// if it doesn't receive response for 10 transmissions
	// start hunting for base radio again
	if(nbrTransmission>TRANSMISSION_TIMEOUT)
	{
		currentState = ST_INIT;
	}
}




//******************************************************************************
//
//  Function: IdleTaskSlave
//
//  Arguments: none
//
//  Returns: none
//
//  Description:
//
//
//******************************************************************************
void IdleTaskSlave(void)
{

    ResetWatchdog();


    StrainGaugeReadInIdleModeMPLS();

    if(IsRs485CmdRcvd())
    {
        nbrTransmission++;
        ResetSecTimer( thTimerSec1, GetTimeParameter(getPairTimeout) +SLAVE_TEC_TIMEOUT_SEC);
    }



    // check for communication lost timeout
    // if it doesn't receive commands for 900 seconds
    // go to power down mode.
    if(SecTimerExpired( thTimerSec1))
    {
        currentState = ST_DEEP_SLEEP;
    }

}





void PowerDownTask(void)
{
	SystemPowerDown();
}


void InitGasGauge(void)
{
#if GAS_GAUGE_ENABLE
	// 	enable 3V3_D. Otherwise it keeps the I2C lines low.
	ResetTimer( thGenTimer, 1000 );
	StartTimer( thGenTimer );
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ResetTimer( thGenTimer, 1000 );
	BatteryGasMonitorReadAllRegs();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
		if(TimerExpired( thGenTimer))
			break;
	}
	STC3100GetRawData(bDataArray,sizeof(bDataArray));
	ResetTimer( thGenTimer, 1000 );
	BatteryGasMonitorReadId();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
		if(TimerExpired( thGenTimer))
			break;
	}

	STC3100GetRawData(bDataArray,sizeof(bDataArray));
	//InitBatteryGasMonitor();
	ResetTimer( thGenTimer, 1000 );
	StartTimer( thGenTimer );
	ResetBatteryGasMonitor();
	//	while(STC3100TxDone()==FALSE)
	//	{
	//		ResetWatchdog();
	//	}
	while(!TimerExpired( thGenTimer))
	{
		ResetWatchdog();
	}
	ResetTimer( thGenTimer, 1000 );
	BatteryGasMonitorReadAllRegs();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
		if(TimerExpired( thGenTimer))
			break;
	}
	STC3100GetRawData(bDataArray,sizeof(bDataArray));
#endif
}


//******************************************************************************
// Function name:    TurnOffAllAdcRef
//******************************************************************************
// Description:      Disconnect all Vrefs from StrainGauge Connectors
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void TurnOffAllAdcRef(void)
{
	for(adcNumber =ADC_USED_FIRST; adcNumber<=ADC_USED_LAST; adcNumber++)
	{
		ADS1220TurnOffReference(adcNumber);
	}
}





//******************************************************************************
// Function name:    InitADCInRFC
//******************************************************************************
// Description:      Configure All ADCs for RFC mode:
//					ADC 0 to 6 (connected to strain gauges)- differential input AIN1 AIN2
//					ADC 7 (connected to Analog Gyro)- as temperature sensor
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void InitADCInRFC(void)
{
	// turn on the ADC CLK
	P11SEL |= BIT0;
	// configure all ADCs connected to strain gauge
	for(adcNumber =ADC_USED_FIRST; adcNumber<=ADC_USED_LAST; adcNumber++)
	{

		analogChannel = ADS1220_IN_AIN1_AIN2;
		//analogChannel = ADS1220_IN_AVDD_BY_4;
		//analogChannel = ADS1220_IN_VREF;
		ADS1220Configure(analogChannel,adcNumber, idacCurrent[adcNumber]);
		ADS1220StartConversion(adcNumber);
	}

}

//******************************************************************************
// Function name:    PowerDownADC
//******************************************************************************
// Description:      Send a power down command to all ADCs.
//					 Turn off the ADC CLK
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void PowerDownADC(void)
{
	for(adcNumber =ADC_USED_FIRST; adcNumber<ADS1220_TOTAL; adcNumber++)
	{

		ADS1220Powerdown(adcNumber);
	}

	P11SEL &= ~BIT0;
}

//******************************************************************************
// Function name:    ReadSerNumber
//******************************************************************************
// Description:      Read Serial number from DS28CM00
//
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void ReadSerNumber(void)
{
	BYTE crcSnCalc;
	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );


	SerNumberReadDataStart();
	while(SerNumberDataReady()==FALSE)
	{
		ResetWatchdog();
		if(TimerExpired( thGenTimer))
			break;
	}

	SerNumberGetRawData(bDataArray,sizeof(bDataArray));
	crcSnCalc=CalcSnCRC(bDataArray, 7);
	SaveSerNumber(bDataArray);

}







//******************************************************************************
// Function name:    PowerDownMCU
//******************************************************************************
// Description:      power down MCU for a IDLE_SLEEP_TIME
//
//
// parameters:       none
//
// Returned value:   none
//
//******************************************************************************
void PowerDownMCU(void)
{
	StopWatchdog();
//	SetupLPMode((WORD) IDLE_SLEEP_TIME);
	SetupLPMode( GetTimeParameter(getRfcRate)/2 - IDLE_TASK_TIME_MS);
	__bis_SR_register(LPM0_bits + GIE);       // Enter LPM0 w/interrupt
	__no_operation();                         // For debugger
	DisableLPMode();
	SetWatchdog();
}



//******************************************************************************
// Function name:    SetAdcCurrent
//******************************************************************************
// Description:     Save the adc current setting.
//
// parameters:      BYTE adcIndex - ADC number
//                  ADS1220_IDAC_CURRENT setCurrent - constant current value.
//
// Returned value:   none
//
//******************************************************************************
void SetAdcCurrent(BYTE adcIndex, BYTE setCurrent)
{
    if((ADS1220_IDAC_CURRENT)setCurrent > AD1220_IDAC_CURRENT_1500uA)
    {
        return;
    }

    idacCurrent[adcIndex] = (ADS1220_IDAC_CURRENT)setCurrent;
}




/************************************
 ******* TEST FUNCTIONS**************
************************************/

#ifdef TEST_FUNCTIONS

void TestPowerDownMode(void)
{
	initMPS430Pins();
	EnableAllPower();
	Set_System_Clock();
	InitTimers();
	SysTimerConfig();
	__bis_SR_register(GIE);
	ReedSwitchInit();
	SetOutputPin(SW_LED, TRUE );

    if(IsDeviceMaster())
    {
        InitRadioInterface();
    }
	thGenTimer = RegisterTimer();
	ResetTimer( thGenTimer, 3000 );
	StartTimer( thGenTimer );
	while(1)
	{
		if(IsSwitchOn())
		{
	//		if(TimerExpired( thGenTimer))
	//		{
	//			SetSystemDeepSleep();
	//			initMPS430Pins();
	//			SetOutputPin(LED_CONTROL, FALSE );
	//			SetOutputPin(SW_LED, TRUE );
	//			Set_System_Clock();
	//			SysTimerConfig();
	//			ResetTimer( thGenTimer, 3000 );
	//			StartTimer( thGenTimer );
	//		}
		}
		else
		{
			if(TimerExpired( thGenTimer))
			{
				//DisableAllPower();
				DisableAnalogPower();
				DisableDigitalPower();
	//			DisableRadioPower();
				PowerDownRadio();
				DisableAnalogGyro();
				SetOutputPin(SW_LED, FALSE );

				//			EnableDigitalPower();
				SetSystemDeepSleep();
				initMPS430Pins();
				EnableAllPower();
				SetPinAsOutput(ZB_RSTn);
				SetOutputPin(ZB_RSTn, FALSE );

				SetOutputPin(SW_LED, TRUE );

				Set_System_Clock();
				InitTimers();
				SysTimerConfig();
				__bis_SR_register(GIE);
				ReedSwitchInit();

			    if(IsDeviceMaster())
			    {
			        InitRadioInterface();
			    }
				ResetTimer( thGenTimer, 3000 );
				StartTimer( thGenTimer );
				SetOutputPin(ZB_RSTn, TRUE );
			}

		}
	}
}





void TestGasGauge(void)
{
#if GAS_GAUGE_ENABLE
	ResetTimer( thGenTimer, 1000 );
	StartTimer( thGenTimer );
	BatteryGasMonitorReadAllRegs();
	while(STC3100DataReady()==FALSE);
	STC3100GetRawData(bDataArray,sizeof(bDataArray));
	BatteryGasMonitorReadId();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
	}
	STC3100GetRawData(bDataArray,sizeof(bDataArray));
	//InitBatteryGasMonitor();
	ResetBatteryGasMonitor();
	while(STC3100TxDone()==FALSE)
	{
		ResetWatchdog();
	}
	BatteryGasMonitorReadAllRegs();
	while(STC3100DataReady()==FALSE)
	{
		ResetWatchdog();
	}
	STC3100GetRawData(bDataArray,sizeof(bDataArray));

	while(1)
	{
		if(TimerExpired( thGenTimer))
		{
			BatteryGasMonitorReadAllRegs();
			while(STC3100DataReady()==FALSE)
			{
				ResetWatchdog();
			}
			STC3100GetRawData(bDataArray,sizeof(bDataArray));
			ParseGasGaugeData(bDataArray);
			ResetTimer( thGenTimer, 1000 );
			StartTimer( thGenTimer );
		}
		ResetWatchdog();
	}

#endif
}







void TestCompass(void)
{
	// test compass
	#if COMPASS_ENABLE
		ResetTimer( thGenTimer, 10 );
		StartTimer( thGenTimer );

		while(1)
		{
			if(TimerExpired( thGenTimer))
			{
				// reading data from the compass takes about 920us
//				SetOutputPin(P8_TP7, TRUE);
				CompassReadDataStart();
				while(CompassDataReady()==FALSE)
				{
					ResetWatchdog();
				}
				CompassGetRawData(bDataArray,sizeof(bDataArray));
				sMagnetData.iXField[0] = bDataArray[0];
				sMagnetData.iXField[0] = (sMagnetData.iXField[0]<<8)|bDataArray[1];
				sMagnetData.iZField[0] = bDataArray[2];
				sMagnetData.iZField[0] = (sMagnetData.iZField[0]<<8)|bDataArray[3];
				sMagnetData.iYField[0] = bDataArray[4];
				sMagnetData.iYField[0] = (sMagnetData.iYField[0]<<8)|bDataArray[5];
//				SetOutputPin(P8_TP7, FALSE);
				ResetTimer( thGenTimer, 10 );
				StartTimer( thGenTimer );
			}
			ResetWatchdog();

		}
	#endif
	// end test compass
}




void TestStGyro(void)
{
	int gyroData[30];
	WORD timeBtwSamples[30];
	WORD timesToStable[30];
	unsigned int i,j,k;
	BOOL gyroStable;

	while(1)
	{
		for(i=0;i<30;i++)
		{

			PowerDownStGyro();
			__delay_cycles(40000);
			__delay_cycles(40000);

			SetSystemUTCTime( (DWORD) 0 );

			setSTreg1();
			setSTreg3();
			gyroStable = FALSE;
			j=0;
			k=0;
			while(k<5)
			{
				while(stGyroHasData()==FALSE);

				gyroStable = !stGyroReadData( &gyroData[i]);
				j++;
				if(gyroStable==TRUE)
					k++;
				else
					k=0;
			}
			timesToStable[i] = j;
			while(stGyroHasData()==FALSE);
			stGyroReadData( &gyroData[i]);

			timeBtwSamples[i] = (WORD)GetSystemUTCTime();

		}

		ResetWatchdog();


	}

}


void TestSerNumber(void)
{
	BYTE crcSnCalc;
	ResetTimer( thGenTimer, 10 );
	StartTimer( thGenTimer );

	while(1)
	{
		if(TimerExpired( thGenTimer))
		{
			SerNumberReadDataStart();
			while(SerNumberDataReady()==FALSE)
			{
				ResetWatchdog();
			}

			SerNumberGetRawData(bDataArray,sizeof(bDataArray));
			crcSnCalc=CalcSnCRC(bDataArray, 7);
			if(crcSnCalc == bDataArray[7])
			{
				SaveSerNumber(bDataArray);
			}
			ResetTimer( thGenTimer, 10 );
			StartTimer( thGenTimer );
		}
	}
}



//******************************************************************************
//
//  Function: TestSensorReadings
//
//  Arguments: void
//
//  Returns: WORD number of bad sensors.
//
//  Description: read all sensors 10 times, toggle the LDO if there is non responsive sensor,
//				and return the number of non responsive sensors after the last reading cycle
//
//******************************************************************************
void TestSensorReadings(void)
{
	WORD i,j;
	WORD sensorsNotSending;
	WORD wCounterR;
	j=10;
	sensorsNotSending = 0;
	EnableExtPower();
	SetOutputPin(RS485_PWR_EN, FALSE);
	UARTA3Init();
	UARTA3RxEnable();

	while(1)
	{
		// toggle the Sensor LDO - if a board doesn't send data
		j=0;
		if(sensorsNotSending>0)
		{
			sensorsNotSending = 0;
			SetOutputPin(RS485_PWR_EN, TRUE);
			__delay_cycles(16000);
			SetOutputPin(RS485_PWR_EN, FALSE);
		}

		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		while(TimerExpired( thGenTimer)==FALSE)
		{
			ResetWatchdog();
		}
		SetOutputPin(EXT_DEV_EN, TRUE);
		SetOutputPin(EXT_DEV_ADD, FALSE);
		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		while(TimerExpired( thGenTimer)==FALSE)
		{
			ResetWatchdog();
		}
		SetOutputPin(EXT_DEV_EN, FALSE);
		ResetTimer( thGenTimer, ZB_TX_PERIOD );
		while(TimerExpired( thGenTimer)==FALSE)
		{
			ResetWatchdog();
		}
		for(i=0;i<4;i++)
		{
			SetOutputPin(EXT_DEV_ADD, TRUE);
			__delay_cycles(40000);	//	wait for 12B to transfer - 1.15mS, 12B at 125Kbps will take 0.96mS		- 2ms
			SetOutputPin(EXT_DEV_ADD, FALSE);
			__delay_cycles(1600);	// add 0.2mS guard band

			wCounterR = ReadUartA3Data(pUartRxData);
			if(wCounterR==12)
			{
				j++;
				wCounterR=0;
				sensorsNotSending++;
			}
			else
			{
				wCounterR=1;
				sensorsNotSending++;
			}

			ResetWatchdog();
		}

	}


}

void TestMcuAdc(void)
{


	while(1)
	{
		__delay_cycles(80000);
		MeasureBatVoltageAdc();
	}
}

#endif
